-- Populate user data
INSERT INTO account (account_id, email, password, firstname, lastname, createdAt) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0148', 'duy.le@iei.vn', '$2a$12$nkwyGYVMia.svVv4INvvk.pgAfu8HC91mIUHU9IN7CLjmqm1mcPfW', 'Duy', 'Le', '2004-10-19 10:23:01+02');
INSERT INTO account (account_id, email, password, firstname, lastname, createdAt) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0149', 'hong.truong@iei.vn', '$2a$12$nkwyGYVMia.svVv4INvvk.pgAfu8HC91mIUHU9IN7CLjmqm1mcPfW', 'Hong', 'Truong', '2004-10-19 10:23:02+02');
INSERT INTO account (account_id, email, password, firstname, lastname, createdAt) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0150', 'phi.tran@iei.vn', '$2a$12$nkwyGYVMia.svVv4INvvk.pgAfu8HC91mIUHU9IN7CLjmqm1mcPfW', 'Phi', 'Tran', '2004-10-19 10:23:03+02');
INSERT INTO account (account_id, email, password, firstname, lastname, createdAt) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0151', 'linh.nguyen@iei.vn', '$2a$12$nkwyGYVMia.svVv4INvvk.pgAfu8HC91mIUHU9IN7CLjmqm1mcPfW', 'Linh', 'Nguyen', '2004-10-19 10:23:04+02');
INSERT INTO account (account_id, email, password, firstname, lastname, createdAt) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0152', 'y.nguyen@iei.vn', '$2a$12$nkwyGYVMia.svVv4INvvk.pgAfu8HC91mIUHU9IN7CLjmqm1mcPfW', 'Y', 'Nguyen', '2004-10-19 10:23:05+02');
INSERT INTO account (account_id, email, password, firstname, lastname, createdAt) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0153', 'son.phan@iei.vn', '$2a$12$nkwyGYVMia.svVv4INvvk.pgAfu8HC91mIUHU9IN7CLjmqm1mcPfW', 'Son', 'Phan', '2004-10-19 10:23:06+02');
INSERT INTO account (account_id, email, password, firstname, lastname, createdAt) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0154', 'son.vo@iei.vn', '$2a$12$nkwyGYVMia.svVv4INvvk.pgAfu8HC91mIUHU9IN7CLjmqm1mcPfW', 'Son', 'Vo', '2004-10-19 10:23:07+02');
INSERT INTO account (account_id, email, password, firstname, lastname, createdAt) VALUES ('9eb52a9b-fb81-4930-b0cd-079a447ed2b1', 'test01@iei.vn', '$2a$12$nkwyGYVMia.svVv4INvvk.pgAfu8HC91mIUHU9IN7CLjmqm1mcPfW', 'Test01', 'IEI', '2004-10-19 10:23:08+02');

-- Populate room data
INSERT INTO room (room_id, name, secret, is_private) VALUES ('e7616832-bb4e-470a-8df4-0534ab56d960', 'IoT', 'U2FsdGVkX1%2FCft6zHuliGhZgGqMRARXy4IGnHcOAp48%3D', FALSE);
INSERT INTO room (room_id, name, secret, is_private) VALUES ('e7616832-bb4e-470a-8df4-0534ab56d961', 'DASA', 'U2FsdGVkX1%2FCft6zHuliGhZgGqMRARXy4IGnHcOAp48%3D', FALSE);
INSERT INTO room (room_id, name, secret, is_private) VALUES ('e7616832-bb4e-470a-8df4-0534ab56d962', 'duy_son_linh_hong', 'U2FsdGVkX1%2FCft6zHuliGhZgGqMRARXy4IGnHcOAp48%3D', FALSE);

-- Populate room_user
INSERT INTO room_user (room_id, account_id) VALUES ('e7616832-bb4e-470a-8df4-0534ab56d960', '2a69ec7d-a147-4c71-8a20-9ba760de0148');
INSERT INTO room_user (room_id, account_id) VALUES ('e7616832-bb4e-470a-8df4-0534ab56d960', '2a69ec7d-a147-4c71-8a20-9ba760de0149');
INSERT INTO room_user (room_id, account_id) VALUES ('e7616832-bb4e-470a-8df4-0534ab56d960', '2a69ec7d-a147-4c71-8a20-9ba760de0150');
INSERT INTO room_user (room_id, account_id) VALUES ('e7616832-bb4e-470a-8df4-0534ab56d960', '2a69ec7d-a147-4c71-8a20-9ba760de0151');
INSERT INTO room_user (room_id, account_id) VALUES ('e7616832-bb4e-470a-8df4-0534ab56d960', '2a69ec7d-a147-4c71-8a20-9ba760de0152');
INSERT INTO room_user (room_id, account_id) VALUES ('e7616832-bb4e-470a-8df4-0534ab56d960', '2a69ec7d-a147-4c71-8a20-9ba760de0153');

-- Populate event
INSERT INTO event (event_id, senderId, receiverId, type, status) VALUES ('e7616832-bb4e-470a-0000-0534ab56d001', '2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0149', 0, 0);
INSERT INTO event (event_id, senderId, receiverId, type, status) VALUES ('e7616832-bb4e-470a-0000-0534ab56d002', '2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0150', 0, 0);
INSERT INTO event (event_id, senderId, receiverId, type, status) VALUES ('e7616832-bb4e-470a-0000-0534ab56d003', '2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0151', 0, 0);
INSERT INTO event (event_id, senderId, receiverId, type, status) VALUES ('e7616832-bb4e-470a-0000-0534ab56d004', '2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0152', 0, 0);
INSERT INTO event (event_id, senderId, receiverId, type, status) VALUES ('e7616832-bb4e-470a-0000-0534ab56d005', '2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0153', 0, 0);
INSERT INTO event (event_id, senderId, receiverId, type, status) VALUES ('e7616832-bb4e-470a-0000-0534ab56d006', '2a69ec7d-a147-4c71-8a20-9ba760de0150', '2a69ec7d-a147-4c71-8a20-9ba760de0152', 0, 0);
INSERT INTO event (event_id, senderId, receiverId, type, status) VALUES ('e7616832-bb4e-470a-0000-0534ab56d007', '2a69ec7d-a147-4c71-8a20-9ba760de0150', '2a69ec7d-a147-4c71-8a20-9ba760de0153', 0, 0);

-- Polulate message
INSERT INTO message (message_id, roomId, content, createdAt) VALUES ('e7616832-bb4e-470a-1111-0534ab56d960', 'e7616832-bb4e-470a-8df4-0534ab56d960', 'Hi IoT Room! Have a nice day!', '2004-10-19 10:23:54+02');
UPDATE room SET last_message_id = 'e7616832-bb4e-470a-1111-0534ab56d960' where room_id = 'e7616832-bb4e-470a-8df4-0534ab56d960';

INSERT INTO message (message_id, roomId, content, createdAt) VALUES ('e7616832-bb4e-470a-1111-0534ab56d961', 'e7616832-bb4e-470a-8df4-0534ab56d960', 'Hi Duy, you too!', '2004-10-19 10:23:54+02');
UPDATE room SET last_message_id = 'e7616832-bb4e-470a-1111-0534ab56d961' where room_id = 'e7616832-bb4e-470a-8df4-0534ab56d960';

-- Populate contacts
-- @Duy
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0149');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0150');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0151');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0152');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0153');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0154');

INSERT INTO account_fcontact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0150');
INSERT INTO account_fcontact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0148', '2a69ec7d-a147-4c71-8a20-9ba760de0151');

--@Hong
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0148');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0150');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0151');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0152');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0153');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0154');

INSERT INTO account_fcontact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0148');
INSERT INTO account_fcontact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0149', '2a69ec7d-a147-4c71-8a20-9ba760de0150');

--@Phi
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0150', '2a69ec7d-a147-4c71-8a20-9ba760de0148');
INSERT INTO account_contact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0150', '2a69ec7d-a147-4c71-8a20-9ba760de0149');

INSERT INTO account_fcontact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0150', '2a69ec7d-a147-4c71-8a20-9ba760de0148');
INSERT INTO account_fcontact (account_id, contact_id) VALUES ('2a69ec7d-a147-4c71-8a20-9ba760de0150', '2a69ec7d-a147-4c71-8a20-9ba760de0150');
