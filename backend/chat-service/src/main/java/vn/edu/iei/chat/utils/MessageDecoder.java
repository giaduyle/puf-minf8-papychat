package vn.edu.iei.chat.utils;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.edu.iei.chat.entity.Message;

public class MessageDecoder implements Decoder.Text<Message> {
    private static ObjectMapper mapper = new ObjectMapper();

    @Override
    public Message decode(final String s) throws DecodeException {
        try {
            return mapper.readValue(s, Message.class);
        } catch (JsonProcessingException e) {
            throw new DecodeException(s.toString(), "Unable to decode websocket message!", e);
        }
    }

    @Override
    public boolean willDecode(final String s) {
        return (s != null);
    }

    @Override
    public void init(final EndpointConfig endpointConfig) {
        // Custom initialization logic
    }

    @Override
    public void destroy() {
        // Close resources
    }
}