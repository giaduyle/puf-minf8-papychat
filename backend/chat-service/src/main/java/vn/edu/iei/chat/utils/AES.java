package vn.edu.iei.chat.utils;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * Utils to Encrypt/Decrypt with CryptoJS compatible algorithm (cross-platform
 * with JS, Java and Dart) Referece:
 * https://medium.com/@chingsuehok/cryptojs-aes-encryption-decryption-for-flutter-dart-7ca123bd7464
 */
public class AES {
    private static final Logger logger = Logger.getLogger(AES.class.getName());
    private static final Random random = new Random();

    public static String encrypt(final String plainText, final String secret) {
        try {
            final byte[] plainData = plainText.getBytes(StandardCharsets.UTF_8);
            final byte[] saltData = genRandomWithNonZero(8);

            final MessageDigest md5 = MessageDigest.getInstance("MD5");
            final byte[][] keyAndIV = GenerateKeyAndIV(32, 16, 1, saltData, secret.getBytes(StandardCharsets.UTF_8),
                    md5);

            final SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
            final IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);
            final Cipher aesCBC = Cipher.getInstance("AES/CBC/PKCS5Padding");
            aesCBC.init(Cipher.ENCRYPT_MODE, key, iv);

            final byte[] encryptedData = aesCBC.doFinal(plainData);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write("Salted__".getBytes());
            outputStream.write(saltData);
            outputStream.write(encryptedData);
            return Base64.encodeBase64String(outputStream.toByteArray());
        } catch (final Exception e) {
            if (e != null)
                e.printStackTrace();
            return null;
        }
    }

    public static String decrypt(final String cipherText, final String secret) {
        try {
            final byte[] cipherData = Base64.decodeBase64(cipherText);
            final byte[] saltData = Arrays.copyOfRange(cipherData, 8, 16);
            final MessageDigest md5 = MessageDigest.getInstance("MD5");
            final byte[][] keyAndIV = GenerateKeyAndIV(32, 16, 1, saltData, secret.getBytes(StandardCharsets.UTF_8),
                    md5);
            final SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
            final IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);
            final byte[] encrypted = Arrays.copyOfRange(cipherData, 16, cipherData.length);
            final Cipher aesCBC = Cipher.getInstance("AES/CBC/PKCS5Padding");
            aesCBC.init(Cipher.DECRYPT_MODE, key, iv);
            final byte[] decryptedData = aesCBC.doFinal(encrypted);
            return new String(decryptedData, StandardCharsets.UTF_8);
        } catch (final Exception e) {
            if (e != null)
                e.printStackTrace();
            return null;
        }
    }

    public static byte[][] GenerateKeyAndIV(final int keyLength, final int ivLength, final int iterations,
            final byte[] salt, final byte[] password, final MessageDigest md) {

        final int digestLength = md.getDigestLength();
        final int requiredLength = (keyLength + ivLength + digestLength - 1) / digestLength * digestLength;
        final byte[] generatedData = new byte[requiredLength];
        int generatedLength = 0;

        try {
            md.reset();

            // Repeat process until sufficient data has been generated
            while (generatedLength < keyLength + ivLength) {

                // Digest data (last digest if available, password data, salt if available)
                if (generatedLength > 0)
                    md.update(generatedData, generatedLength - digestLength, digestLength);
                md.update(password);
                if (salt != null)
                    md.update(salt, 0, 8);
                md.digest(generatedData, generatedLength, digestLength);

                // additional rounds
                for (int i = 1; i < iterations; i++) {
                    md.update(generatedData, generatedLength, digestLength);
                    md.digest(generatedData, generatedLength, digestLength);
                }

                generatedLength += digestLength;
            }

            // Copy key and IV into separate byte arrays
            final byte[][] result = new byte[2][];
            result[0] = Arrays.copyOfRange(generatedData, 0, keyLength);
            if (ivLength > 0)
                result[1] = Arrays.copyOfRange(generatedData, keyLength, keyLength + ivLength);

            return result;

        } catch (final Exception e) {
            if (e != null)
                logger.info(e.toString());
            return null;

        } finally {
            // Clean out temporary data
            Arrays.fill(generatedData, (byte) 0);
        }
    }

    public static byte[] genRandomWithNonZero(int seedLength) {
        byte[] salt = new byte[seedLength];
        final int randomMax = 245;
        for (int i = 0; i < seedLength; i++) {
            salt[i] = (byte) (random.nextInt(randomMax) + 1);
        }
        return salt;
    }

    public static String genRandomKey(int seedLength) {
        return Base64.encodeBase64String(genRandomWithNonZero(seedLength));
    }
}