package vn.edu.iei.chat.utils;

import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.control.ActivateRequestContext;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.jwt.JsonWebToken;

import vn.edu.iei.chat.repository.AccountRepository;

@Provider
@ApplicationScoped
@ActivateRequestContext
public class ChatRequestFilter implements ContainerRequestFilter {
    @Inject
    JsonWebToken jwt;

    @Inject
    AccountRepository repository;

    @Override
    @Transactional
    public void filter(ContainerRequestContext requestContext) {
        if (jwt != null && repository != null) {
            var me = repository.findByEmail(jwt.getClaim("email"));
            if (me != null) {
                me.lastSeenTime = new Date();
                repository.persistAndFlush(me);
            }
        }
    }
}
