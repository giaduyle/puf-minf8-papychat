package vn.edu.iei.chat.repository;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import vn.edu.iei.chat.entity.Media;

@ApplicationScoped
public class MediaRepository implements PanacheRepositoryBase<Media, String> {
}
