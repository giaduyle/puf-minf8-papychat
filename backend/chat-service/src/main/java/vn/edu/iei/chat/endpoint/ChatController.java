package vn.edu.iei.chat.endpoint;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import vn.edu.iei.chat.utils.MessageEncoder;

@ApplicationScoped
@ServerEndpoint(value = "/chat/{room_id}/{token_id}", encoders = { MessageEncoder.class })
public class ChatController {
    private static final Logger logger = Logger.getLogger(ChatController.class.getName());

    @Inject
    ChatService chatService;

    final Executor executor = Executors.newSingleThreadExecutor();

    @OnOpen
    public void onOpen(Session session, @PathParam("room_id") String roomId, @PathParam("token_id") String tokenId) {
        logger.info("Server onOpen!");
        var future = CompletableFuture.supplyAsync(() -> {
            boolean validated = chatService.validateToken(session, roomId, tokenId);
            if (!validated) {
                try {
                    session.close(new CloseReason(CloseReason.CloseCodes.CANNOT_ACCEPT, "Token validation failed!"));
                } catch (IOException e) {
                }
            }
            return null;
        }, executor);
        future.join();
    }

    @OnClose
    public void onClose(Session session, @PathParam("token_id") String tokenId) {
        logger.info("Server onClose!");
        chatService.onClose(session, tokenId);
    }

    @OnError
    public void onError(Session session, @PathParam("token_id") String tokenId, Throwable throwable) {
        logger.info("Server onError!");
        chatService.onError(session, tokenId);
    }

    @OnMessage
    public void onMessage(Session session, String message) {
        logger.info("Server onMessage: " + message);
        var future = CompletableFuture.supplyAsync(() -> {
            chatService.broadcastMessage(session, message);
            return null;
        }, executor);
        future.join();
    }
}