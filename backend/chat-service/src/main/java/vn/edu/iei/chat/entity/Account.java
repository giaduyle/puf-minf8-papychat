package vn.edu.iei.chat.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Cacheable
@JsonInclude(Include.NON_NULL)
public class Account extends PanacheEntityBase implements BaseEntity  {

    public static class Status {
        public static final int Inactive = 0;
        public static final int Online = 1;
        public static final int Offline = 2;
        public static final int Away = 3;
        public static final int Busy = 4;
    }

    public static class Gender {
        public static final int Unspecified = 0;
        public static final int Female = 1;
        public static final int Male = 2;
    }

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "account_id", updatable = false)
    public String id;

    @Column(length = 254, unique = true, nullable = false)
    public String email;

    @Column(length = 256, nullable = false)
    @JsonProperty(access = Access.WRITE_ONLY)
    public String password;

    @Column(length = 256)
    public String firstName;

    @Column(length = 256)
    public String lastName;

    @Column(length = 256)
    public String nickName;

    @Column(length = 512)
    public String address;

    @Column(length = 16)
    public String phone;

    @Column(length = 512)
    public String avatarUrl;

    public Date birthday;

    public Integer gender;

    public Integer status;

    @ManyToMany
    @JoinTable(name = "account_contact",
        joinColumns = { @JoinColumn(name = "account_id") },
        inverseJoinColumns = { @JoinColumn(name = "contact_id") })
    @JsonIgnore
    public Set<Account> contacts = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "account_fcontact",
        joinColumns = { @JoinColumn(name = "account_id") },
        inverseJoinColumns = { @JoinColumn(name = "contact_id") })
    @JsonIgnore
    public Set<Account> favoriteContacts = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "account_bcontact",
        joinColumns = { @JoinColumn(name = "account_id") },
        inverseJoinColumns = { @JoinColumn(name = "contact_id") })
    @JsonIgnore
    public Set<Account> blockedContacts = new HashSet<>();

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    @Temporal(TemporalType.TIMESTAMP)
    public Date createdAt;

    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    @Temporal(TemporalType.TIMESTAMP)
    public Date updatedAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    @Temporal(TemporalType.TIMESTAMP)
    public Date lastSeenTime;

    public String getId() {
        return id;
    }
}
