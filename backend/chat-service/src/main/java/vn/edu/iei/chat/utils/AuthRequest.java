package vn.edu.iei.chat.utils;

import javax.ws.rs.QueryParam;

public class AuthRequest {
    @QueryParam("email")
    public String email;

    @QueryParam("password")
    public String password;
}