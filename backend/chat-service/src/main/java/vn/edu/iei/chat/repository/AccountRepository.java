package vn.edu.iei.chat.repository;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import vn.edu.iei.chat.entity.Account;

@ApplicationScoped
public class AccountRepository implements PanacheRepositoryBase<Account, String> {
    public Account findByEmail(String email) {
        return find("email", email).firstResult();
    }
}