package vn.edu.iei.chat.api;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.jwt.JsonWebToken;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import io.quarkus.panache.common.Sort.Direction;
import vn.edu.iei.chat.entity.Account;
import vn.edu.iei.chat.entity.Message;
import vn.edu.iei.chat.entity.Room;
import vn.edu.iei.chat.repository.AccountRepository;
import vn.edu.iei.chat.repository.MessageRepository;
import vn.edu.iei.chat.repository.RoomRepository;
import vn.edu.iei.chat.utils.AES;
import vn.edu.iei.chat.utils.ApiResponse;
import vn.edu.iei.chat.utils.PageRequest;

@Path("/api/v1/rooms")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RoomController {
    @Inject
    RoomRepository repository;

    @Inject
    MessageRepository messageRepository;

    @Inject
    AccountRepository accountRepository;

    @Inject
    JsonWebToken jwt;

    @Context
    UriInfo uriInfo;

    @ConfigProperty(name = "vn.edu.iei.chat.secret")
    String secretKey;

    @GET
    @RolesAllowed("user")
    public Response queryAll(@QueryParam("q") String q, @BeanParam PageRequest pageRequest) {
        if (q == null) {
            var pagedQuery = repository.find("isPrivate", Sort.by("createdAt"), false)
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
            return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                    .header("X-Page-Count", pagedQuery.pageCount()).build();
        }
        var pagedQuery = repository
                .find("isPrivate = FALSE and lower(name) LIKE lower(concat('%',?1,'%'))", Sort.by("createdAt"), q)
                .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                .header("X-Page-Count", pagedQuery.pageCount()).build();
    }

    @POST
    @RolesAllowed("user")
    @Transactional
    public Response create(Room entity) {
        if (entity == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        if (entity.members == null) {
            entity.members = new HashSet<>();
        }
        String email = jwt.getClaim("email");
        if (email == null || email.length() == 0)
            return Response.status(Response.Status.UNAUTHORIZED).build();
        var me = accountRepository.findByEmail(email);
        if (me == null)
            return Response.status(Response.Status.UNAUTHORIZED).build();
        entity.members.add(me);
        if (entity.name == null || entity.name.length() == 0) {
            StringBuilder nameBuilder = new StringBuilder();
            for (var m : entity.members) {
                nameBuilder.append(m.firstName);
            }
            entity.name = nameBuilder.toString();
        }
        entity.secret = AES.genRandomKey(16);
        return ApiResponse.create(entity, repository, uriInfo);
    }

    @PUT
    @RolesAllowed("user")
    @Transactional
    public Response update(Room entity) {
        if (entity == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        Room room = repository.findById(entity.id);
        if (room == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        room.name = entity.name;
        room.description = entity.description;
        room.isPrivate = entity.isPrivate;
        room.admins = entity.admins != null ? Set.copyOf(entity.admins) : null;
        room.lastMessage = entity.lastMessage;
        return ApiResponse.update(room, repository, uriInfo);
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}")
    public Response getOne(@PathParam("id") String id) {
        return ApiResponse.getOneById(id, repository);
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/secret")
    public Response getSecret(@PathParam("id") String id) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        String email = jwt.getClaim("email");
        for (var member : entity.members) {
            if (member.email.equals(email)) {
                return Response.ok(AES.encrypt(entity.secret, secretKey)).build();
            }
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @DELETE
    @RolesAllowed("user")
    @Path("/{id}")
    @Transactional
    public Response deleteOne(@PathParam("id") String id) {
        return ApiResponse.delete(id, repository, uriInfo);
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/messages")
    public Response getMessages(@PathParam("id") String id, @BeanParam PageRequest pageRequest) {
        PanacheQuery<Message> pagedQuery = null;
        if (pageRequest.before != null) {
            try {
                pagedQuery = messageRepository.find("roomId = ?1 AND createdAt <= ?2",
                        Sort.by("createdAt", Direction.Descending), id, pageRequest.getBeforeAsDate())
                        .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (pagedQuery == null && pageRequest.after != null) {
            try {
                pagedQuery = messageRepository.find("roomId = ?1 AND createdAt >= ?2",
                        Sort.by("createdAt", Direction.Descending), id, pageRequest.getAfterAsDate())
                        .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (pagedQuery == null) {
            pagedQuery = messageRepository.find("roomId = ?1", Sort.by("createdAt", Direction.Descending), id)
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        }
        return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                .header("X-Page-Count", pagedQuery.pageCount()).build();
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/members")
    public Response getMembers(@PathParam("id") String id, @BeanParam PageRequest pageRequest) {
        var pagedQuery = accountRepository
                .find("SELECT m FROM Room r join r.members m WHERE r.id = ?1 ORDER BY m.createdAt", id)
                .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                .header("X-Page-Count", pagedQuery.pageCount()).build();
    }

    @PUT
    @RolesAllowed("user")
    @Path("/{id}/members")
    @Transactional
    public Response putMembers(@PathParam("id") String id, Set<String> ids) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        if (ids == null || ids.size() == 0)
            return Response.notModified().build();
        Set<Account> members = new HashSet<>();
        for (var fid : ids) {
            var member = accountRepository.findById(fid);
            if (member != null)
                members.add(member);
        }
        entity.members.clear();
        entity.members = members;
        entity.persistAndFlush();
        return Response.ok(entity.members).build();
    }

    @PATCH
    @RolesAllowed("user")
    @Path("/{id}/members")
    @Transactional
    public Response patchContact(@PathParam("id") String id, Set<String> ids) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        if (ids == null || ids.size() == 0)
            return Response.notModified().build();
        for (var fid : ids) {
            var member = accountRepository.findById(fid);
            if (member != null)
                entity.members.add(member);
        }
        entity.persistAndFlush();
        return Response.ok(entity.members).build();
    }

    @DELETE
    @RolesAllowed("user")
    @Path("/{id}/members/{fid}")
    @Transactional
    public Response removeContact(@PathParam("id") String id, @PathParam("fid") String fid) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        var member = accountRepository.findById(fid);
        if (member == null)
            return Response.notModified().build();
        entity.members.remove(member);
        entity.persistAndFlush();
        return Response.noContent().build();
    }

}
