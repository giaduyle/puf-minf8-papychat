package vn.edu.iei.chat.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

public class PageRequest {
    @QueryParam("pageNum")
    @DefaultValue("0")
    public int pageNum;

    @QueryParam("pageSize")
    @DefaultValue("10")
    public int pageSize;

    @QueryParam("beforeTime")
    public String before;

    @QueryParam("afterTime")
    public String after;

    public PageRequest() {
    }

    public PageRequest(int pageNum, int pageSize) {
        this.pageSize = pageSize;
    }

    public Date getBeforeAsDate() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
        return dateFormat.parse(this.before);
    }

    public Date getAfterAsDate() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
        return dateFormat.parse(this.after);
    }
}