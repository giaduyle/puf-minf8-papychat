package vn.edu.iei.chat.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Cacheable
@JsonInclude(Include.NON_NULL)
public class Room extends PanacheEntityBase implements BaseEntity  {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "room_id", updatable = false, nullable = false)
    public String id;

    @Column(length = 256)
    public String name;

    @Column(length = 512)
    public String description;

    @Column(nullable = false)
    @JsonProperty(access = Access.WRITE_ONLY)
    public String secret;

    @Column(name = "is_private")
    public boolean isPrivate;

    @ManyToMany
    @JoinTable(
        name="room_admin",
        joinColumns={@JoinColumn(name="room_id")},
        inverseJoinColumns={@JoinColumn(name="account_id")}
    )
    @JsonIgnore
    public Set<Account> admins = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name="room_user",
        joinColumns={@JoinColumn(name="room_id")},
        inverseJoinColumns={@JoinColumn(name="account_id")}
    )
    @JsonIgnore
    public Set<Account> members = new HashSet<>();

    @OneToOne
    @JoinColumn(name = "last_message_id")
    public Message lastMessage;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    @Temporal(TemporalType.TIMESTAMP)
    public Date createdAt;

    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    @Temporal(TemporalType.TIMESTAMP)
    public Date updatedAt;

    public String getId() {
        return id;
    }
}
