package vn.edu.iei.chat.api;

import java.util.logging.Logger;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.transaction.Transactional;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.jwt.JsonWebToken;

import at.favre.lib.crypto.bcrypt.BCrypt;
import vn.edu.iei.chat.entity.Account;
import vn.edu.iei.chat.repository.AccountRepository;
import vn.edu.iei.chat.utils.AES;
import vn.edu.iei.chat.utils.AuthRequest;
import vn.edu.iei.chat.utils.TokenUtils;

@Path("/api/v1/auth")
public class AuthController {
    private static final Logger logger = Logger.getLogger(AuthController.class.getName());

    @Inject
    JsonWebToken jwt;

    @Inject
    AccountRepository accountRepository;

    @ConfigProperty(name = "mp.jwt.verify.issuer")
    public String issuer;

    @ConfigProperty(name = "quarkusjwt.jwt.duration")
    public Long duration;

    @ConfigProperty(name = "vn.edu.iei.chat.secret")
    String secretKey;

    @PermitAll
    @GET
    @Path("/login")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@BeanParam AuthRequest authRequest) {
        if (authRequest == null || authRequest.email == null || authRequest.password == null)
            return Response.status(Response.Status.UNAUTHORIZED).build();
        String password = AES.decrypt(authRequest.password, secretKey);
        if (password == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        Account account = accountRepository.findByEmail(authRequest.email);
        var verifyResult = BCrypt.verifyer().verify(password.toCharArray(), account.password);
        if (account != null && verifyResult.verified) {
            try {
                String token = TokenUtils.generateToken(account.email, duration, issuer);
                JsonObject jsonToken = Json.createObjectBuilder().add("token", token).build();
                account.status = Account.Status.Online;
                accountRepository.persist(account);
                return Response.ok(jsonToken).build();
            } catch (Exception e) {
                e.printStackTrace();
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @PermitAll
    @GET
    @Path("/google")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response loginGoogle(@QueryParam("token") String token) {
        if (token == null)
            return Response.status(Response.Status.BAD_REQUEST).build();

        String accessToken = AES.decrypt(token, secretKey);
        if (accessToken == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
        Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential)
                .setApplicationName("Oauth2").build();
        try {
            var userInfo = oauth2.userinfo().get().execute();
            if (userInfo != null) {
                String email = userInfo.getEmail();
                if(email == null)
                    return Response.status(Response.Status.UNAUTHORIZED).build();

                var entity = accountRepository.findByEmail(email);
                if (entity == null) {
                    int gender = Account.Gender.Unspecified;
                    String genderStr = userInfo.getGender() != null ? userInfo.getGender().toLowerCase() : "";
                    if (genderStr.equals("male"))
                        gender = Account.Gender.Male;
                    else if (genderStr.equals("female"))
                        gender = Account.Gender.Female;

                    entity = new Account();
                    entity.email = email;
                    entity.password = BCrypt.withDefaults().hashToString(12, userInfo.getId().toCharArray());
                    entity.gender = gender;
                    entity.firstName = userInfo.getGivenName();
                    entity.lastName = userInfo.getFamilyName();
                    entity.avatarUrl = userInfo.getPicture();
                }
                entity.status = Account.Status.Online;
                accountRepository.persist(entity);
                try {
                    var jwtToken = TokenUtils.generateToken(email, duration, issuer);
                    var jsonToken = Json.createObjectBuilder().add("token", jwtToken).build();
                    return Response.ok(jsonToken).build();
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.UNAUTHORIZED).build();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @GET
    @RolesAllowed("user")
    @Path("/refresh")
    public Response refreshToken() {
        try {
            String token = TokenUtils.generateToken(jwt.getClaim("email"), duration, issuer);
            JsonObject jsonToken = Json.createObjectBuilder().add("token", token).build();
            return Response.ok(jsonToken).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @PermitAll
    @GET
    @Path("/encrypt")
    @Produces(MediaType.APPLICATION_JSON)
    public Response encode(@QueryParam("text") String text) {
        var cypher = AES.encrypt(text, secretKey);
        if (cypher == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        JsonObject jsonToken = Json.createObjectBuilder().add("cypher", cypher).build();
        return Response.ok(jsonToken).build();
    }
}
