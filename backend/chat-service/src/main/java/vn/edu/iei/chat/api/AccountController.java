package vn.edu.iei.chat.api;

import java.util.Collections;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.swing.text.html.parser.Entity;
import javax.transaction.Transactional;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.jwt.JsonWebToken;

import at.favre.lib.crypto.bcrypt.BCrypt;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import vn.edu.iei.chat.entity.Account;
import vn.edu.iei.chat.entity.Event;
import vn.edu.iei.chat.repository.AccountRepository;
import vn.edu.iei.chat.repository.EventRepository;
import vn.edu.iei.chat.repository.RoomRepository;
import vn.edu.iei.chat.utils.AES;
import vn.edu.iei.chat.utils.ApiResponse;
import vn.edu.iei.chat.utils.PageRequest;
import vn.edu.iei.chat.utils.TokenUtils;

@Path("/api/v1/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountController {
    private static final Logger logger = Logger.getLogger(AccountController.class.getName());

    @Inject
    JsonWebToken jwt;

    @Inject
    AccountRepository repository;

    @Inject
    RoomRepository roomRepository;

    @Inject
    EventRepository eventRepository;


    @Context
    UriInfo uriInfo;

    @ConfigProperty(name = "vn.edu.iei.chat.secret")
    String secretKey;

    @ConfigProperty(name = "mp.jwt.verify.issuer")
    public String issuer;

    @ConfigProperty(name = "quarkusjwt.jwt.duration")
    public Long duration;

    @GET
    @RolesAllowed("user")
    public Response queryUser(@QueryParam("q") String q, @QueryParam("friend") Integer friend,
            @BeanParam PageRequest pageRequest) {
        String email = jwt.getClaim("email");
        PanacheQuery<Account> pagedQuery;

        try {
            if (q == null || q.length() == 0) {
                if (friend == null) {
                    pagedQuery = repository.find(
                    "from Account a WHERE a.id NOT IN (SELECT c.id FROM Account b join b.blockedContacts c WHERE b.email = ?1) AND a.email <> ?1",
                    Sort.by("createdAt"), email).page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
                }
                else if (friend == 0) {
                    pagedQuery = repository.find(
                            "FROM Account a WHERE a.id NOT IN (SELECT c.id FROM Account b join b.contacts c WHERE b.email = ?1) AND a.id NOT IN (SELECT c.id FROM Account b join b.blockedContacts c WHERE b.email = ?1) AND a.email <> ?1",
                            Sort.by("createdAt"), email).page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
                } else {
                    pagedQuery = repository
                            .find("SELECT c FROM Account a JOIN a.contacts c WHERE a.email = ?1 ORDER BY c.createdAt",
                                    email)
                            .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
                }
                return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                        .header("X-Page-Count", pagedQuery.pageCount()).build();
            }

            if (friend == null) {
                pagedQuery = repository.find(
                        "FROM Account a WHERE (lower(a.firstName) LIKE lower(concat('%',?1,'%')) OR lower(a.lastName) LIKE lower(concat('%',?1,'%')) OR lower(a.nickName) LIKE lower(concat('%',?1,'%')) OR lower(a.email) LIKE lower(concat('%',?1,'%')) OR lower(a.phone) LIKE lower(concat('%',?1,'%'))) AND a.id NOT IN (SELECT c.id FROM Account b join b.blockedContacts c WHERE b.email = ?2) AND a.email <> ?2 ORDER BY a.createdAt",
                        q, email).page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
            } else if (friend == 0) {
                pagedQuery = repository.find(
                        "FROM Account a WHERE (lower(a.firstName) LIKE lower(concat('%',?1,'%')) OR lower(a.lastName) LIKE lower(concat('%',?1,'%')) OR lower(a.nickName) LIKE lower(concat('%',?1,'%')) OR lower(a.email) LIKE lower(concat('%',?1,'%')) OR lower(a.phone) LIKE lower(concat('%',?1,'%'))) AND (a.id NOT IN (SELECT c.id FROM Account b JOIN b.contacts c WHERE b.email = ?2)) AND (a.id NOT IN (SELECT c2.id FROM Account b2 JOIN b2.blockedContacts c2 WHERE b2.email = ?2)) AND a.email <> ?2 GROUP BY a.id ORDER BY a.createdAt",
                        q, email).page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
            } else {
                pagedQuery = repository.find(
                        "FROM Account acc JOIN acc.contacts a WHERE (lower(a.firstName) LIKE lower(concat('%',?1,'%')) OR lower(a.lastName) LIKE lower(concat('%',?1,'%')) OR lower(a.nickName) LIKE lower(concat('%',?1,'%')) OR lower(a.email) LIKE lower(concat('%',?1,'%')) OR lower(a.phone) LIKE lower(concat('%',?1,'%'))) AND acc.email = ?2  GROUP BY a.id ORDER BY a.createdAt",
                        q, email).page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
            }
            return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                    .header("X-Page-Count", pagedQuery.pageCount()).build();
        } catch (NoResultException e) {
            return Response.ok(Collections.emptyList()).header("X-Count", 0)
                .header("X-Page-Count", 0).build();
        }
    }

    @POST
    @PermitAll
    @Transactional
    public Response create(Account entity) {
        if (entity == null || entity.email == null || entity.password == null)
            return Response.status(Response.Status.BAD_REQUEST).build();

        var account = repository.findByEmail(entity.email);
        if (account != null)
            return Response.status(Response.Status.CONFLICT).build();

        String password = AES.decrypt(entity.password, secretKey);
        if (password == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        entity.password = BCrypt.withDefaults().hashToString(12, password.toCharArray());

        try {
            String token = TokenUtils.generateToken(entity.email, duration, issuer);
            entity.status = Account.Status.Online;
            repository.persist(entity);
            return Response.ok(entity).status(Response.Status.CREATED).header("X-API-Token", token).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @PUT
    @RolesAllowed("user")
    @Transactional
    public Response update(Account entity) {
        if (entity == null)
            return Response.status(Response.Status.BAD_REQUEST).build();

        Account account = repository.findById(entity.id);
        if (account == null)
            return Response.status(Response.Status.NOT_FOUND).build();

        // Validate if user change info by themself
        if (!account.email.equals(jwt.getClaim("email")))
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();

        // Update account
        if (entity.email != null)
            account.email = entity.email;
        if (entity.password != null)
            account.password = BCrypt.withDefaults().hashToString(12, entity.password.toCharArray());
        account.firstName = entity.firstName;
        account.lastName = entity.lastName;
        account.nickName = entity.nickName;
        account.address = entity.address;
        account.phone = entity.phone;
        account.avatarUrl = entity.avatarUrl;
        account.birthday = entity.birthday;
        account.gender = entity.gender;
        account.status = entity.status;
        account.contacts = entity.contacts != null ? Set.copyOf(entity.contacts) : null;
        account.favoriteContacts = entity.favoriteContacts != null ? Set.copyOf(entity.favoriteContacts) : null;
        return ApiResponse.update(account, repository, uriInfo);
    }

    @GET
    @RolesAllowed("user")
    @Path("/me")
    public Response getMe() {
        var me = repository.findByEmail(jwt.getClaim("email"));
        if (me == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.ok(me).build();
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}")
    public Response getOne(@PathParam("id") String id) {
        return ApiResponse.getOneById(id, repository);
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/friends")
    public Response getContact(@PathParam("id") String id) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.ok(entity.contacts).build();
    }

    @PATCH
    @RolesAllowed("user")
    @Path("/{id}/friends")
    @Transactional
    public Response patchContact(@PathParam("id") String id, Set<String> friendIds) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        if (friendIds == null || friendIds.size() == 0)
            return Response.notModified().build();
        for (var fid : friendIds) {
            if (!fid.equals(id)) {
                var friend = repository.findById(fid);
                if (friend != null) {
                    boolean blocked = false;
                    for (var bContact : friend.blockedContacts) {
                        if (bContact.id.equals(id)) {
                            blocked = true;
                            break;
                        }
                    }

                    if (!blocked) {
                        // Not add here, wait accepted message
                        // entity.contacts.add(friend);

                        // Add invite friend event
                        Event event = new Event();
                        event.senderId = id;
                        event.receiverId = fid;
                        event.type = Event.Type.InviteFriend;
                        event.status = Event.Status.New;
                        eventRepository.persistAndFlush(event);
                    }
                }
            }
        }
        // Return no content, friend will be added once receiver accepted the invitation
        return Response.noContent().build();
    }

    @DELETE
    @RolesAllowed("user")
    @Path("/{id}/friends/{fid}")
    @Transactional
    public Response removeContact(@PathParam("id") String id, @PathParam("fid") String fid) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        var friend = repository.findById(fid);
        if (friend == null)
            return Response.notModified().build();
        entity.contacts.remove(friend);
        entity.favoriteContacts.remove(friend);
        entity.persistAndFlush();
        return Response.noContent().build();
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/favorite_friends")
    public Response getFavoriteContact(@PathParam("id") String id) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.ok(entity.favoriteContacts).build();
    }

    @PATCH
    @RolesAllowed("user")
    @Path("/{id}/favorite_friends")
    @Transactional
    public Response patchFavoriteContact(@PathParam("id") String id, Set<String> friendIds) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        if (friendIds == null || friendIds.size() == 0)
            return Response.notModified().build();
        boolean added = false;
        for (var fid : friendIds) {
            if (!fid.equals(id)) {
                var friend = repository.findById(fid);
                if (friend != null) {
                    boolean blocked = false;
                    for (var bContact : friend.blockedContacts) {
                        if (bContact.id.equals(id)) {
                            blocked = true;
                            break;
                        }
                    }
                    if (!blocked) {
                        boolean isFriend = false;
                        for (var f : entity.contacts) {
                            if (f.id.equals(fid)) {
                                isFriend = true;
                                break;
                            }
                        }
                        if (!isFriend) {
                            // Add invite friend event
                            Event event = new Event();
                            event.senderId = id;
                            event.receiverId = fid;
                            event.type = Event.Type.InviteFriend;
                            event.status = Event.Status.New;
                            eventRepository.persistAndFlush(event);
                        } else {
                            added = true;
                            entity.favoriteContacts.add(friend);
                        }
                    }
                }
            }
        }
        if (added)
            entity.persistAndFlush();
        return Response.noContent().build();
    }

    @DELETE
    @RolesAllowed("user")
    @Path("/{id}/favorite_friends/{fid}")
    @Transactional
    public Response removeFavoriteContact(@PathParam("id") String id, @PathParam("fid") String fid) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        var friend = repository.findById(fid);
        if (friend == null)
            return Response.notModified().build();
        entity.favoriteContacts.remove(friend);
        entity.persistAndFlush();
        return Response.noContent().build();
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/blocked_friends")
    public Response getBlockedContact(@PathParam("id") String id) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.ok(entity.blockedContacts).build();
    }

    @PATCH
    @RolesAllowed("user")
    @Path("/{id}/blocked_friends")
    @Transactional
    public Response patchBlockedContact(@PathParam("id") String id, Set<String> friendIds) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        if (friendIds == null || friendIds.size() == 0)
            return Response.notModified().build();
        for (var fid : friendIds) {
            if (!fid.equals(id)) {
                var friend = repository.findById(fid);
                if (friend != null) {
                    entity.blockedContacts.add(friend);
                    entity.contacts.remove(friend);
                    entity.favoriteContacts.remove(friend);
                }
            }
        }
        entity.persistAndFlush();
        return Response.ok(entity.blockedContacts).build();
    }

    @DELETE
    @RolesAllowed("user")
    @Path("/{id}/blocked_friends/{fid}")
    @Transactional
    public Response removeBlockedContact(@PathParam("id") String id, @PathParam("fid") String fid) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        var friend = repository.findById(fid);
        if (friend == null)
            return Response.notModified().build();
        entity.blockedContacts.remove(friend);
        entity.persistAndFlush();
        return Response.noContent().build();
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/rooms")
    public Response getRooms(@PathParam("id") String id, @BeanParam PageRequest pageRequest) {
        var pagedQuery = roomRepository
                .find("select r from Room r join r.members m where m.id = ?1 ORDER BY r.createdAt", id)
                .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                .header("X-Page-Count", pagedQuery.pageCount()).build();
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/events")
    public Response getEvents(@PathParam("id") String id, @QueryParam("type") Integer type,
            @QueryParam("status") Integer status, @BeanParam PageRequest pageRequest) {
        PanacheQuery<Event> pagedQuery;
        if (type != null && status != null) {
            pagedQuery = eventRepository
                    .find("from Event e where e.receiverId = ?1 AND e.type = ?2 AND e.status = ?3",
                            Sort.by("createdAt"), id, type, status)
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        } else if (type != null) {
            pagedQuery = eventRepository
                    .find("from Event e where e.receiverId = ?1 AND e.type = ?2", Sort.by("createdAt"), id, type)
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        } else if (status != null) {
            pagedQuery = eventRepository
                    .find("from Event e where e.receiverId = ?1 AND e.status = ?2", Sort.by("createdAt"), id, status)
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        } else {
            pagedQuery = eventRepository.find("from Event e where e.receiverId = ?1", Sort.by("createdAt"), id)
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        }
        return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                .header("X-Page-Count", pagedQuery.pageCount()).build();
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/sent_events")
    public Response getSentEvents(@PathParam("id") String id, @QueryParam("type") Integer type,
            @QueryParam("status") Integer status, @BeanParam PageRequest pageRequest) {
        PanacheQuery<Event> pagedQuery;
        if (type != null && status != null) {
            pagedQuery = eventRepository.find("from Event e where e.senderId = ?1 AND e.type = ?2 AND e.status = ?3",
                    Sort.by("createdAt"), id, type, status).page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        } else if (type != null) {
            pagedQuery = eventRepository
                    .find("from Event e where e.senderId = ?1 AND e.type = ?2", Sort.by("createdAt"), id, type)
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        } else if (status != null) {
            pagedQuery = eventRepository
                    .find("from Event e where e.senderId = ?1 AND e.status = ?2", Sort.by("createdAt"), id, status)
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        } else {
            pagedQuery = eventRepository.find("from Event e where e.senderId = ?1", Sort.by("createdAt"), id)
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        }
        return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                .header("X-Page-Count", pagedQuery.pageCount()).build();
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/events/{eid}")
    public Response getEvent(@PathParam("id") String id, @PathParam("eid") String eid) {
        var event = eventRepository.findById(eid);
        if (event == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.ok(event).build();
    }

    @PATCH
    @RolesAllowed("user")
    @Path("/{id}/events/{eid}")
    @Transactional
    public Response patchEvent(@PathParam("id") String id, @PathParam("eid") String eid, Event entity) {
        if (entity == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        var me = repository.findById(id);
        var event = eventRepository.findById(eid);
        if (me == null || event == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        event.status = entity.status;
        eventRepository.persistAndFlush(event);

        // Add accepted friend to contact to both
        if (event.status == Event.Status.Accepted) {
            if (id.equals(event.receiverId)) {
                var friend = repository.findById(event.senderId);
                if (friend != null) {
                    me.contacts.add(friend);
                    friend.contacts.add(me);
                    repository.persistAndFlush(me);
                }
            }
        }
        return Response.ok(event).build();
    }

    @DELETE
    @RolesAllowed("user")
    @Path("/{id}")
    @Transactional
    public Response delete(@PathParam("id") String id) {
        var account = repository.findById(id);
        if (account == null)
            return Response.status(Response.Status.NOT_FOUND).build();

        // Validate if user change info by themself
        if (!account.email.equals(jwt.getClaim("email")))
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();

        return ApiResponse.delete(id, repository, uriInfo);
    }
}
