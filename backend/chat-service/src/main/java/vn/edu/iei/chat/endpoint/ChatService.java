package vn.edu.iei.chat.endpoint;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.control.ActivateRequestContext;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.websocket.CloseReason;
import javax.websocket.Session;

import vn.edu.iei.chat.entity.Account;
import vn.edu.iei.chat.entity.Message;
import vn.edu.iei.chat.entity.Room;
import vn.edu.iei.chat.entity.Token;
import vn.edu.iei.chat.repository.AccountRepository;
import vn.edu.iei.chat.repository.MessageRepository;
import vn.edu.iei.chat.repository.RoomRepository;
import vn.edu.iei.chat.repository.TokenRepository;

@ApplicationScoped
@ActivateRequestContext
public class ChatService {
    private static final Logger logger = Logger.getLogger(ChatService.class.getName());

    private static final Map<String, Session> sessions = new ConcurrentHashMap<>();

    @Inject
    TokenRepository tokenRepository;

    @Inject
    AccountRepository accountRepository;

    @Inject
    MessageRepository messageRepository;

    @Inject
    RoomRepository roomRepository;

    @Transactional
    public boolean validateToken(Session session, String roomId, String tokenId) {
        logger.info("Validating token: " + tokenId);

        try {
            Token token = tokenRepository.findById(tokenId);
            if (token == null) {
                logger.info("Cannot find token in db!");
                return false;
            }

            if (token.used == true) {
                logger.info("Token used!");
                return false;
            }

            // Check token expired (timeout is 5 minutes)
            if (token.createdAt.getTime() + (1000 * 60 * 5) < new Date().getTime()) {
                logger.info("Token expired!");
                token.used = true;
                token.persist();
                return false;
            }

            // Set token used
            token.used = true;
            token.persist();

            // Validate token vs room
            if (!roomId.equals(token.roomId)) {
                logger.info("Token is not for this room!");
                return false;
            }

            logger.info("Token validated!");

            // Set session info
            Account sender = accountRepository.findByEmail(token.email);
            session.getUserProperties().put("senderId", sender.id);
            session.getUserProperties().put("roomId", token.roomId);

            logger.info("Token validated, push session!");
            // Push session to map
            sessions.put(tokenId, session);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    @Transactional
    public boolean broadcastMessage(Session session, String message) {
        String senderId = (String) session.getUserProperties().get("senderId");
        String roomId = (String) session.getUserProperties().get("roomId");

        Message msg = new Message();
        msg.senderId = senderId;
        msg.roomId = roomId;
        msg.content = message;
        msg.createdAt = msg.updatedAt = new Date();
        messageRepository.persist(msg);

        Room room = roomRepository.findById(roomId);
        room.lastMessage = msg;
        roomRepository.persist(room);

        sessions.values().forEach(s -> {
            if (s.isOpen() && roomId.equals((String) session.getUserProperties().get("roomId"))) {
                s.getAsyncRemote().sendObject(msg, result -> {
                    if (result.getException() != null) {
                        logger.info("Unable to send message: " + result.getException());
                    }
                });
            }
        });
        return true;
    }

    public void onClose(Session session, String tokenId) {
        if (sessions.containsKey(tokenId)) {
            try {
                sessions.get(tokenId).close();
            } catch (IOException e) {
            }
            sessions.remove(tokenId);
        }
    }

    public void onError(Session session, String tokenId) {
        if (sessions.containsKey(tokenId)) {
            try {
                sessions.get(tokenId)
                        .close(new CloseReason(CloseReason.CloseCodes.UNEXPECTED_CONDITION, "Server error!"));
            } catch (IOException e) {
            }
            sessions.remove(tokenId);
        }
    }
}