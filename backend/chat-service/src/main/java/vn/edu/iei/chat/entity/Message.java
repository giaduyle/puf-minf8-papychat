package vn.edu.iei.chat.entity;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import org.hibernate.annotations.GenericGenerator;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Cacheable
@JsonInclude(Include.NON_NULL)
public class Message extends PanacheEntityBase implements BaseEntity {
    /**
     * Priority: enum of message priority
     */
    public static class Priority {
        public static final int Low = 0;
        public static final int Medium = 1;
        public static final int High = 2;
        public static final int Critical = 3;
    }

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "message_id", updatable = false, nullable = false)
    public String id;

    @Column(length = 4096)
    public String content;

    public String senderId;

    public String roomId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    @Temporal(TemporalType.TIMESTAMP)
    public Date createdAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    @Temporal(TemporalType.TIMESTAMP)
    public Date updatedAt;

    public String getId() {
        return id;
    }
}