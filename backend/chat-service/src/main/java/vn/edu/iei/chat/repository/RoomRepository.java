package vn.edu.iei.chat.repository;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import vn.edu.iei.chat.entity.Room;

@ApplicationScoped
public class RoomRepository implements PanacheRepositoryBase<Room, String> {
}