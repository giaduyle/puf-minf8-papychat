package vn.edu.iei.chat.utils;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import vn.edu.iei.chat.entity.BaseEntity;

public class ApiResponse {
    public static <T extends BaseEntity> Response getAll(PageRequest pageRequest,
            PanacheRepositoryBase<T, String> repository) {
        PanacheQuery<T> pagedQuery;
        try {
            pagedQuery = repository.find("createdAt <= ?1", Sort.by("createdAt"), pageRequest.getBeforeAsDate())
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        } catch (Exception e) {
            pagedQuery = repository.findAll(Sort.by("createdAt"))
                    .page(Page.of(pageRequest.pageNum, pageRequest.pageSize));
        }
        return Response.ok(pagedQuery.list()).header("X-Count", pagedQuery.count())
                .header("X-Page-Count", pagedQuery.pageCount()).build();
    }

    public static <T extends BaseEntity> Response getOneById(String id, PanacheRepositoryBase<T, String> repository) {
        var entity = repository.findById(id);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.ok(entity).build();
    }

    public static <T extends BaseEntity> Response getOneByField(String field,
            PanacheRepositoryBase<T, String> repository, Object... values) {
        var entity = repository.find(field, values);
        if (entity == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.ok(entity).build();
    }

    public static <T extends BaseEntity> Response create(T entity, PanacheRepositoryBase<T, String> repository,
            UriInfo uriInfo) {
        if (entity == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        try {
            repository.persist(entity);
            return Response.ok(entity).status(Response.Status.CREATED).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    public static <T extends BaseEntity> Response update(T entity, PanacheRepositoryBase<T, String> repository,
            UriInfo uriInfo) {
        if (entity == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        try {
            repository.persist(entity);
            return Response.ok(entity).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    public static <T extends BaseEntity> Response delete(String id, PanacheRepositoryBase<T, String> repository,
            UriInfo uriInfo) {
        var deleted = repository.deleteById(id);
        if (!deleted)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}