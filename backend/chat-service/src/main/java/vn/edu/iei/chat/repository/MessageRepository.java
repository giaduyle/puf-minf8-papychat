package vn.edu.iei.chat.repository;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import vn.edu.iei.chat.entity.Message;

@ApplicationScoped
public class MessageRepository implements PanacheRepositoryBase<Message, String> {
}