package vn.edu.iei.chat.api;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import vn.edu.iei.chat.entity.Media;
import vn.edu.iei.chat.repository.MediaRepository;
import vn.edu.iei.chat.utils.ApiResponse;
import vn.edu.iei.chat.utils.PageRequest;

@Path("/api/v1/medias")
public class MediaController {
    @Inject
    MediaRepository repository;

    @Context
    UriInfo uriInfo;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("user")
    public Response getAll(@BeanParam PageRequest pageRequest) {
        return ApiResponse.getAll(pageRequest, repository);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("user")
    @Transactional
    public Response create(Media entity) {
        if (entity == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        return ApiResponse.create(entity, repository, uriInfo);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("user")
    @Transactional
    public Response update(Media entity) {
        if (entity == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        Media media = repository.findById(entity.id);
        if (media == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        media.name = entity.name;
        media.contentType = entity.contentType;
        media.content = Arrays.copyOf(entity.content, entity.content.length);
        return ApiResponse.update(media, repository, uriInfo);
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("user")
    @Path("/{id}")
    public Response getOne(@PathParam("id") String id) {
        return ApiResponse.getOneById(id, repository);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("user")
    @Path("/{id}")
    @Transactional
    public Response deleteOne(@PathParam("id") String id) {
        return ApiResponse.delete(id, repository, uriInfo);
    }

    @GET
    @RolesAllowed("user")
    @Path("/{id}/download")
    @Transactional
    public Response download(@PathParam("id") String id) {
        Media media = repository.findById(id);
        if (media == null)
            Response.status(Response.Status.NOT_FOUND).build();

        byte[] content = media.content;
        ResponseBuilder response = Response.ok((Object) content);
        response.header("Content-Disposition", "attachment;filename=" + media.name);
        response.header("Content-Type", media.contentType);
        return response.build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("user")
    @Transactional
    public Response upload(MultipartFormDataInput input) {
        Set<Media> medias = new HashSet<>();
        if (input == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        for (String key : uploadForm.keySet()) {
            // Get file data to save
            List<InputPart> inputParts = uploadForm.get(key);
            for (InputPart inputPart : inputParts) {
                try {
                    MultivaluedMap<String, String> header = inputPart.getHeaders();
                    String contentType = header.getFirst("Content-Type");
                    String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
                    String fileName = "";
                    for (String contentKey : contentDisposition) {
                        if ((contentKey.trim().startsWith("filename"))) {
                            String[] name = contentKey.split("=");
                            fileName = name[1].trim().replaceAll("\"", "");
                            break;
                        }
                    }

                    // convert the uploaded file to inputstream
                    InputStream inputStream = inputPart.getBody(InputStream.class, null);
                    byte[] bytes = IOUtils.toByteArray(inputStream);

                    Media media = new Media();
                    media.name = fileName;
                    media.contentType = contentType;
                    media.content = bytes;
                    repository.persist(media);
                    medias.add(media);
                } catch (Exception e) {
                }
            }
        }
        return Response.ok(medias).build();
    }
}
