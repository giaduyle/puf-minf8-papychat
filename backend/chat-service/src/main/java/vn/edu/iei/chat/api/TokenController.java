package vn.edu.iei.chat.api;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.jwt.JsonWebToken;

import vn.edu.iei.chat.entity.Account;
import vn.edu.iei.chat.entity.Room;
import vn.edu.iei.chat.entity.Token;
import vn.edu.iei.chat.repository.RoomRepository;
import vn.edu.iei.chat.repository.TokenRepository;

@Path("/api/v1/tokens")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TokenController {
    @Inject
    JsonWebToken jwt;

    @Inject
    TokenRepository repository;

    @Inject
    RoomRepository roomRepository;

    @GET
    @Path("/{room_id}")
    @Transactional
    public Response getToken(@PathParam("room_id") String id) {
        // Check if room exists
        Room room = roomRepository.findById(id);
        if(room == null) Response.status(Response.Status.NOT_FOUND).build();

        // Check if user allow to join the room
        String email = jwt.getClaim("email");
        boolean allowed = !room.isPrivate;
        if(!allowed) {
            for(Account member: room.members) {
                if(member.email == email) {
                    allowed = true;
                    break;
                }
            }
        }
        if(!allowed) Response.status(Response.Status.FORBIDDEN).build();

        // Create token, and save to database
        Token token = new Token();
        token.email = email;
        token.roomId = id;
        token.used = false;
        repository.persist(token);

        // Return as json object
        JsonObject jsonToken = Json.createObjectBuilder()
            .add("token", token.id)
            .build();
        return Response.ok(jsonToken).build();
    }
}
