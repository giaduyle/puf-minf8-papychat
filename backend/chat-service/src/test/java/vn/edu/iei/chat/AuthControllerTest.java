package vn.edu.iei.chat;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@QuarkusTest
public class AuthControllerTest {
    static String accessToken = null;

    @BeforeEach
    void setup() {
        RestAssured.baseURI = "https://localhost";
        RestAssured.port = 8444;
        RestAssured.useRelaxedHTTPSValidation();
    }

    @Test
    public void testUserAuth() {
        Response response = RestAssured.given().urlEncodingEnabled(true).param("email", "test01@iei.vn")
                .param("password", "U2FsdGVkX198nZbfTO71Ph0m7Mcxp/U7GAC8hPk7qVo=")
                .header("Accept", ContentType.JSON.getAcceptHeader()).get("/api/v1/auth/login").then().statusCode(200)
                .extract().response();
        JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        JsonObject object = jsonReader.readObject();
        accessToken = object.getString("token");
        Assertions.assertTrue(accessToken.length() > 0);

        response = RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/users/me").then().statusCode(200)
                .extract().response();
        jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        object = jsonReader.readObject();
        Assertions.assertTrue(object.getString("email").equals("test01@iei.vn"));
    }

    @Test
    public void testRefresh() {
        Assertions.assertTrue(accessToken.length() > 0);
        Response response = RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/auth/refresh").then()
                .statusCode(200).extract().response();
        JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        JsonObject object = jsonReader.readObject();
        String newAccessToken = object.getString("token");
        Assertions.assertTrue(newAccessToken.length() > 0 && !newAccessToken.equals(accessToken));

        accessToken = newAccessToken;
        response = RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/users/me").then().statusCode(200)
                .extract().response();
        jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        object = jsonReader.readObject();
        Assertions.assertTrue(object.getString("email").equals("test01@iei.vn"));
    }
}