package vn.edu.iei.chat;

import static org.hamcrest.Matchers.is;

import java.io.File;
import java.io.StringReader;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import vn.edu.iei.chat.utils.AES;

@QuarkusTest
public class MediaControllerTest {
    @ConfigProperty(name = "vn.edu.iei.chat.secret")
    String secretKey;

    static String accessToken = null;
    static String mediaId = null;

    private static final Logger logger = Logger.getLogger(MediaControllerTest.class.getName());

    @BeforeEach
    void setup() {
        RestAssured.baseURI = "https://localhost";
        RestAssured.port = 8444;
        RestAssured.useRelaxedHTTPSValidation();
    }

    public void login() {
        if (accessToken == null) {
            Response response = RestAssured.given().urlEncodingEnabled(true).param("email", "test01@iei.vn")
                    .param("password", AES.encrypt("P@ssw0rd", secretKey))
                    .header("Accept", ContentType.JSON.getAcceptHeader()).get("/api/v1/auth/login").then()
                    .statusCode(200).extract().response();
            JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
            JsonObject object = jsonReader.readObject();
            accessToken = new String(object.getString("token"));
            Assertions.assertTrue(accessToken != null && accessToken.length() > 0);
        }
    }

    @Test
    public void testUpload() {
        login();

        if (mediaId == null) {
            File avatarFile = new File("./src/test/resources/avatar.png");

            Response response = RestAssured.given().auth().oauth2(accessToken).when()
                    .multiPart("avatar", avatarFile, "application/octet-stream").post("/api/v1/medias").then()
                    .statusCode(200).body("$.size()", is(1)).extract().response();

            JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
            JsonArray jsonArray = jsonReader.readArray();
            Assertions.assertTrue(jsonArray != null && jsonArray.size() == 1);
            JsonObject jsobObject = jsonArray.get(0).asJsonObject();
            Assertions.assertTrue(jsobObject != null && jsobObject.getString("name").equals("avatar.png"));
            mediaId = jsobObject.getString("id");
            Assertions.assertTrue(mediaId != null && mediaId.length() > 0);
        }
    }

    @Test
    public void testGet() {
        testUpload();

        Response response = RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/medias/" + mediaId)
                .then().statusCode(200).extract().response();

        JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        JsonObject jsonObject = jsonReader.readObject();
        Assertions.assertTrue(jsonObject != null && jsonObject.getString("name").equals("avatar.png"));
    }

    @Test
    public void testDownload() {
        testUpload();

        Response response = RestAssured.given().auth().oauth2(accessToken).when()
                .get("/api/v1/medias/" + mediaId + "/download").then().statusCode(200).extract().response();

        Assertions.assertTrue(response.getHeader("Content-Type").equals("application/octet-stream"));
    }

}