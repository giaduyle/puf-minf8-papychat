package vn.edu.iei.chat;

import static org.hamcrest.Matchers.is;

import java.io.StringReader;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import vn.edu.iei.chat.utils.AES;

@QuarkusTest
public class RoomControllerTest {
    private static final Logger logger = Logger.getLogger(RoomControllerTest.class.getName());
    static String accessToken = null;
    static String roomId = null;
    static JsonObject roomObj = null;

    @ConfigProperty(name = "vn.edu.iei.chat.secret")
    String secretKey;

    @BeforeEach
    void setup() {
        RestAssured.baseURI = "https://localhost";
        RestAssured.port = 8444;
        RestAssured.useRelaxedHTTPSValidation();
    }

    @Test
    public void loginRoom() {
        if (accessToken == null) {
            Response response = RestAssured.given().urlEncodingEnabled(true).param("email", "test01@iei.vn")
                    .param("password", AES.encrypt("P@ssw0rd", secretKey))
                    .header("Accept", ContentType.JSON.getAcceptHeader()).get("/api/v1/auth/login").then()
                    .statusCode(200).extract().response();
            JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
            JsonObject object = jsonReader.readObject();
            accessToken = new String(object.getString("token"));
            Assertions.assertTrue(accessToken != null && accessToken.length() > 0);
        }
    }

    @Test
    public void createRoom() {
        loginRoom();
        if (roomId == null) {
            JsonObject jsonEntity = Json.createObjectBuilder().add("name", "Test room")
                    .add("description", "This is a test room!").add("isPrivate", true).build();
            Response response = RestAssured.given().auth().oauth2(accessToken).contentType("application/json")
                    .body(jsonEntity.toString()).when().post("/api/v1/rooms").then().statusCode(201).extract()
                    .response();
            JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
            JsonObject object = jsonReader.readObject();
            Assertions.assertTrue(object != null && object.getString("name").equals("Test room"));
            roomId = object.getString("id");
        }
    }

    @Test
    public void getRoom() {
        if (roomObj == null) {
            createRoom();
            Response response = RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/rooms/" + roomId)
                    .then().statusCode(200).extract().response();
            JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
            roomObj = jsonReader.readObject();
            Assertions.assertTrue(roomObj != null && roomObj.getString("name").equals("Test room"));
        }
    }

    @Test
    public void updateRoom() {
        getRoom();
        JsonObject newObj = insertValue(roomObj, "name", "Updated Test Room");

        RestAssured.given().auth().oauth2(accessToken).contentType("application/json").body(newObj.toString()).when()
                .put("/api/v1/rooms").then().statusCode(200);

        Response response = RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/rooms/" + roomId).then()
                .statusCode(200).extract().response();
        JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        roomObj = jsonReader.readObject();
        Assertions.assertTrue(roomObj != null && roomObj.getString("name").equals("Updated Test Room"));
    }

    @Test
    public void getMessages() {
        getRoom();

        RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/rooms/" + roomId + "/messages").then()
                .statusCode(200).body("$.size()", is(0));

        RestAssured.given().auth().oauth2(accessToken).when()
                .get("/api/v1/rooms/" + "e7616832-bb4e-470a-8df4-0534ab56d960" + "/messages").then().statusCode(200)
                .body("$.size()", is(2));
    }

    @Test
    public void getMembers() {
        getRoom();

        RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/rooms/" + roomId + "/members").then()
                .statusCode(200).body("$.size()", is(1));

        RestAssured.given().auth().oauth2(accessToken).when()
                .get("/api/v1/rooms/" + "e7616832-bb4e-470a-8df4-0534ab56d960" + "/members").then().statusCode(200)
                .body("$.size()", is(6));
    }

    @Test
    public void addRemoveMembers() {
        getRoom();

        JsonArray jsonArray = Json.createArrayBuilder().add("2a69ec7d-a147-4c71-8a20-9ba760de0150")
                .add("2a69ec7d-a147-4c71-8a20-9ba760de0151").add("2a69ec7d-a147-4c71-8a20-9ba760de0152").build();

        RestAssured.given().auth().oauth2(accessToken).contentType("application/json").body(jsonArray.toString()).when()
                .put("/api/v1/rooms/" + roomId + "/members").then().statusCode(200);

        RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/rooms/" + roomId + "/members").then()
                .statusCode(200).body("$.size()", is(3));

        RestAssured.given().auth().oauth2(accessToken).contentType("application/json").when()
                .delete("/api/v1/rooms/" + roomId + "/members/2a69ec7d-a147-4c71-8a20-9ba760de0151").then()
                .statusCode(204);

        RestAssured.given().auth().oauth2(accessToken).when().get("/api/v1/rooms/" + roomId + "/members").then()
                .statusCode(200).body("$.size()", is(2));
    }

    /**
     * Helper to change value in JsonObject
     */
    private JsonObject insertValue(JsonObject source, String key, String value) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add(key, value);
        source.entrySet().forEach(e -> {
            if (!key.equals(e.getKey()))
                builder.add(e.getKey(), e.getValue());
        });
        return builder.build();
    }
}