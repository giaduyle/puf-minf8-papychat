package vn.edu.iei.chat;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeChatServiceIT extends ChatControllerTest {

    // Execute the same tests but in native mode.
}