package vn.edu.iei.chat;

import static org.hamcrest.Matchers.is;

import java.io.StringReader;
import java.net.URI;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import vn.edu.iei.chat.entity.Message;
import vn.edu.iei.chat.utils.MessageDecoder;

@QuarkusTest
public class ChatControllerTest {
    private static final LinkedBlockingDeque<String> MESSAGES = new LinkedBlockingDeque<>();
    private static final Logger logger = Logger.getLogger(ChatControllerTest.class.getName());

    @BeforeEach
    void setup() {
        RestAssured.baseURI = "https://localhost";
        RestAssured.port = 8444;
        RestAssured.useRelaxedHTTPSValidation();
    }

    @Test
    public void testWebsocketChat() {
        Response response = RestAssured.given().urlEncodingEnabled(true).param("email", "test01@iei.vn")
                .param("password", "U2FsdGVkX198nZbfTO71Ph0m7Mcxp/U7GAC8hPk7qVo=")
                .header("Accept", ContentType.JSON.getAcceptHeader()).get("/api/v1/auth/login").then().statusCode(200)
                .extract().response();
        JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        JsonObject object = jsonReader.readObject();
        String userToken = object.getString("token");

        response = RestAssured.given().auth().oauth2(userToken).when()
                .get("/api/v1/tokens/e7616832-bb4e-470a-8df4-0534ab56d961").then().statusCode(200).extract().response();
        jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        String tokenUuid = jsonReader.readObject().getString("token");

        try {
            URI uri = new URI("ws://localhost:8081/chat");
            uri = uri.resolve(uri.getPath() + "/e7616832-bb4e-470a-8df4-0534ab56d961/" + tokenUuid);
            logger.info("URI: " + uri.getPath());

            try (Session session = ContainerProvider.getWebSocketContainer().connectToServer(Client.class, uri)) {
                Assertions.assertEquals("CONNECT", MESSAGES.poll(10, TimeUnit.SECONDS));
                Assertions.assertEquals("_ready_", MESSAGES.poll(10, TimeUnit.SECONDS));
                session.getAsyncRemote().sendText("hello world");
                Assertions.assertEquals("hello world", MESSAGES.poll(10, TimeUnit.SECONDS));

                var msgResponse = RestAssured.given().auth().oauth2(userToken).when()
                        .get("/api/v1/rooms/" + "e7616832-bb4e-470a-8df4-0534ab56d961" + "/messages").then()
                        .statusCode(200).body("$.size()", is(2)).extract().response();
                msgResponse.body().prettyPrint();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ClientEndpoint(decoders = { MessageDecoder.class })
    public static class Client {
        @OnOpen
        public void open(Session session) {
            MESSAGES.add("CONNECT");
            // Send a message to indicate that we are ready,
            // as the message handler may not be registered immediately after this callback.
            session.getAsyncRemote().sendText("_ready_");
        }

        @OnMessage
        void message(Message message) {
            final ObjectMapper mapper = new ObjectMapper();
            try {
                logger.info("Client onMessage: " + mapper.writeValueAsString(message));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            MESSAGES.add(message.content);
        }

    }
}