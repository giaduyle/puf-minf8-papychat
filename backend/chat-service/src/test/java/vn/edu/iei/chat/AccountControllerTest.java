package vn.edu.iei.chat;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

import java.io.StringReader;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import vn.edu.iei.chat.utils.AES;

@QuarkusTest
public class AccountControllerTest {
    private static final Logger logger = Logger.getLogger(AccountControllerTest.class.getName());
    static ObjectMapper mapper = new ObjectMapper();
    static boolean created = false;
    static String accessToken = null;

    @ConfigProperty(name = "vn.edu.iei.chat.secret")
    String secretKey;

    @BeforeEach
    void setup() {
        RestAssured.baseURI = "https://localhost";
        RestAssured.port = 8444;
        RestAssured.useRelaxedHTTPSValidation();
    }

    @Test
    public void create() {
        if (!created) {

            String password = AES.encrypt("P@ssw0rd", secretKey);
            JsonObject jsonEntity = Json.createObjectBuilder().add("email", "test@iei.vn").add("password", password)
                    .build();
            given().contentType("application/json").body(jsonEntity.toString()).when().post("/api/v1/users").then()
                    .statusCode(201);
            created = true;
        }
    }

    @Test
    public String login() {
        if (accessToken == null) {
            create();
            Response response = given().urlEncodingEnabled(true).param("email", "test@iei.vn")
                    .param("password", AES.encrypt("P@ssw0rd", secretKey))
                    .header("Accept", ContentType.JSON.getAcceptHeader()).get("/api/v1/auth/login").then()
                    .statusCode(200).extract().response();
            JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
            JsonObject object = jsonReader.readObject();
            accessToken = new String(object.getString("token"));
            Assertions.assertTrue(accessToken != null && accessToken.length() > 0);
        }
        return accessToken;
    }

    @Test
    public JsonObject getMe() {
        String accessToken = login();
        Assertions.assertTrue(accessToken != null && accessToken.length() > 0);

        Response response = given().auth().oauth2(accessToken).when().get("/api/v1/users/me").then().statusCode(200)
                .extract().response();
        JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        JsonObject object = jsonReader.readObject();
        Assertions.assertTrue(object.getString("email").equals("test@iei.vn"));
        return object;
    }

    @Test
    public void pagedList() {
        String accessToken = login();
        given().auth().oauth2(accessToken).when().get("/api/v1/users?pageNum=0&pageSize=2").then().statusCode(200).body(
                "$.size()", is(2), "firstName", containsInAnyOrder("Duy", "Hong"), "lastName",
                containsInAnyOrder("Le", "Truong"));
    }

    @Test
    public void search() {
        String accessToken = login();
        given().auth().oauth2(accessToken).when().get("/api/v1/users?q=Hong&pageNum=0&pageSize=2").then()
                .statusCode(200).body("$.size()", is(1), "firstName", containsInAnyOrder("Hong"), "lastName",
                        containsInAnyOrder("Truong"));
    }

    @Test
    public void update() {
        String accessToken = login();
        JsonObject jsonObject = getMe();
        JsonObject jsonEntity = insertValue(jsonObject, "firstName", "Updated Test");

        Response response = given().auth().oauth2(accessToken).contentType("application/json")
                .body(jsonEntity.toString()).when().put("/api/v1/users").then().statusCode(200).extract().response();
        ;
        JsonReader jsonReader = Json.createReader(new StringReader(response.getBody().asString()));
        jsonObject = jsonReader.readObject();
        Assertions.assertTrue(jsonObject.getString("firstName").equals("Updated Test"));
    }

    @Test
    public void getFriends() {
        String accessToken = login();
        JsonObject jsonObject = getMe();
        given().auth().oauth2(accessToken).when().get("/api/v1/users/" + jsonObject.getString("id") + "/friends").then()
                .statusCode(200).body("$.size()", is(0));
    }

    @Test
    public void addRemoveFriends() {
        String accessToken = login();
        JsonObject jsonObject = getMe();
        JsonArray jsonArray = Json.createArrayBuilder().add("2a69ec7d-a147-4c71-8a20-9ba760de0150")
                .add("2a69ec7d-a147-4c71-8a20-9ba760de0151").add("2a69ec7d-a147-4c71-8a20-9ba760de0152").build();
        given().auth().oauth2(accessToken).contentType("application/json").body(jsonArray.toString()).when()
                .patch("/api/v1/users/" + jsonObject.getString("id") + "/friends").then().statusCode(204);
        given().auth().oauth2(accessToken).when().get("/api/v1/users/" + jsonObject.getString("id") + "/friends").then()
                .statusCode(200).body("$.size()", is(0));
        given().auth().oauth2(accessToken).contentType("application/json").when()
                .delete("/api/v1/users/" + jsonObject.getString("id") + "/friends/2a69ec7d-a147-4c71-8a20-9ba760de0151")
                .then().statusCode(204);
        given().auth().oauth2(accessToken).when().get("/api/v1/users/" + jsonObject.getString("id") + "/friends").then()
                .statusCode(200).body("$.size()", is(0));
    }

    /**
     * Helper to change value in JsonObject
     */
    private JsonObject insertValue(JsonObject source, String key, String value) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add(key, value);
        source.entrySet().forEach(e -> builder.add(e.getKey(), e.getValue()));
        return builder.build();
    }
}