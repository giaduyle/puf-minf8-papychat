docker network create --driver bridge pgnetwork

docker run -it --rm=true \
    --name pgadmin --network=pgnetwork \
    -e PGADMIN_DEFAULT_EMAIL=admin@iei.vn \
    -e PGADMIN_DEFAULT_PASSWORD=P@ssw0rd \
    -e PGADMIN_LISTEN_PORT=5050 \
    -p 5050:5050 \
    -d dpage/pgadmin4:4.24

docker run --ulimit memlock=-1:-1 -it --rm=true --memory-swappiness=0 \
    --name chat-service-db \
    --network=pgnetwork \
    -e POSTGRES_USER=papyrus \
    -e POSTGRES_PASSWORD=papyrus \
    -e POSTGRES_DB=chat-service-db \
    -p 5432:5432 \
    postgres:12.3

docker stop pgadmin
