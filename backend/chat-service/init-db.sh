#!/bin/bash
#
# set_credential_envs_from_url <database_url> <env_prefix>
#
# Example:#
#   set_credential_envs_from_url 'proto://user:pass@host:1234/db' 'DATABASE'
#
# Exports the following variables:
#   DATABASE_PROTO DATABASE_USER DATABASE_PASS DATABASE_HOST DATABASE_PORT DATABASE_NAME
#
set_credential_envs_from_url () {
  # extract the protocol
  var_name_proto="$2_PROTO"
  proto="`echo $1 | grep '://' | sed -e's,^\(.*://\).*,\1,g'`"
  # remove the protocol
  url=`echo $1| sed -e s,$proto,,g`
  export $var_name_proto=$proto

  # extract the user and password (if any)
  userpass="`echo $url | grep @ | cut -d@ -f1`"

  var_name_pass="$2_PASS"
  export $var_name_pass=`echo $userpass | grep : | cut -d: -f2`

  var_name_user="$2_USER"
  if [ -n "$var_name_pass" ]; then
      export $var_name_user=`echo $userpass | grep : | cut -d: -f1`
  else
      export $var_name_user=$userpass
  fi

  # extract the host -- updated
  hostport=`echo $url | sed -e s,$userpass@,,g | cut -d/ -f1`

  var_name_port="$2_PORT"
  export $var_name_port=`echo $hostport | grep : | cut -d: -f2`

  var_name_host="$2_HOST"
  if [ -n "$var_name_port" ]; then
      export $var_name_host=`echo $hostport | grep : | cut -d: -f1`
  else
      export $var_name_host=$hostport
  fi

  # extract the path (if any)
  var_name="$2_NAME"
  export $var_name="`echo $url | grep / | cut -d/ -f2-`"
}

set_credential_envs_from_url $1 DATABASE

export JDBC_DATABASE_URL="jdbc:postgresql://${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}"
export JDBC_DATABASE_USERNAME=$DATABASE_USER
export JDBC_DATABASE_PASSWORD=$DATABASE_PASS
