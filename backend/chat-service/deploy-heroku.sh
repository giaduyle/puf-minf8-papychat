#!/bin/bash

chmod +x ./init-db.sh
source ./init-db.sh $(heroku config:get DATABASE_URL -a papy-chat-service)

echo $JDBC_DATABASE_URL
echo $JDBC_DATABASE_USERNAME
echo $JDBC_DATABASE_PASSWORD

export PORT=8080

mvn package
docker build -f src/main/docker/Dockerfile.jvm -t quarkus/papy-chat-service .

heroku container:login
docker tag quarkus/papy-chat-service registry.heroku.com/papy-chat-service/web
docker push registry.heroku.com/papy-chat-service/web
heroku container:release web -a papy-chat-service
