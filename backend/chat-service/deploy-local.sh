#!/bin/bash

chmod +x ./init-db.sh
source ./init-db.sh $(heroku config:get DATABASE_URL -a papy-chat-service)

echo $JDBC_DATABASE_URL
echo $JDBC_DATABASE_USERNAME
echo $JDBC_DATABASE_PASSWORD

export PORT=8080

mvn package
docker build -f src/main/docker/Dockerfile.jvm -t quarkus/papy-chat-service .
docker run -i --rm --name papy-chat-service -e PORT=8080 -p 8080:8080 quarkus/papy-chat-service
