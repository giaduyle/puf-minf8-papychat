mvn io.quarkus:quarkus-maven-plugin:1.6.0.Final:create ^
    -D projectGroupId=vn.edu.iei.chat ^
    -D projectArtifactId=chat-service ^
    -D className="vn.edu.iei.chat.ChatService" ^
    -D path="/api/v1"

pushd chat-service
mvnw quarkus:list-extensions

.\mvnw quarkus:add-extension -D extensions="quarkus-hibernate-orm-panache"

.\mvnw quarkus:add-extension -D extensions="quarkus-rest-client-jackson"

.\mvnw quarkus:add-extension -D extensions="quarkus-jdbc-postgresql"

.\mvnw quarkus:add-extension -D extensions="undertow-websockets"
popd
