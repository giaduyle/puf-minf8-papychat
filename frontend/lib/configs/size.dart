import 'package:flutter/material.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double defaultSize;
  static Orientation orientation;

  static SizeConfig of(BuildContext context) => SizeConfig._(context);

  SizeConfig._(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    orientation = _mediaQueryData.orientation;
  }
}

// Get the percentage of height as per screen size
double getPercentScreenHeight(double value) {
  if (value.abs() < 0 || value.abs() > 1) {
    throw new ArgumentError.value(
        value, "value", "Should be from (-)0.0 to (-)1.0");
  }
  return value * SizeConfig.screenHeight;
}

// Get the percentage of height as per screen size
double getPercentScreenWidth(double value) {
  if (value.abs() < 0 || value.abs() > 1) {
    throw new ArgumentError.value(
        value, "value", "Should be from (-)0.0 to (-)1.0");
  }
  return value * SizeConfig.screenWidth;
}

// Get the proportionate height as per screen size
double getProportionateScreenHeight(double inputHeight) {
  // 812 is the layout height that designer use
  return (inputHeight / 812.0) * SizeConfig.screenHeight;
}

// Get the proportionate height as per screen size
double getProportionateScreenWidth(double inputWidth) {
  // 375 is the layout width that designer use
  return (inputWidth / 375.0) * SizeConfig.screenWidth;
}
