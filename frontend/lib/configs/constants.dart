import 'dart:io';

const String kStickerPrefix = "sticker::";
const String kMediaPrefix = "media::";
const String kAssetMediaMessage = "${kMediaPrefix}asset::";
final Map<String, String> kDefaultHeaders = {
  HttpHeaders.acceptHeader: ContentType.json.mimeType,
  HttpHeaders.contentTypeHeader: ContentType.json.mimeType
};
const String kValidPasswordPattern = r'(?=.*?[#?!@$%^&*-])';
const String kValidPhonePattern =
    r'(^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$)';
