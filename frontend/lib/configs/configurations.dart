import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:papyrus_chat_app/controllers/auth_controller.dart';
import 'package:papyrus_chat_app/controllers/media_controller.dart';
import 'package:papyrus_chat_app/controllers/room_controller.dart';
import 'package:papyrus_chat_app/controllers/sticker_controller.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/api/auth_api.dart';
import 'package:papyrus_chat_app/core/api/call_api.dart';
import 'package:papyrus_chat_app/core/api/chat_api.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';
import 'package:papyrus_chat_app/core/api/media_api.dart';
import 'package:papyrus_chat_app/core/api/room_api.dart';
import 'package:papyrus_chat_app/core/api/user_api.dart';
import 'package:papyrus_chat_app/core/models/event.dart';
import 'package:papyrus_chat_app/core/models/media.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/event_repository.dart';
import 'package:papyrus_chat_app/core/repositories/media_repository.dart';
import 'package:papyrus_chat_app/core/repositories/room_repository.dart';
import 'package:papyrus_chat_app/core/repositories/sticker_repository.dart';
import 'package:papyrus_chat_app/core/repositories/token_repository.dart';
import 'package:papyrus_chat_app/core/repositories/user_repository.dart';

Future<void> initConfigurations({String environment}) async {
  await _loadSettings(environment: environment);
  await initStorageEngine();
  await registerAdapters();
  await initHttpClient();
  await registerRepositories();
  await registerApis();
  await registerControllers();
}

Future<void> _loadSettings({String environment}) async {
  String env = environment ?? getCurrentEnvironment();
  WidgetsFlutterBinding.ensureInitialized();
  switch (env) {
    case 'prod':
      await GlobalConfiguration().loadFromAsset("server_settings");
      return;
    case 'mock':
      await GlobalConfiguration().loadFromAsset("server_settings_mock");
      return;
    case 'dev':
      await GlobalConfiguration().loadFromAsset("server_settings_dev");
      return;
  }
}

Future<void> initHttpClient() async {
  Get.lazyPut<HttpFacade>(() {
    GlobalConfiguration configs = GlobalConfiguration();
    return HttpFacade("${configs.get("host")}:${configs.get("port")}");
  });
}

Future<void> initStorageEngine() async {
  await Hive.initFlutter();
}

Future<void> registerAdapters() async {
  Hive.registerAdapter(EventAdapter());
  Hive.registerAdapter(EventTypeAdapter());
  Hive.registerAdapter(EventStatusAdapter());
  Hive.registerAdapter(MediaAdapter());
  Hive.registerAdapter(MessageAdapter());
  Hive.registerAdapter(RoomAdapter());
  Hive.registerAdapter(UserAdapter());
  Hive.registerAdapter(UserGenderAdapter());
  Hive.registerAdapter(UserStatusAdapter());
}

Future<void> registerRepositories() async {
  Get.lazyPut<StickerRepository>(() => StickerRepository());
  await Get.putAsync<EventRepository>(EventRepository.of);
  await Get.putAsync<MediaRepository>(MediaRepository.of);
  await Get.putAsync<RoomRepository>(RoomRepository.of);
  await Get.putAsync<TokenRepository>(TokenRepository.of);
  await Get.putAsync<UserRepository>(UserRepository.of);
}

Future<void> registerApis() async {
  Get.lazyPut<AuthApi>(() => AuthApi());
  Get.lazyPut<CallApi>(() => CallApi());
  Get.lazyPut<ChatApi>(() => ChatApi());
  Get.lazyPut<MediaApi>(() => MediaApi());
  Get.lazyPut<RoomApi>(() => RoomApi());
  Get.lazyPut<UserApi>(() => UserApi());
}

Future<void> registerControllers() async {
  Get.lazyPut<AuthController>(() => AuthController());
  Get.lazyPut<RoomController>(() => RoomController());
  Get.lazyPut<StickerController>(() => StickerController());
  Get.lazyPut<UserController>(() => UserController());
  Get.lazyPut<MediaController>(() => MediaController());
}

String getCurrentEnvironment() {
  return const String.fromEnvironment('ENV', defaultValue: 'dev');
}
