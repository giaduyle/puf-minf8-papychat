import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/views/screens/login_screen.dart';
import 'package:papyrus_chat_app/views/screens/main_screen.dart';
import 'package:papyrus_chat_app/views/screens/onboard_screen.dart';
import 'package:papyrus_chat_app/views/screens/register_screen.dart';

const String MainRoute = "/";
const String OnBoardRoute = "/onboard";
const String LoginRoute = "/login";
const String RegisterRoute = "/register";

// ignore: non_constant_identifier_names
final Map<String, WidgetBuilder> PRRoutes =
    Map<String, WidgetBuilder>.unmodifiable({
  OnBoardRoute: (context) => OnBoardScreen(),
  LoginRoute: (context) => LoginScreen(),
  RegisterRoute: (context) => RegisterScreen(),
  MainRoute: (context) => MainScreen(),
});
