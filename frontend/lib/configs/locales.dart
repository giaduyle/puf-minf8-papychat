import 'package:duration/locale.dart';
import 'package:easy_localization/easy_localization.dart';

class AutoTranslatedDurationLocale implements DurationLocale {
  @override
  String day(int amount, [bool abbreviated = true]) {
    if (abbreviated) {
      return "abbreviated_day".tr();
    }
    return "day".plural(amount);
  }

  @override
  String hour(int amount, [bool abbreviated = true]) {
    if (abbreviated) {
      return "abbreviated_hour".tr();
    }
    return "hour".plural(amount);
  }

  @override
  String microseconds(int amount, [bool abbreviated = true]) {
    if (abbreviated) {
      return "abbreviated_microseconds".tr();
    }
    return "microseconds".plural(amount);
  }

  @override
  String millisecond(int amount, [bool abbreviated = true]) {
    if (abbreviated) {
      return "abbreviated_milliseconds".tr();
    }
    return "milliseconds".plural(amount);
  }

  @override
  String minute(int amount, [bool abbreviated = true]) {
    if (abbreviated) {
      return "abbreviated_minute".tr();
    }
    return "minute".plural(amount);
  }

  @override
  String month(int amount, [bool abbreviated = true]) {
    if (abbreviated) {
      return "abbreviated_month".tr();
    }
    return "month".plural(amount);
  }

  @override
  String second(int amount, [bool abbreviated = true]) {
    if (abbreviated) {
      return "abbreviated_second".tr();
    }
    return "second".plural(amount);
  }

  @override
  String week(int amount, [bool abbreviated = true]) {
    if (abbreviated) {
      return "abbreviated_week".tr();
    }
    return "week".plural(amount);
  }

  @override
  String year(int amount, [bool abbreviated = true]) {
    if (abbreviated) {
      return "abbreviated_year".tr();
    }
    return "year".plural(amount);
  }
}
