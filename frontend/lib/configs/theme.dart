import 'package:flutter/material.dart';

const _kPrimaryColor = Color(0xffffa91f);
const _kPrimaryColorLight = Color(0xffffdb57);
const _kPrimaryColorDark = Color(0xffc67a00);

const _kAccentColor = Color(0xffFFF1CC);

const kPrimaryTextColor = Color(0xffc67a00);
const kAccentTextColor = Colors.white;

//const kGreyColor = Color(0xfff0f0f0);

const kGreyColor = Color(0xffe0e0e0);

final kLinearGradient = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  stops: [0.2, 0.8],
  colors: [
    _kPrimaryColor,
    Colors.white,
  ],
);

const double kFieldAndButtonWidth = 350.0;

const kAnimationDuration = Duration(milliseconds: 200);

const kTitleTextStyle = TextStyle(
  color: kPrimaryTextColor,
  fontSize: 30.0,
  fontWeight: FontWeight.bold,
);

final kThemeData = ThemeData(
  brightness: Brightness.light,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  primaryColor: _kPrimaryColor,
  primaryColorDark: _kPrimaryColorDark,
  primaryColorLight: _kPrimaryColorLight,
  splashColor: _kPrimaryColorLight,
  accentColor: _kAccentColor,
  unselectedWidgetColor: _kPrimaryColorDark,
);

const kInfoTextStyle = TextStyle(
  color: Colors.black,
  fontSize: 20.0,
);
