import 'package:logger/logger.dart';

final logger = Logger(printer: PrettyPrinter());

void logInfo(String message) => logger.log(Level.info, message);

void logDebug(String message) => logger.log(Level.debug, message);

void logWarning(String message) => logger.log(Level.warning, message);

void logError(Exception error) =>
    logger.log(Level.error, error.toString(), error);
