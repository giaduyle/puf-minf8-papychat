import 'package:papyrus_chat_app/core/models/media.dart';
import 'package:papyrus_chat_app/core/models/sticker.dart';
import 'package:papyrus_chat_app/core/repositories/common/repository_mixin.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class StickerRepository implements BaseRepository {
  final Map<String, Map<String, Sticker>> _collections = {
    "mimi": Map.fromEntries(<Sticker>[
      Sticker("mimi::1", AssetMedia("stickers/mimi1.gif")),
      Sticker("mimi::2", AssetMedia("stickers/mimi2.gif")),
      Sticker("mimi::4", AssetMedia("stickers/mimi4.gif")),
      Sticker("mimi::5", AssetMedia("stickers/mimi5.gif")),
      Sticker("mimi::6", AssetMedia("stickers/mimi6.gif")),
      Sticker("mimi::7", AssetMedia("stickers/mimi7.gif")),
      Sticker("mimi::8", AssetMedia("stickers/mimi8.gif")),
      Sticker("mimi::9", AssetMedia("stickers/mimi9.gif")),
    ].map((e) => MapEntry(e.id, e)))
  };

  List<String> get allCollectionIds => _collections.keys;

  List<Sticker> getStickers(String collectionId) {
    return List.of(_findStickerCollection(collectionId).values);
  }

  Sticker getSticker(String collectionId, String stickerId) {
    return _findStickerCollection(collectionId)[stickerId];
  }

  Map<String, Sticker> _findStickerCollection(String collectionId) {
    return _collections[collectionId] ?? Map();
  }

  @override
  Future<void> close() async {
  }

  @override
  Future<int> clear() {
    throw UnimplementedError();
  }

  @override
  Future<void> compact() {
    throw UnimplementedError();
  }

  @override
  Future<void> delete() {
    throw UnimplementedError();
  }
}
