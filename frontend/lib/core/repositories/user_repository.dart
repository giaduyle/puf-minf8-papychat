import 'package:hive/hive.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/common/repository_mixin.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

const String kCurrentUser = "current_user";

class UserRepository implements BaseRepository {
  final Box<User> _meBox;
  final Box<User> _friendsBox;
  final Box<User> _blocksBox;
  final Box<User> _favouritesBox;

  UserRepository._(
      this._meBox, this._friendsBox, this._blocksBox, this._favouritesBox);

  static Future<UserRepository> of() async {
    return UserRepository._(
        await Hive.openBox(kCurrentUser,
            encryptionCipher: HiveAesCipher(SecurityUtils.secretKey().bytes)),
        await Hive.openBox("friends",
            encryptionCipher: HiveAesCipher(SecurityUtils.secretKey().bytes)),
        await Hive.openBox("blocks",
            encryptionCipher: HiveAesCipher(SecurityUtils.secretKey().bytes)),
        await Hive.openBox("favourites",
            encryptionCipher: HiveAesCipher(SecurityUtils.secretKey().bytes)));
  }

  void putMe(User user) {
    _meBox.put(kCurrentUser, user);
  }

  User getMe() {
    return _meBox.get(kCurrentUser);
  }

  void putFriends(List<User> friends) {
    putAllToBox(_friendsBox, friends, (record) => record.id);
  }

  Future<int> clearFriends() {
    return _friendsBox.clear();
  }

  List<User> getFriends() {
    return List.of(_friendsBox.values);
  }

  List<User> searchFriends(String text) {
    return List.of(searchInBox(_friendsBox, text, (user, text) {
      bool fullName = user.fullName?.contains(text) ?? false;
      bool email = user.email?.contains(text) ?? false;
      bool nickName = user.nickName?.contains(text) ?? false;
      bool phone = user.phone?.contains(text) ?? false;
      return fullName || email || nickName || phone;
    }));
  }

  User getFriend(String userId) {
    return _friendsBox.get(userId);
  }

  void unFriend(String userId) {
    _friendsBox.delete(userId);
    _favouritesBox.delete(userId);
  }

  void putBlocks(List<User> blocks) {
    putAllToBox(_blocksBox, blocks, (record) {
      String userId = record.id;
      unFriend(userId);
      return userId;
    });
  }

  Future<int> clearBlocks() {
    return _blocksBox.clear();
  }

  List<User> getBlocks() {
    return List.of(_blocksBox.values);
  }

  User getBlock(String userId) {
    return _blocksBox.get(userId);
  }

  void unblock(String userId) {
    putFriends([getBlock(userId)]);
    _blocksBox.delete(userId);
  }

  void putFavourites(List<User> favorites) {
    putAllToBox(_favouritesBox, favorites, (record) => record.id);
  }

  Future<int> clearFavourites() {
    return _favouritesBox.clear();
  }

  List<User> getFavourites() {
    return List.of(_favouritesBox.values);
  }

  void removeFavourite(String userId) {
    _favouritesBox.delete(userId);
  }

  @override
  Future<int> clear() async {
    int total = 0;
    total += await clearFriends();
    total += await clearBlocks();
    total += await clearFavourites();
    total += await _meBox.clear();
    return total;
  }

  @override
  Future<void> delete() async {
    deleteInjection<UserRepository>();
    await _meBox.deleteFromDisk();
    await _friendsBox.deleteFromDisk();
    await _blocksBox.deleteFromDisk();
    await _favouritesBox.deleteFromDisk();
  }

  @override
  Future<void> close() async {
    deleteInjection<UserRepository>();
    await _meBox.close();
    await _friendsBox.close();
    await _blocksBox.close();
    await _favouritesBox.close();
  }

  @override
  Future<void> compact() async {
    await _meBox.compact();
    await _friendsBox.compact();
    await _blocksBox.compact();
    await _favouritesBox.compact();
  }
}
