import 'package:hive/hive.dart';
import 'package:papyrus_chat_app/core/models/media.dart';
import 'package:papyrus_chat_app/core/repositories/common/repository_mixin.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class MediaRepository implements BaseRepository {
  final LazyBox<Media> _mediasBox;

  MediaRepository._(this._mediasBox);

  static Future<MediaRepository> of() async {
    return MediaRepository._(
      await Hive.openLazyBox("medias",
          encryptionCipher: HiveAesCipher(SecurityUtils.secretKey().bytes)),
    );
  }

  Future<void> putMedia(Media media) {
    return _mediasBox.put(media.id, media);
  }

  Future<void> putMedias(List<Media> medias) {
    return putAllToBox<Media>(_mediasBox, medias, (media) => media.id);
  }

  Future<Media> getMedia(String id) {
    return _mediasBox.get(id);
  }

  Future<void> deleteMedia(Media media) {
    return _mediasBox.delete(media.id);
  }

  Future<void> deleteMedias(List<Media> medias) {
    return _mediasBox.deleteAll(medias.map((media) => media.id).toList());
  }

  @override
  Future<int> clear() {
    return _mediasBox.clear();
  }

  @override
  Future<void> close() {
    deleteInjection<MediaRepository>();
    return _mediasBox.close();
  }

  @override
  Future<void> delete() {
    deleteInjection<MediaRepository>();
    return _mediasBox.deleteFromDisk();
  }

  @override
  Future<void> compact() {
    return _mediasBox.compact();
  }
}
