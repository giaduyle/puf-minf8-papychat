import 'package:basic_utils/basic_utils.dart';
import 'package:hive/hive.dart';

abstract class BaseRepository {
  Future<void> compact();

  Future<int> clear();

  Future<void> close();

  Future<void> delete();
}

int reversedKeyComparator(dynamic k1, dynamic k2) {
  if (k1 is int) {
    if (k2 is int) {
      if (k1 > k2) {
        return -1;
      } else if (k1 < k2) {
        return 1;
      } else {
        return 0;
      }
    } else {
      return 1;
    }
  } else if (k2 is String) {
    return -1 * (k1 as String).compareTo(k2);
  } else {
    return -1;
  }
}

Future<void> putAllToBox<T>(BoxBase<T> box, Iterable<T> records,
    dynamic Function(T record) keyBuilder) async {
  await box.putAll(Map.fromEntries(
      records.map((record) => MapEntry(keyBuilder(record), record)).toList()));
}

Iterable<T> searchInBox<T>(
    Box<T> box, String search, bool Function(T record, String text) predicate) {
  if (StringUtils.isNullOrEmpty(search)) {
    return box.values;
  }
  return box.values.where((record) => predicate(record, search));
}
