import 'package:basic_utils/basic_utils.dart';
import 'package:hive/hive.dart';
import 'package:papyrus_chat_app/core/models/event.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/common/repository_mixin.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class EventRepository implements BaseRepository {
  static final String _roomKey = "room";
  static final String _userKey = "user";

  final Box<List<dynamic>> _eventsBox;

  EventRepository._(this._eventsBox);

  static Future<EventRepository> of() async {
    return EventRepository._(await Hive.openBox("events",
        compactionStrategy: (int entries, int deletedEntries) {
      return deletedEntries > 30 && deletedEntries / entries > 0.2;
    }));
  }

  Future<void> putRoomEvents(Room room, List<Event> events) {
    return _eventsBox.put("${_roomKey}_${room.id}", events);
  }

  List<Event> getRoomEvents(Room room) {
    var events = _eventsBox.get("${_roomKey}_${room.id}");
    if (IterableUtils.isNullOrEmpty(events)) {
      return List();
    }
    return events;
  }

  Future<void> clearRoomEvents(Room room) async {
    return _eventsBox.delete("${_roomKey}_${room.id}");
  }

  Future<void> putUserEvents(User user, List<Event> events) async {
    if (events.isEmpty) {
      return;
    }
    await _eventsBox.put("${_userKey}_${user.id}", List.of(events));
  }

  List<Event> getUserEvents(User user) {
    var events = _eventsBox.get("${_userKey}_${user.id}");
    if (IterableUtils.isNullOrEmpty(events)) {
      return List<Event>();
    }
    return events.map((e) => e as Event).toList();
  }

  Future<void> clearUserEvents(User user) {
    return _eventsBox.delete("${_userKey}_${user.id}");
  }

  @override
  Future<int> clear() {
    return _eventsBox.clear();
  }

  @override
  Future<void> close() {
    deleteInjection<EventRepository>();
    return _eventsBox.close();
  }

  @override
  Future<void> delete() {
    deleteInjection<EventRepository>();
    return _eventsBox.deleteFromDisk();
  }

  @override
  Future<void> compact() {
    return _eventsBox.compact();
  }
}
