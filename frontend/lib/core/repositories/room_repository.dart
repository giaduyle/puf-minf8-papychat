import 'package:hive/hive.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/common/repository_mixin.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class RoomRepository implements BaseRepository {
  static final String _roomMessagesKey = "room_messages";
  static final String _roomMembersKey = "room_members";

  final Box<Room> _roomsBox;
  final Box<DateTime> _beforeTimeBox;

  RoomRepository._(this._roomsBox, this._beforeTimeBox);

  static Future<RoomRepository> of() async {
    return RoomRepository._(
        await Hive.openBox("rooms",
            encryptionCipher: HiveAesCipher(SecurityUtils.secretKey().bytes),
            keyComparator: reversedKeyComparator),
        await Hive.openBox("rooms_before_time"));
  }

  Future<void> putRoomMessagesPage(Room room, Page page) async {
    await _beforeTimeBox.put(room.id, page.beforeTime);
  }

  Future<void> deleteRoomMessagesPage(Room room) async {
    await _beforeTimeBox.delete(room.id);
  }

  Page getRoomMessagesPage(Room room) {
    return Page(
        pageNum: 0,
        pageSize: 30,
        beforeTime: _beforeTimeBox.get(room.id, defaultValue: DateTime.now()));
  }

  void putRooms(List<Room> rooms) {
    putAllToBox(_roomsBox, rooms, (room) => room.id);
  }

  List<Room> getRooms() {
    return List.of(_roomsBox.values);
  }

  List<Room> searchRooms(String text) {
    return List.of(searchInBox(_roomsBox, text, (room, text) {
      return room.name.contains(text);
    }));
  }

  Room getRoom(String id) {
    return _roomsBox.get(id);
  }

  Future<void> deleteRoom(Room room) async {
    Room existed = _roomsBox.get(room.id);
    if (existed == null) {
      throw StateError("Cannot delete non-existed room ${room.name}");
    }
    await deleteMessages(room);
    await removeAllMembers(room);
    await deleteRoomMessagesPage(room);
    return _roomsBox.delete(room.id);
  }

  Future<void> putMessages(Room room, List<Message> messages) async {
    _isRoomNotExisted(room);
    Box<Message> box = await _messageBox(room);
    // Hive only support 32bit keys.
    // Using seconds since epoch can partially help.
    putAllToBox<Message>(box, messages, (message) {
      if (room.lastMessage == null || message.isNewerThan(room.lastMessage)) {
        room.lastMessage = message;
      }
      return "${message.id}_"
          "${message.createdAt.millisecondsSinceEpoch ~/ 1000}";
    });
    return box.close();
  }

  Future<void> deleteMessages(Room room) {
    _isRoomNotExisted(room);
    return Hive.deleteBoxFromDisk("${_roomMessagesKey}_${room.id}");
  }

  Future<List<Message>> getMessages(Room room) async {
    _isRoomNotExisted(room);
    Box<Message> box = await _messageBox(room);
    List<Message> records = List.of(box.values);
    records.sort(Message.descendingSorter);
    await box.close();
    return records;
  }

  Future<Message> findLatestMessage(Room room) async {
    _isRoomNotExisted(room);
    Box<Message> box = await _messageBox(room);
    Message record = box.getAt(0);
    await box.close();
    return record;
  }

  Future<void> putMembers(Room room, List<User> members) async {
    if (members == null || _isRoomNotExisted(room)) {
      return;
    }
    Box<User> box = await _memberBox(room);
    putAllToBox<User>(box, members, (member) => member.id);
    return box.close();
  }

  Future<void> removeMembers(Room room, List<User> members) async {
    if (members == null || _isRoomNotExisted(room)) {
      return;
    }
    _isRoomNotExisted(room);
    Box<User> box = await _memberBox(room);
    return box.deleteAll(members.map((e) => e.id).toList());
  }

  Future<void> removeAllMembers(Room room) async {
    if (_isRoomNotExisted(room)) {
      return;
    }
    return Hive.deleteBoxFromDisk("${_roomMembersKey}_${room.id}");
  }

  Future<List<User>> getMembers(Room room) async {
    _isRoomNotExisted(room);
    Box<User> box = await _memberBox(room);
    List<User> records = List.of(box.values);
    await box.close();
    return records;
  }

  bool _isRoomNotExisted(Room room) {
    return getRoom(room?.id) == null;
  }

  @override
  Future<int> clear() async {
    int total = 0;
    _roomsBox.values.forEach((room) async {
      await deleteRoom(room);
      total++;
    });
    await _beforeTimeBox.clear();
    return total;
  }

  @override
  Future<void> close() async {
    deleteInjection<RoomRepository>();
    await _beforeTimeBox.close();
    await _roomsBox.close();
  }

  @override
  Future<void> delete() async {
    await clear();
    await _beforeTimeBox.deleteFromDisk();
    await _roomsBox.deleteFromDisk();
  }

  @override
  Future<void> compact() async {
    _roomsBox.values.forEach((room) async {
      (await _messageBox(room)).compact();
      (await _memberBox(room)).compact();
    });
    await _beforeTimeBox.compact();
    await _roomsBox.compact();
  }

  Future<Box<Message>> _messageBox(Room room) async {
    return Hive.openBox("${_roomMessagesKey}_${room.id}",
        keyComparator: reversedKeyComparator,
        encryptionCipher: HiveAesCipher(SecurityUtils.secretKey().bytes));
  }

  Future<Box<User>> _memberBox(Room room) async {
    return Hive.openBox("${_roomMembersKey}_${room.id}",
        encryptionCipher: HiveAesCipher(SecurityUtils.secretKey().bytes));
  }
}
