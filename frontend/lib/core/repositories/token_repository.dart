import 'package:hive/hive.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:papyrus_chat_app/core/repositories/common/repository_mixin.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class TokenRepository implements BaseRepository {
  final Box<String> _box;

  TokenRepository._(this._box);

  static Future<TokenRepository> of() async {
    return TokenRepository._(await Hive.openBox("tokens",
        encryptionCipher: HiveAesCipher(SecurityUtils.secretKey().bytes)));
  }

  void save(Token token) {
    if (token.id == null) {
      throw ArgumentError('Token id must not be null');
    }
    _box.put(token.id, token.token);
  }

  Token get(String id) {
    String savedToken = _box.get(id);
    if (savedToken == null) {
      return null;
    }
    return Token(id: id, token: savedToken);
  }

  AuthToken getAuthToken() {
    String savedToken = _box.get(AuthToken.tokenId);
    if (savedToken == null) {
      return null;
    }
    return AuthToken(savedToken);
  }

  RoomToken getRoomToken(String roomId) {
    String savedToken = _box.get("${RoomToken.tokenId}$roomId");
    if (savedToken == null) {
      return null;
    }
    return RoomToken(roomId, savedToken);
  }

  @override
  Future<void> close() {
    deleteInjection<TokenRepository>();
    return _box.close();
  }

  @override
  Future<void> delete() {
    deleteInjection<TokenRepository>();
    return _box.deleteFromDisk();
  }

  @override
  Future<int> clear() {
    return _box.clear();
  }

  @override
  Future<void> compact() {
    return _box.compact();
  }
}
