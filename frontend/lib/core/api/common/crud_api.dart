import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import 'http_facade.dart';

abstract class CRUDApi<T> {
  @protected
  final HttpFacade httpFacade = inject();

  String get path;

  T Function(Map<String, dynamic> json) get parser;

  Map<String, dynamic> Function(T entity) get serializer;

  Future<T> create(T entity) {
    return httpFacade.post(path,
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: serializer(entity),
        parser: (bodyString) => parser(jsonDecode(bodyString)));
  }

  Future<T> update(T entity) {
    return httpFacade.patch(path,
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: serializer(entity),
        parser: (bodyString) => parser(jsonDecode(bodyString)));
  }

  Future<T> replace(T entity) {
    return httpFacade.put(path,
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: serializer(entity),
        parser: (bodyString) => parser(jsonDecode(bodyString)));
  }

  Future<T> delete(String id) {
    return httpFacade.delete("$path/$id",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        parser: (bodyString) => parser(jsonDecode(bodyString)));
  }

  Future<T> get(String id) {
    return httpFacade.get("$path/$id",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        parser: (bodyString) => parser(jsonDecode(bodyString)));
  }

  Future<PageData<T>> getMany({Page page}) {
    return doQuery<T>(path, page, parser);
  }

  Future<PageData<T>> search(String text, {Page page}) {
    assert(text != null);
    return doQuery<T>(path, page, parser, queries: {"q": text});
  }

  Future<PageData<T>> doQuery<T>(
      String path, Page page, T Function(Map<String, dynamic>) parser,
      {Map<String, String> queries}) {
    Page requestPage = page ?? Page(pageNum: 0, pageSize: 10);
    int returnCount;
    int returnPageCount;
    return httpFacade.get(path,
        page: requestPage,
        queries: queries,
        headers: AuthUtils.buildAuthenticatedHeaders(),
        onResponseHeaders: (responseHeaders) {
      String xCount = responseHeaders['x-count'];
      returnCount = xCount != null ? int.parse(xCount) : null;
      String xPageCount = responseHeaders['x-page-count'];
      returnPageCount = xPageCount != null ? int.parse(xPageCount) : null;
      logDebug(responseHeaders.toString());
    }, parser: (bodyString) {
      return PageData(
          Page(
              pageNum: requestPage.pageNum,
              pageSize: requestPage.pageSize,
              beforeTime: requestPage.beforeTime,
              returnCount: returnCount,
              returnPageCount: returnPageCount),
          JsonUtils.parseArray<T>(
              jsonDecode(bodyString), (jsonObject) => parser(jsonObject)));
    });
  }
}
