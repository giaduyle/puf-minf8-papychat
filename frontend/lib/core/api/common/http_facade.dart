import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mime_type/mime_type.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/core/models/exceptions.dart';
import 'package:http_parser/src/media_type.dart';

class HttpFacade {
  final String baseUrl;

  HttpFacade(this.baseUrl) : assert(StringUtils.isNotNullOrEmpty(baseUrl));

  Future<T> get<T>(
    String path, {
    Page page,
    Map<String, String> queries,
    Map<String, String> headers,
    Function(Map<String, String>) onResponseHeaders,
    T Function(String bodyString) parser,
    bool Function(int statusCode) statusChecker,
  }) async {
    String url = "$baseUrl$path${formatParams(page: page, queries: queries)}";
    logDebug("GET Url=$url, Headers=$headers");
    return _onParsedResponse(await http.get(url, headers: headers),
        onResponseHeaders, statusChecker, parser);
  }

  Future<Uint8List> readBytes(
    String path, {
    Map<String, String> queries,
    Map<String, String> headers,
    Function(Map<String, String>) onResponseHeaders,
    bool Function(int statusCode) statusChecker,
  }) async {
    String url = "$baseUrl$path${formatParams(queries: queries)}";
    logDebug("GET Url=$url, Headers=$headers");
    return _onRawResponse(await http.get(url, headers: headers),
        onResponseHeaders, statusChecker);
  }

  Future<Uint8List> uploadBytes(
    String path,
    Uint8List bytes,
    String fileName, {
    Map<String, String> queries,
    Map<String, String> headers,
    Function(Map<String, String>) onResponseHeaders,
    bool Function(int statusCode) statusChecker,
  }) async {
    var request = http.MultipartRequest('POST', Uri.parse("$baseUrl$path"));
    request.headers.addAll(headers);
    request.headers.remove(HttpHeaders.contentTypeHeader);
    String mimeType = mime(fileName);
    if (mimeType == null) mimeType = ContentType.binary.mimeType;
    var contentType = MediaType.parse(mimeType);
    request.files.add(http.MultipartFile.fromBytes('file', bytes,
        filename: fileName, contentType: contentType));
    logDebug("POST Url=$baseUrl$path, File-Name=$fileName, "
        "Content-Length=${request.contentLength}, Headers=${request.headers}, "
        "Fields=${request.fields}, contentType: $contentType");
    var stream = await request.send();
    return _onRawResponse(await http.Response.fromStream(stream),
        onResponseHeaders, statusChecker);
  }

  Future<T> post<T>(String path,
      {Map<String, String> headers,
      dynamic bodyJson,
      Function(Map<String, String>) onResponseHeaders,
      T Function(String bodyString) parser,
      bool Function(int statusCode) statusChecker}) async {
    String url = "$baseUrl$path";
    String json = jsonEncode(bodyJson);
    logDebug("POST Url=$url, Headers=$headers, RequestBody=$json");
    return _onParsedResponse(
        await http.post(url, headers: headers, body: json, encoding: utf8),
        onResponseHeaders,
        statusChecker,
        parser);
  }

  Future<T> put<T>(
    String path, {
    Map<String, String> headers,
    dynamic bodyJson,
    Function(Map<String, String>) onResponseHeaders,
    T Function(String bodyString) parser,
    bool Function(int statusCode) statusChecker,
  }) async {
    String url = "$baseUrl$path";
    String json = jsonEncode(bodyJson);
    logDebug("PUT Url=$url, Headers=$headers, RequestBody=$json");
    return _onParsedResponse(
        await http.put(url, headers: headers, body: json, encoding: utf8),
        onResponseHeaders,
        statusChecker,
        parser);
  }

  Future<T> patch<T>(String path,
      {Map<String, String> headers,
      dynamic bodyJson,
      Function(Map<String, String>) onResponseHeaders,
      T Function(String bodyString) parser,
      bool Function(int statusCode) statusChecker}) async {
    String url = "$baseUrl$path";
    String json = jsonEncode(bodyJson);
    logDebug("PATCH Url=$url, Headers=$headers, RequestBody=$json");
    return _onParsedResponse(
        await http.patch(url, headers: headers, body: json, encoding: utf8),
        onResponseHeaders,
        statusChecker,
        parser);
  }

  Future<T> delete<T>(
    String path, {
    Page page,
    Map<String, String> queries,
    Map<String, String> headers,
    Function(Map<String, String>) onResponseHeaders,
    T Function(String bodyString) parser,
    bool Function(int statusCode) statusChecker,
  }) async {
    String url = "$baseUrl$path${formatParams(page: page, queries: queries)}";
    logDebug("DELETE Url: $url, Headers=$headers");
    return _onParsedResponse(await http.delete(url, headers: headers),
        onResponseHeaders, statusChecker, parser);
  }

  static bool isSuccess(http.Response response) {
    return response.statusCode > HttpStatus.continue_ &&
        response.statusCode < HttpStatus.multipleChoices;
  }

  static bool isError(http.Response response) {
    return response.statusCode >= HttpStatus.badRequest;
  }

  static HttpError error(http.Response response) {
    if (response.statusCode >= HttpStatus.internalServerError) {
      return HttpServerError(response);
    }
    if (response.statusCode >= HttpStatus.badRequest) {
      return HttpClientError(response);
    }
    return HttpError(response);
  }

  static String formatParams({Page page, Map<String, String> queries}) {
    if (page == null && queries == null) {
      return "";
    }
    String formatted = [page?.toString(), _formatQueries(queries)]
        .where((element) => element != null)
        .join("&");
    if (StringUtils.isNullOrEmpty(formatted)) {
      return "";
    }
    return "?$formatted";
  }

  Uint8List _onRawResponse(
    http.Response response,
    Function(Map<String, String>) onResponseHeaders,
    bool Function(int statusCode) statusChecker,
  ) {
    if (onResponseHeaders != null) {
      onResponseHeaders(response.headers);
    }
    if (_checkStatus(response, statusChecker)) {
      return response.bodyBytes;
    }
    throw error(response);
  }

  T _onParsedResponse<T>(
      http.Response response,
      Function(Map<String, String>) onResponseHeaders,
      bool Function(int statusCode) statusChecker,
      T Function(String bodyString) parser) {
    if (onResponseHeaders != null) {
      onResponseHeaders(response.headers);
    }
    if (_checkStatus(response, statusChecker)) {
      if (response.body == null || response.body.isEmpty || parser == null) {
        return null;
      }
      return parser(response.body);
    }
    throw error(response);
  }

  static String _formatQueries(Map<String, String> queries) {
    if (queries?.isEmpty ?? true) {
      return null;
    }
    return queries.entries
        .where((entry) => StringUtils.isNotNullOrEmpty(entry.value))
        .map((e) => "${e.key}=${e.value}")
        .join("&");
  }

  static bool _checkStatus(http.Response response,
      bool Function(int statusCode) customStatusChecker) {
    if (customStatusChecker != null) {
      return customStatusChecker(response.statusCode);
    }
    return isSuccess(response);
  }
}

@immutable
class Page extends Equatable {
  final int pageNum;
  final int pageSize;
  final DateTime beforeTime;
  final DateTime afterTime;
  final int returnCount;
  final int returnPageCount;

  Page(
      {this.pageNum,
      this.pageSize,
      this.beforeTime,
      this.afterTime,
      this.returnCount,
      this.returnPageCount});

  @override
  List<Object> get props => [
        pageNum,
        pageSize,
        beforeTime,
        this.afterTime,
        returnCount,
        returnPageCount
      ];

  bool get hasNext => pageNum != returnPageCount;

  @override
  String toString() {
    return [_pageNum(), _pageSize(), _beforeTime(), _afterTime()]
        .where((element) => element != null)
        .join("&");
  }

  String _pageNum() {
    return pageNum == null ? null : "pageNum=$pageNum";
  }

  String _pageSize() {
    return pageSize == null ? null : "pageSize=$pageSize";
  }

  String _beforeTime() {
    return beforeTime == null
        ? null
        : "beforeTime=${beforeTime.toIso8601String()}";
  }

  String _afterTime() {
    return afterTime == null
        ? null
        : "afterTime=${afterTime.toIso8601String()}";
  }

  Page get next => Page(
      pageNum: this.pageNum + 1,
      pageSize: this.pageSize,
      beforeTime: this.beforeTime,
      afterTime: this.afterTime,
      returnCount: this.returnCount,
      returnPageCount: this.returnPageCount);

  Page get previous => Page(
      pageNum: this.pageNum == 0 ? 0 : this.pageNum - 1,
      pageSize: this.pageSize,
      beforeTime: this.beforeTime,
      afterTime: this.afterTime,
      returnCount: this.returnCount,
      returnPageCount: this.returnPageCount);
}

@immutable
class PageData<T> {
  final Page page;
  final List<T> data;

  PageData(this.page, this.data);
}
