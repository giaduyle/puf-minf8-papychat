import 'dart:convert';

import 'package:global_configuration/global_configuration.dart';
import 'package:papyrus_chat_app/core/api/common/crud_api.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import 'common/http_facade.dart';

class RoomApi with CRUDApi<Room> {
  String get path => "${GlobalConfiguration().getString("api")}/rooms";

  @override
  Room Function(Map<String, dynamic> json) get parser =>
      (json) => Room.fromJson(json);

  @override
  Map<String, dynamic> Function(Room room) get serializer =>
      (room) => room.toJson();

  Future<PageData<Message>> getMessages(String roomId, {Page page}) {
    return doQuery<Message>(
        "$path/$roomId/messages", page, (json) => Message.fromJson(json));
  }

  Future<PageData<User>> getMembers(String roomId, {Page page}) {
    return doQuery<User>(
        "$path/$roomId/members", page, (json) => User.fromJson(json));
  }

  Future<List<User>> replaceMembers(String roomId, List<String> userIds) {
    return httpFacade.put("$path/$roomId/members",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: userIds,
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<List<User>> addMembers(String roomId, List<String> userIds) {
    return httpFacade.patch("$path/$roomId/members",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: userIds,
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<void> removeMember(String roomId, String userId) {
    return httpFacade.delete("$path/$roomId/members/$userId",
        headers: AuthUtils.buildAuthenticatedHeaders());
  }
}
