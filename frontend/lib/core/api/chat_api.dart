import 'dart:convert';

import 'package:global_configuration/global_configuration.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import 'common/http_facade.dart';

class ChatApi {
  HttpFacade _httpFacade = inject();

  String get path => "${GlobalConfiguration().getString("api")}";

  Future<RoomToken> getRoomToken(String roomId) async {
    return _httpFacade.get("$path/tokens/$roomId",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        parser: (bodyString) =>
            RoomToken(roomId, Token.fromJson(jsonDecode(bodyString)).token));
  }

  WebSocketChannel joinRoom(RoomToken roomToken) {
    String webSocket = GlobalConfiguration().getString("web-socket");
    int port = GlobalConfiguration().getInt("port");
    String wsUrl =
        '$webSocket:$port/chat/${roomToken.roomId}/${roomToken.token}';
    logDebug("Connecting to web socket: $wsUrl");
    return WebSocketChannel.connect(Uri.parse(wsUrl));
  }

  void sendMessage(IOWebSocketChannel channel, String message) {
    logDebug("Sending: $message");
    channel.sink.add(message);
  }
}
