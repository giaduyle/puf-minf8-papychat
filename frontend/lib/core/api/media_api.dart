import 'dart:convert';
import 'dart:typed_data';

import 'package:global_configuration/global_configuration.dart';
import 'package:papyrus_chat_app/core/api/common/crud_api.dart';
import 'package:papyrus_chat_app/core/models/media.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class MediaApi with CRUDApi<Media> {
  @override
  String get path => "${GlobalConfiguration().getString("api")}/medias";

  @override
  Media Function(Map<String, dynamic> json) get parser =>
      (json) => Media.fromJson(json);

  @override
  Map<String, dynamic> Function(Media media) get serializer =>
      (media) => media.toJson();

  Future<Media> download(String mediaId) async {
    Media media = Media(id: mediaId);
    media.content = await httpFacade.readBytes("$path/$mediaId/download",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        onResponseHeaders: (headers) {
      media.name = headers["Content-Disposition"];
      media.contentType = headers["Content-Type"];
    });
    return media;
  }

  Future<Media> upload(Media media) async {
    var headers = AuthUtils.buildAuthenticatedHeaders();
    headers['Content-Disposition'] = "attachment;filename=${media.name}";
    Uint8List bytes = media.content as Uint8List;
    Uint8List responseBytes = await httpFacade
        .uploadBytes("$path", bytes, media.name, headers: headers);
    return Media.fromJsonArray(jsonDecode(utf8.decode(responseBytes))).first;
  }
}
