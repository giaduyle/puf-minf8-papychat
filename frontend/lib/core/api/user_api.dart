import 'dart:convert';

import 'package:basic_utils/basic_utils.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:papyrus_chat_app/configs/constants.dart';
import 'package:papyrus_chat_app/core/api/common/crud_api.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';
import 'package:papyrus_chat_app/core/models/event.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/token_repository.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class UserApi with CRUDApi<User> {
  String get path => "${GlobalConfiguration().getString("api")}/users";

  @override
  User Function(Map<String, dynamic> json) get parser =>
      (json) => User.fromJson(json);

  @override
  Map<String, dynamic> Function(User user) get serializer =>
      (user) => user.toJson();

  Future<User> getMe() {
    return httpFacade.get("$path/me",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        parser: (bodyString) => parser(jsonDecode(bodyString)));
  }

  Future<User> create(User user) {
    return httpFacade.post<User>(path,
        headers: kDefaultHeaders, bodyJson: serializer(_encryptPassword(user)),
        onResponseHeaders: (headers) {
      var tokenString = headers['x-api-token'];
      if (StringUtils.isNotNullOrEmpty(tokenString)) {
        inject<TokenRepository>().save(AuthToken(tokenString));
      }
    }, parser: (bodyString) {
      var created = parser(jsonDecode(bodyString));
      created.password = user.password;
      return created;
    });
  }

  Future<User> update(User user) {
    return super.update(_encryptPassword(user));
  }

  Future<User> replace(User user) {
    return super.replace(_encryptPassword(user));
  }

  Future<List<User>> getFriends(String userId) {
    return httpFacade.get("$path/$userId/friends",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<List<User>> replaceFriends(String userId, List<String> friendIds) {
    return httpFacade.put("$path/$userId/friends",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: friendIds,
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<List<User>> addFriend(String userId, List<String> friendIds) {
    return httpFacade.patch("$path/$userId/friends",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: friendIds,
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<void> removeFriend(String userId, String friendId) {
    return httpFacade.delete("$path/$userId/friends/$friendId",
        headers: AuthUtils.buildAuthenticatedHeaders());
  }

  Future<List<User>> getFavourites(String userId) {
    return httpFacade.get("$path/$userId/favorite_friends",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<List<User>> replaceFavourites(String userId, List<String> friendIds) {
    return httpFacade.put("$path/$userId/favorite_friends",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: friendIds,
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<List<User>> addFavourite(String userId, List<String> friendIds) {
    return httpFacade.patch("$path/$userId/favorite_friends",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: friendIds,
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<void> removeFavourite(String userId, String friendId) {
    return httpFacade.delete("$path/$userId/favorite_friends/$friendId",
        headers: AuthUtils.buildAuthenticatedHeaders());
  }

  Future<List<User>> getBlocks(String userId) {
    return httpFacade.get("$path/$userId/blocked_friends",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<List<User>> replaceBlocks(String userId, List<String> friendIds) {
    return httpFacade.put("$path/$userId/blocked_friends",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: friendIds,
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<List<User>> block(String userId, List<String> friendIds) {
    return httpFacade.patch("$path/$userId/blocked_friends",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: friendIds,
        parser: (bodyString) => User.fromJsonArray(jsonDecode(bodyString)));
  }

  Future<void> unblock(String userId, String friendId) {
    return httpFacade.delete("$path/$userId/blocked_friends/$friendId",
        headers: AuthUtils.buildAuthenticatedHeaders());
  }

  Future<PageData<Room>> getRooms(String userId, {Page page}) {
    return doQuery("$path/$userId/rooms", page, (json) => Room.fromJson(json));
  }

  Future<Event> updateEvent(String userId, Event event) {
    return httpFacade.patch("$path/$userId/events/${event.id}",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        bodyJson: event.toJson(),
        parser: (bodyString) => Event.fromJson(jsonDecode(bodyString)));
  }

  Future<PageData<Event>> getEvents(String userId,
      {EventType type, EventStatus status, Page page}) {
    return doQuery("$path/$userId/events", page, (json) => Event.fromJson(json),
        queries: {
          "type": type == null ? null : type.index.toString(),
          "status": status == null ? null : status.index.toString()
        });
  }

  Future<PageData<Event>> getSentEvents(String userId,
      {EventType type, EventStatus status, Page page}) {
    return doQuery(
        "$path/$userId/sent_events", page, (json) => Event.fromJson(json),
        queries: {
          "type": type == null ? null : type.index.toString(),
          "status": status == null ? null : status.index.toString()
        });
  }

  Future<Event> getEvent(String userId, String eventId) {
    return httpFacade.get("$path/$userId/events/$eventId",
        headers: AuthUtils.buildAuthenticatedHeaders(),
        parser: (bodyString) => Event.fromJson(jsonDecode(bodyString)));
  }

  User _encryptPassword(User user) {
    if (StringUtils.isNullOrEmpty(user.password)) {
      return user;
    }
    User cloned = User.fromJson(user.toJson());
    cloned.password =
        SecurityUtils.encryptAESCryptoJSDefaultSecret(user.password);
    return cloned;
  }
}
