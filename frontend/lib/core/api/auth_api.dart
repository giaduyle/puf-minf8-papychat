import 'dart:convert';
import 'dart:io';

import 'package:global_configuration/global_configuration.dart';
import 'package:papyrus_chat_app/configs/constants.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import 'common/http_facade.dart';

class AuthApi {
  HttpFacade _httpFacade = inject();

  String get path => "${GlobalConfiguration().getString("api")}/auth";

  Future<AuthToken> login(User user) async {
    return _httpFacade.get("$path/login",
        headers: kDefaultHeaders,
        queries: {
          "email": user.email,
          "password": Uri.encodeComponent(
              SecurityUtils.encryptAESCryptoJSDefaultSecret(user.password))
        },
        parser: (body) => AuthToken(Token.fromJson(jsonDecode(body)).token));
  }

  Future<AuthToken> google(String accessToken) async {
    return _httpFacade.get("$path/google",
        headers: kDefaultHeaders,
        queries: {
          "token": Uri.encodeComponent(
              SecurityUtils.encryptAESCryptoJSDefaultSecret(accessToken))
        },
        parser: (body) => AuthToken(Token.fromJson(jsonDecode(body)).token));
  }

  Future<AuthToken> refresh(Token token) async {
    return _httpFacade.get("$path/refresh",
        headers: {HttpHeaders.authorizationHeader: "Bearer ${token.token}"},
        parser: (body) => AuthToken(Token.fromJson(jsonDecode(body)).token));
  }
}
