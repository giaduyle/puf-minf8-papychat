// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserStatusAdapter extends TypeAdapter<UserStatus> {
  @override
  final int typeId = 11;

  @override
  UserStatus read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return UserStatus.Offline;
      case 1:
        return UserStatus.Online;
      case 2:
        return UserStatus.Away;
      case 3:
        return UserStatus.Busy;
      default:
        return null;
    }
  }

  @override
  void write(BinaryWriter writer, UserStatus obj) {
    switch (obj) {
      case UserStatus.Offline:
        writer.writeByte(0);
        break;
      case UserStatus.Online:
        writer.writeByte(1);
        break;
      case UserStatus.Away:
        writer.writeByte(2);
        break;
      case UserStatus.Busy:
        writer.writeByte(3);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserStatusAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class UserGenderAdapter extends TypeAdapter<UserGender> {
  @override
  final int typeId = 12;

  @override
  UserGender read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return UserGender.Unspecified;
      case 1:
        return UserGender.Female;
      case 2:
        return UserGender.Male;
      default:
        return null;
    }
  }

  @override
  void write(BinaryWriter writer, UserGender obj) {
    switch (obj) {
      case UserGender.Unspecified:
        writer.writeByte(0);
        break;
      case UserGender.Female:
        writer.writeByte(1);
        break;
      case UserGender.Male:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserGenderAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class UserAdapter extends TypeAdapter<User> {
  @override
  final int typeId = 1;

  @override
  User read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return User(
      id: fields[0] as String,
      email: fields[1] as String,
      password: fields[2] as String,
      firstName: fields[3] as String,
      lastName: fields[4] as String,
      nickName: fields[5] as String,
      address: fields[6] as String,
      phone: fields[7] as String,
      avatarUrl: fields[8] as String,
      birthday: fields[9] as DateTime,
      status: fields[15] as UserStatus,
      gender: fields[14] as UserGender,
      lastSeenTime: fields[10] as DateTime,
      favourite: fields[16] as bool,
      createdAt: fields[11] as DateTime,
      updatedAt: fields[12] as DateTime,
    )..isMe = fields[17] as bool;
  }

  @override
  void write(BinaryWriter writer, User obj) {
    writer
      ..writeByte(17)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.email)
      ..writeByte(2)
      ..write(obj.password)
      ..writeByte(3)
      ..write(obj.firstName)
      ..writeByte(4)
      ..write(obj.lastName)
      ..writeByte(5)
      ..write(obj.nickName)
      ..writeByte(6)
      ..write(obj.address)
      ..writeByte(7)
      ..write(obj.phone)
      ..writeByte(8)
      ..write(obj.avatarUrl)
      ..writeByte(9)
      ..write(obj.birthday)
      ..writeByte(10)
      ..write(obj.lastSeenTime)
      ..writeByte(11)
      ..write(obj.createdAt)
      ..writeByte(12)
      ..write(obj.updatedAt)
      ..writeByte(14)
      ..write(obj.gender)
      ..writeByte(15)
      ..write(obj.status)
      ..writeByte(16)
      ..write(obj.favourite)
      ..writeByte(17)
      ..write(obj.isMe);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as String,
    email: json['email'] as String,
    password: json['password'] as String,
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    nickName: json['nickName'] as String,
    address: json['address'] as String,
    phone: json['phone'] as String,
    avatarUrl: json['avatarUrl'] as String,
    birthday: json['birthday'] == null
        ? null
        : DateTime.parse(json['birthday'] as String),
    status: _statusFromJson(json['status'] as int),
    gender: _genderFromJson(json['gender'] as int),
    lastSeenTime: json['lastSeenTime'] == null
        ? null
        : DateTime.parse(json['lastSeenTime'] as String),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'password': instance.password,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'nickName': instance.nickName,
      'address': instance.address,
      'phone': instance.phone,
      'avatarUrl': instance.avatarUrl,
      'birthday': instance.birthday?.toIso8601String(),
      'lastSeenTime': instance.lastSeenTime?.toIso8601String(),
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'gender': _genderToJson(instance.gender),
      'status': _statusToJson(instance.status),
    };
