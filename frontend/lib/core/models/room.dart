import 'package:basic_utils/basic_utils.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

part 'room.g.dart';

@JsonSerializable()
@HiveType(typeId: 2)
class Room {
  @HiveField(0)
  String id;

  @HiveField(1)
  String name;

  @HiveField(2)
  bool isPrivate = true;

  @HiveField(3)
  DateTime createdAt = DateTime.now();

  @HiveField(4)
  DateTime updatedAt = DateTime.now();

  @HiveField(5)
  Message lastMessage;

  @JsonKey(ignore: true)
  List<User> members;

  Room(
      {this.id,
      this.name,
      this.isPrivate = true,
      this.members,
      this.createdAt,
      this.updatedAt,
      this.lastMessage});

  factory Room.fromJson(Map<String, dynamic> json) => _$RoomFromJson(json);

  static List<Room> fromJsonArray(List<dynamic> jsonArray) {
    return JsonUtils.parseArray(
        jsonArray, (jsonObject) => Room.fromJson(jsonObject));
  }

  Map<String, dynamic> toJson() => _$RoomToJson(this);

  String get title {
    if (name != null) {
      return name;
    }
    if (IterableUtils.isNullOrEmpty(members)) {
      return id;
    }
    return members
        .where((element) => !element.isMe)
        .map((e) => e.firstName)
        .join(", ");
  }

  @override
  String toString() => toJson().toString();
}
