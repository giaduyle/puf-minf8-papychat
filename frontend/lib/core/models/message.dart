import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:papyrus_chat_app/configs/constants.dart';
import 'package:papyrus_chat_app/core/models/media.dart';
import 'package:papyrus_chat_app/core/models/sticker.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

part 'message.g.dart';

@JsonSerializable()
@HiveType(typeId: 3)
class Message implements Comparable<Message> {
  @HiveField(0)
  String id;

  @HiveField(1)
  String senderId;

  @HiveField(2)
  String content;

  @HiveField(3)
  String roomId;

  @HiveField(4)
  DateTime createdAt = DateTime.now();

  @HiveField(5)
  DateTime updatedAt = DateTime.now();

  @JsonKey(ignore: true)
  Media media;

  Message(
      {this.id,
      this.senderId,
      this.content,
      this.createdAt,
      this.updatedAt,
      this.roomId,
      this.media});

  bool isNewerThan(Message other) {
    return this.createdAt.compareTo(other.createdAt) > 0;
  }

  bool get isSticker => content != null && content.contains(kStickerPrefix);

  Sticker get sticker =>
      !isSticker ? null : Sticker(content.replaceAll(kStickerPrefix, ""));

  bool get isMedia =>
      media != null || content != null && content.contains(kMediaPrefix);

  String get getMediaId =>
      !isMedia ? null : content.replaceAll(kMediaPrefix, "");

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

  static List<Message> fromJsonArray(List<dynamic> jsonArray) {
    return JsonUtils.parseArray(
        jsonArray, (jsonObject) => Message.fromJson(jsonObject));
  }

  static int descendingSorter(Message msg1, Message msg2) =>
      -1 * msg1.createdAt.compareTo(msg2.createdAt);

  Map<String, dynamic> toJson() => _$MessageToJson(this);

  @override
  String toString() => toJson().toString();

  @override
  int compareTo(Message other) {
    if (other == null) {
      return -1;
    }
    return descendingSorter(this, other);
  }
}
