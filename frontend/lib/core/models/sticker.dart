import 'package:flutter/cupertino.dart';
import 'package:papyrus_chat_app/core/models/media.dart';

class Sticker {
  String id;
  Media media;

  Sticker(this.id, [this.media]);

  String get collectionId => id == null ? null : id.split("::")[0];

  Widget toImageAsset({double width = 50, double height = 50}) => Image.asset(
        media.content,
        width: width,
        height: height,
        fit: BoxFit.cover,
      );

  @override
  String toString() {
    Map<String, dynamic> json = {'id': id};
    json.addAll(media?.toJson());
    return json.toString();
  }
}
