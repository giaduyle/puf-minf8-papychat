import 'package:http/http.dart';

class HttpError implements Exception {
  String error;
  int code;

  HttpError(final Response response) {
    error = "${response.reasonPhrase} ${response.body}";
    code = response.statusCode;
  }

  @override
  String toString() {
    return "Http general error: $code-$error";
  }
}

class HttpClientError extends HttpError {
  HttpClientError(Response response) : super(response);

  @override
  String toString() {
    return "Http Client error: $code-$error";
  }
}

class HttpServerError extends HttpError {
  HttpServerError(Response response) : super(response);

  @override
  String toString() {
    return "Http Server error: $code-$error";
  }
}
