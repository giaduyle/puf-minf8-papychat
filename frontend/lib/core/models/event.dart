import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

part 'event.g.dart';

@HiveType(typeId: 51)
enum EventType {
  @HiveField(0)
  InviteFriend
}

_typeFromJson(int literal) =>
    literal == null ? null : EventType.values[literal];

_typeToJson(EventType type) => type == null ? null : type.index;

@HiveType(typeId: 52)
enum EventStatus {
  @HiveField(0)
  New,

  @HiveField(1)
  Accepted,

  @HiveField(2)
  Denied
}

_statusFromJson(int literal) => EventStatus.values[literal];

_statusToJson(EventStatus status) => status.index;

@JsonSerializable(explicitToJson: true)
@HiveType(typeId: 5)
class Event {
  @HiveField(0)
  String id;

  @HiveField(1)
  String senderId;

  @HiveField(2)
  String receiverId;

  @JsonKey(
    fromJson: _typeFromJson,
    toJson: _typeToJson,
  )
  @HiveField(3)
  EventType type;

  @JsonKey(
    fromJson: _statusFromJson,
    toJson: _statusToJson,
  )
  @HiveField(4)
  EventStatus status;

  @HiveField(5)
  DateTime createdAt = DateTime.now();

  @HiveField(6)
  DateTime updatedAt = DateTime.now();

  @JsonKey(ignore: true)
  @HiveField(7)
  User sender;

  Event(
      {this.id,
      this.senderId,
      this.receiverId,
      this.type,
      this.status,
      this.createdAt,
      this.updatedAt});

  factory Event.fromJson(Map<String, dynamic> json) => _$EventFromJson(json);

  static List<Event> fromJsonArray(List<dynamic> jsonArray) {
    return JsonUtils.parseArray(
        jsonArray, (jsonObject) => Event.fromJson(jsonObject));
  }

  Map<String, dynamic> toJson() => _$EventToJson(this);

  @override
  String toString() => toJson().toString();
}
