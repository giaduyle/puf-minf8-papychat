import 'package:json_annotation/json_annotation.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

part 'token.g.dart';

@JsonSerializable()
class Token {
  @JsonKey(ignore: true)
  String id;

  String token;

  Token({this.id, this.token});

  bool get expired => JwtDecoder.isExpired(token);

  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);

  Map<String, dynamic> toJson() => _$TokenToJson(this);

  @override
  String toString() => toJson().toString();
}

class AuthToken extends Token {
  static final String tokenId = "AUTH_TOKEN";

  AuthToken(String token) : super(id: tokenId, token: token);

  static AuthToken get unauthenticated => AuthToken(null);
}

class RoomToken extends Token {
  static final String tokenId = "ROOM_TOKEN_";

  final String roomId;

  RoomToken(this.roomId, String token)
      : super(id: "$tokenId$roomId", token: token);
}
