import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:papyrus_chat_app/configs/constants.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

part 'media.g.dart';

@JsonSerializable()
@HiveType(typeId: 4)
class Media {
  @HiveField(0)
  String id;

  @HiveField(1)
  String name;

  @HiveField(2)
  String contentType;

  @HiveField(3)
  DateTime createdAt = DateTime.now();

  @HiveField(4)
  DateTime updatedAt = DateTime.now();

  @JsonKey(ignore: true)
  @HiveField(5)
  dynamic content;

  Media(
      {this.id,
      this.name,
      this.contentType,
      this.content,
      this.createdAt,
      this.updatedAt});

  factory Media.fromJson(Map<String, dynamic> json) => _$MediaFromJson(json);

  static List<Media> fromJsonArray(List<dynamic> jsonArray) {
    return JsonUtils.parseArray(
        jsonArray, (jsonObject) => Media.fromJson(jsonObject));
  }

  Map<String, dynamic> toJson() => _$MediaToJson(this);

  @override
  String toString() => toJson().toString();

  Widget toImageAsset({double width = 60, double height = 60}) => Image.asset(
        content as String,
        width: width,
        height: height,
        fit: BoxFit.cover,
      );

  Widget toImageMemory({double width = 60, double height = 60}) => Image.memory(
        content as Uint8List,
        width: width,
        height: height,
        fit: BoxFit.cover,
      );
}

class AssetMedia extends Media {
  AssetMedia(String pathFromAsset)
      : super(
            id: null,
            contentType: kAssetMediaMessage,
            content: "assets/$pathFromAsset");
}
