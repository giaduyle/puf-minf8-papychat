import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

part 'user.g.dart';

@HiveType(typeId: 11)
enum UserStatus {
  @HiveField(0)
  Offline,

  @HiveField(1)
  Online,

  @HiveField(2)
  Away,

  @HiveField(3)
  Busy
}

_statusFromJson(int literal) =>
    literal == null ? null : UserStatus.values[literal];

_statusToJson(UserStatus status) => status == null ? null : status.index;

@HiveType(typeId: 12)
enum UserGender {
  @HiveField(0)
  Unspecified,

  @HiveField(1)
  Female,

  @HiveField(2)
  Male
}

_genderFromJson(int literal) =>
    literal == null ? null : UserGender.values[literal];

_genderToJson(UserGender gender) => gender == null ? null : gender.index;

@HiveType(typeId: 1)
@JsonSerializable(explicitToJson: true)
class User {
  @HiveField(0)
  String id;

  @HiveField(1)
  String email;

  @HiveField(2)
  String password;

  @HiveField(3)
  String firstName;

  @HiveField(4)
  String lastName;

  @HiveField(5)
  String nickName;

  @HiveField(6)
  String address;

  @HiveField(7)
  String phone;

  @HiveField(8)
  String avatarUrl;

  @HiveField(9)
  DateTime birthday;

  @HiveField(10)
  DateTime lastSeenTime;

  @HiveField(11)
  DateTime createdAt = DateTime.now();

  @HiveField(12)
  DateTime updatedAt = DateTime.now();

  @JsonKey(
    fromJson: _genderFromJson,
    toJson: _genderToJson,
  )
  @HiveField(14)
  UserGender gender;

  @JsonKey(
    fromJson: _statusFromJson,
    toJson: _statusToJson,
  )
  @HiveField(15)
  UserStatus status;

  @JsonKey(ignore: true)
  @HiveField(16)
  bool favourite;

  @JsonKey(ignore: true)
  @HiveField(17)
  bool isMe = false;

  User(
      {this.id,
      this.email,
      this.password,
      this.firstName,
      this.lastName,
      this.nickName,
      this.address,
      this.phone,
      this.avatarUrl,
      this.birthday,
      this.status,
      this.gender,
      this.lastSeenTime,
      this.favourite = false,
      this.createdAt,
      this.updatedAt});

  String get fullName => "$firstName, $lastName";

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  static List<User> fromJsonArray(List<dynamic> jsonArray) {
    return JsonUtils.parseArray(
        jsonArray, (jsonObject) => User.fromJson(jsonObject));
  }

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  String toString() => toJson().toString();
}
