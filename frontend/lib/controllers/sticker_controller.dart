import 'package:papyrus_chat_app/core/models/sticker.dart';
import 'package:papyrus_chat_app/core/repositories/sticker_repository.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class StickerController {
  final StickerRepository _repository = inject();

  List<String> get allCollectionIds => _repository.allCollectionIds;

  List<Sticker> getStickers(String collectionId) {
    return _repository.getStickers(collectionId);
  }

  Sticker getSticker(String collectionId, String stickerId) {
    return _repository.getSticker(collectionId, stickerId);
  }
}
