import 'package:get/get.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/configs/routes.dart';
import 'package:papyrus_chat_app/core/api/auth_api.dart';
import 'package:papyrus_chat_app/core/api/user_api.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/event_repository.dart';
import 'package:papyrus_chat_app/core/repositories/media_repository.dart';
import 'package:papyrus_chat_app/core/repositories/room_repository.dart';
import 'package:papyrus_chat_app/core/repositories/token_repository.dart';
import 'package:papyrus_chat_app/core/repositories/user_repository.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class AuthController {
  AuthApi _authApi = inject();
  UserApi _userApi = inject();
  UserRepository _userRepo = inject();
  TokenRepository _tokenRepo = inject();

  Future<void> login(User user) {
    return _authApi.login(user).then((token) async {
      _tokenRepo.save(token);
      _saveMe(await _userApi.getMe());
      Get.offAllNamed(MainRoute);
    });
  }

  Future<void> googleLogin(String accessToken) {
    return _authApi.google(accessToken).then((token) async {
      _tokenRepo.save(token);
      _saveMe(await _userApi.getMe());
      Get.offAllNamed(MainRoute);
    });
  }

  Future<User> register(User user) {
    return _userApi.create(user).then((created) async {
      await _saveMe(created);
      if (_tokenRepo.getAuthToken() == null) {
        await login(created);
      }
      return created;
    });
  }

  Future<void> logout() async {
    await _tokenRepo.clear();
    await inject<UserRepository>().clear();
    await inject<RoomRepository>().clear();
    await inject<EventRepository>().clear();
    await inject<MediaRepository>().clear();
    Get.offAllNamed(MainRoute);
  }

  bool isAuthenticated() {
    AuthToken token = _tokenRepo.getAuthToken();
    if (token == null) {
      return false;
    }
    if (_userRepo.getMe() == null) {
      logout();
      return false;
    }
    return !token.expired;
  }

  void _saveMe(User user) {
    user.isMe = true;
    _userRepo.putMe(user);
  }

  Future<void> refresh() async {
    return _authApi
        .refresh(_tokenRepo.getAuthToken())
        .then((value) => _tokenRepo.save(value), onError: (error) async {
      logError(error);
      await _tokenRepo.clear();
    });
  }
}
