import 'dart:convert';

import 'package:basic_utils/basic_utils.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:get/get.dart';
import 'package:papyrus_chat_app/configs/constants.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/core/api/chat_api.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';
import 'package:papyrus_chat_app/core/api/media_api.dart';
import 'package:papyrus_chat_app/core/api/room_api.dart';
import 'package:papyrus_chat_app/core/api/user_api.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/media_repository.dart';
import 'package:papyrus_chat_app/core/repositories/room_repository.dart';
import 'package:papyrus_chat_app/core/repositories/user_repository.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class RoomController extends GetxController {
  final UserApi _userApi = inject();
  final RoomApi _roomApi = inject();
  final ChatApi _chatApi = inject();
  final MediaApi _mediaApi = inject();
  final MediaRepository _mediaRepository = inject();
  final UserRepository _userRepo = inject();
  final RoomRepository _myRoomRepo = inject();

  WebSocketChannel _currentChannel;
  Room _currentRoom;

  List<Room> myRooms = <Room>[];
  List<Room> publicRooms = <Room>[];

  RxList<Message> messageStream = <Message>[].obs;
  RxString searchServerStream = "".obs;

  @override
  void onInit() {
    debounce(searchServerStream, (value) async {
      await searchPublicRooms(searchText: value);
    }, time: Duration(milliseconds: 200));
  }

  Future<void> loadMyRooms() async {
    myRooms = _myRoomRepo.getRooms();
    if (myRooms.isEmpty) {
      fetchMyRooms();
    }
    publicRooms.clear();
    await _populateRoomInfo(myRooms);
    update();
  }

  Future<void> fetchMyRooms() async {
    Page page = Page(pageNum: 0, pageSize: 10);
    while (page.hasNext) {
      var rooms = await _userApi.getRooms(_userRepo.getMe().id, page: page);
      if (rooms.data.isEmpty) {
        return;
      }
      for (Room room in rooms.data) {
        await _fetchRoomInfo([room]);
        _myRoomRepo.putRooms([room]);
        if (room.members.isNotEmpty) {
          await _myRoomRepo.putMembers(room, room.members);
        }
      }
      page = rooms.page.next;
    }
    loadMyRooms();
  }

  Future<void> searchMyRooms({String searchText}) async {
    myRooms = _myRoomRepo.searchRooms(searchText);
    await _populateRoomInfo(myRooms);
    update();
  }

  Future<Room> findRoomForUser(User user) async {
    var rooms = _myRoomRepo.getRooms().where((room) {
      return room.members.length <= 2 &&
          room.members.any((element) => element.id == user.id);
    });
    if (rooms.isEmpty) {
      await fetchMyRooms();
      rooms = _myRoomRepo.getRooms().where((room) {
        return room.members.length <= 2 &&
            room.members.any((element) => element.id == user.id);
      });
      if (rooms.isEmpty) {
        return null;
      }
    }
    return rooms.first;
  }

  Future<void> searchPublicRooms({String searchText}) async {
    publicRooms = (await _roomApi.search(searchText,
            page: Page(pageNum: 0, pageSize: 9999)))
        .data;
    await _fetchRoomInfo(publicRooms);
    update();
  }

  Future<Room> addRoom(Room room, List<User> members) async {
    var created = await _roomApi.create(room);
    _myRoomRepo.putRooms([created]);
    await addMembers(created, members);
    await loadMyRooms();
    return created;
  }

  Future<void> removeRoom(Room room) async {
    await _myRoomRepo.deleteRoom(room);
    loadMyRooms();
    await _roomApi.delete(room.id);
  }

  Future<void> updateRoom(Room room) async {
    var updated = await _roomApi.replace(room);
    _fetchRoomInfo([updated]);
    _myRoomRepo.putRooms([updated]);
    update();
  }

  Future<void> addMembers(Room room, List<User> users) async {
    if (IterableUtils.isNullOrEmpty(room.members)) {
      room.members = users;
    } else {
      room.members.addAll(users);
    }
    await _roomApi.addMembers(room.id, users.map((user) => user.id).toList());
    await _myRoomRepo.putMembers(room, users);
    update();
  }

  Future<void> removeMember(Room room, User user) async {
    await _roomApi.removeMember(room.id, user.id);
    if (user.isMe) {
      await _myRoomRepo.deleteRoom(room);
      loadMyRooms();
    } else {
      await _myRoomRepo.removeMembers(room, [user]);
      room.members.removeWhere((member) => member.id == user.id);
      update();
    }
  }

  Future<bool> loadMessages() async {
    if (_currentRoom == null) {
      throw _notJoinedRoomError();
    }
    var messages = await _myRoomRepo.getMessages(_currentRoom);
    if (messages.isNotEmpty) {
      _broadcastInternalMessages(messages);
    }
    return messages.isNotEmpty;
  }

  Future<void> fetchPastMessages() async {
    if (_currentRoom == null) {
      throw _notJoinedRoomError();
    }
    var page = _myRoomRepo.getRoomMessagesPage(_currentRoom);
    var result = await _roomApi.getMessages(_currentRoom.id, page: page);
    List<Message> messages = result.data;
    if (messages.isNotEmpty) {
      messages.sort(Message.descendingSorter);
      var nextPage = Page(beforeTime: messages.last.createdAt);
      await _myRoomRepo.putRoomMessagesPage(_currentRoom, nextPage);
      await _myRoomRepo.putMessages(_currentRoom, messages);
      _broadcastInternalMessages(messages);
    }
  }

  Future<void> fetchNewerMessages() async {
    if (_currentRoom == null) {
      throw _notJoinedRoomError();
    }
    if (_currentRoom.lastMessage == null) {
      return;
    }
    var page = Page(
        pageNum: 0,
        pageSize: 30,
        afterTime: _currentRoom.lastMessage?.createdAt ?? DateTime.now());
    var result = await _roomApi.getMessages(_currentRoom.id, page: page);
    List<Message> messages = result.data;
    if (messages.isNotEmpty) {
      messages.removeWhere((msg) => msg.id == _currentRoom.lastMessage.id);
      await _myRoomRepo.putMessages(_currentRoom, messages);
      _broadcastInternalMessages(messages);
    }
  }

  Future<void> joinRoom(Room room) async {
    if (_currentRoom != null && _currentRoom.id == room.id) {
      return;
    }
    _currentChannel = _chatApi.joinRoom(await _chatApi.getRoomToken(room.id));
    _currentChannel.stream.listen((messageEvent) async {
      _putMessage(Message.fromJson(jsonDecode(messageEvent)));
    });
    _currentRoom = room;
  }

  void _putMessage(Message message) async {
    await _myRoomRepo.putMessages(_currentRoom, [message]);
    _broadcastInternalMessages([message]);
    update();
  }

  void _broadcastInternalMessages(List<Message> messages) {
    messageStream.addAll(messages);
    messageStream.sort(Message.descendingSorter);
  }

  void outCurrentRoom() {
    messageStream.clear();
    _currentChannel?.sink?.close();
    _currentChannel = null;
    _currentRoom = null;
  }

  void sendMessage(Message message) async {
    var messageText = message.content;
    if (message.isMedia) {
      var uploaded = await _mediaApi.upload(message.media);
      uploaded.content = message.media.content;
      await _mediaRepository.putMedia(uploaded);
      messageText = "$kMediaPrefix${uploaded.id}";
    }
    _currentRoom.lastMessage = message;
    _myRoomRepo.putRooms([_currentRoom]);
    logDebug("Sending message: $messageText");
    _currentChannel?.sink?.add(messageText);
  }

  Error _notJoinedRoomError() {
    return StateError(tr("room_not_joined"));
  }

  Future<void> _populateRoomInfo(List<Room> rooms) async {
    for (Room room in rooms) {
      room.members = await _myRoomRepo.getMembers(room);
    }
  }

  Future<void> _fetchRoomInfo(List<Room> rooms) async {
    for (Room room in rooms) {
      if (room.members?.isEmpty ?? true) {
        room.members = (await _roomApi.getMembers(room.id,
                page: Page(pageNum: 0, pageSize: 9999)))
            .data;
      }
      if (room.lastMessage == null) {
        var messages = (await _roomApi.getMessages(room.id,
                page: Page(pageNum: 0, pageSize: 1)))
            .data;
        if (messages.isNotEmpty) {
          room.lastMessage = messages[0];
        }
      }
    }
  }
}
