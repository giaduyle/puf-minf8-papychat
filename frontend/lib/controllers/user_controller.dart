import 'package:get/get.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';
import 'package:papyrus_chat_app/core/api/user_api.dart';
import 'package:papyrus_chat_app/core/models/event.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/event_repository.dart';
import 'package:papyrus_chat_app/core/repositories/user_repository.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class UserController extends GetxController {
  final UserApi _userApi = inject();
  final UserRepository _userRepo = inject();
  final EventRepository _eventRepo = inject();

  var friends = <User>[];
  var strangers = <User>[];
  var events = <Event>[];

  List<User> get blocks => _userRepo.getBlocks();

  RxString searchServerStream = "".obs;

  @override
  void onInit() async {
    debounce(searchServerStream, (value) async => await searchStrangers(value),
        time: Duration(milliseconds: 300));
  }

  User whoAmI() {
    return _userRepo.getMe();
  }

  User getFriend(String userId) {
    return _userRepo.getFriend(userId);
  }

  bool isFriend(User user) {
    return _userRepo.getFriend(user.id) != null;
  }

  bool isStranger(User user) {
    return !isMe(user) && !isFriend(user);
  }

  Future<User> refreshMe() async {
    var updatedMe = await _userApi.getMe();
    _userRepo.putMe(updatedMe);
    return updatedMe;
  }

  User whereAmI(List<User> users) {
    users.forEach((user) {
      if (user.id == _userRepo.getMe().id) {
        return user;
      }
    });
    return null;
  }

  bool isMe(User user) {
    User me = _userRepo.getMe();
    if (me == null) {
      return false;
    }
    return me.id == user.id;
  }

  Future<User> updateMe(User user) async {
    User updatedUser = await _userApi.replace(user);
    _userRepo.putMe(user);
    update();
    return updatedUser;
  }

  Future<User> deleteMe(User user) async {
    User deletedUser = await _userApi.delete(user.id);
    _userRepo.delete();
    update();
    return deletedUser;
  }

  Future<void> searchStrangers(String text) async {
    strangers =
        (await _userApi.search(text, page: Page(pageNum: 0, pageSize: 9999)))
            .data;
    update();
  }

  Future<void> loadFriends() async {
    this.friends = _userRepo.getFriends();
    update();
  }

  Future<void> fetchFriends() async {
    var friends = (await _userApi.getFriends(_userRepo.getMe().id));
    var favourites = Map.fromIterable(
        await _userApi.getFavourites(_userRepo.getMe().id),
        key: (e) => e.id,
        value: (e) => e);
    friends.forEach((friend) {
      friend.favourite = favourites.containsKey(friend.id);
    });
    _userRepo.putFriends(friends);
    _userRepo.putBlocks((await _userApi.getBlocks(_userRepo.getMe().id)));
    loadFriends();
  }

  Future<void> searchFriends(String text) async {
    friends = _userRepo.searchFriends(text);
    update();
  }

  Future<void> addFriend(User user) async {
    await _userApi.addFriend(_userRepo.getMe().id, [user.id]);
    loadFriends();
  }

  Future<void> unFriend(User user) async {
    await _userApi.removeFriend(_userRepo.getMe().id, user.id);
    _userRepo.unFriend(user.id);
    loadFriends();
  }

  Future<void> addFavourite(User user) async {
    await _userApi.addFavourite(_userRepo.getMe().id, [user.id]);
    user.favourite = true;
    _userRepo.putFriends([user]);
    loadFriends();
  }

  Future<void> removeFavourite(User user) async {
    await _userApi.removeFavourite(_userRepo.getMe().id, user.id);
    user.favourite = false;
    _userRepo.putFriends([user]);
    loadFriends();
  }

  Future<void> block(User user) async {
    _userRepo.putBlocks(await _userApi.block(_userRepo.getMe().id, [user.id]));
    _userRepo.unFriend(user.id);
    loadFriends();
  }

  Future<void> unblock(User user) async {
    await _userApi.unblock(_userRepo.getMe().id, user.id);
    _userRepo.unblock(user.id);
    _userRepo.putFriends([user]);
    loadFriends();
  }

  Future<void> updateEvent(User user, Event event) async {
    var updated = await _userApi.updateEvent(user.id, event);
    if (event.status == EventStatus.Accepted ||
        event.status == EventStatus.Denied) {
      var events = _eventRepo.getUserEvents(user);
      events.removeWhere((event) => event.id == updated.id);
      await _eventRepo.clearUserEvents(user);
      await _eventRepo.putUserEvents(user, events);
    }
    await loadEvents(user);
  }

  Future<void> loadEvents(User user) async {
    events = _eventRepo.getUserEvents(user);
    update();
  }

  Future<void> fetchIncomingEvents(User user) async {
    var events = (await _userApi.getEvents(user.id,
            status: EventStatus.New, page: Page(pageNum: 0, pageSize: 9999)))
        .data;
    for (Event event in events) {
      var user = await _userApi.get(event.senderId);
      event.sender = user;
    }
    await _eventRepo.clearUserEvents(user);
    await _eventRepo.putUserEvents(user, events);
    await loadEvents(user);
  }
}
