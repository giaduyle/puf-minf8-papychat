import 'package:papyrus_chat_app/core/api/media_api.dart';
import 'package:papyrus_chat_app/core/models/media.dart';
import 'package:papyrus_chat_app/core/repositories/media_repository.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class MediaController {
  final MediaApi _mediaApi = inject();
  final MediaRepository _mediaRepository = inject();

  Future<Media> upload(Media media) async {
    Media uploaded = await _mediaApi.upload(media);
    await _mediaRepository.putMedias([uploaded]);
    return uploaded;
  }

  Future<Media> get(String id) async {
    var media = await _mediaRepository.getMedia(id);
    if (media != null) {
      return media;
    }
    var downloaded = await _mediaApi.get(id);
    downloaded.content = (await _mediaApi.download(id)).content;
    await _mediaRepository.putMedia(downloaded);
    return downloaded;
  }
}
