import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart';
import 'package:crypto/crypto.dart';
import 'package:duration/duration.dart';
import 'package:duration/locale.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:encrypt/encrypt.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:papyrus_chat_app/configs/constants.dart';
import 'package:papyrus_chat_app/configs/locales.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:papyrus_chat_app/core/repositories/token_repository.dart';
import 'package:password/password.dart';
import 'package:tuple/tuple.dart';

T inject<T>() {
  return Get.find<T>();
}

Future<T> injectFuture<T>() {
  return inject<Future<T>>();
}

void deleteInjection<T>() {
  Get.delete<T>();
}

class AuthUtils {
  AuthUtils._();

  static Map<String, String> buildAuthenticatedHeaders() {
    AuthToken token = inject<TokenRepository>().getAuthToken();
    if (token == null) {
      return kDefaultHeaders;
    }
    Map<String, String> headers = Map.from(kDefaultHeaders);
    headers[HttpHeaders.authorizationHeader] = "Bearer ${token.token}";
    return headers;
  }
}

class JsonUtils {
  JsonUtils._();

  static List<T> parseArray<T>(List<dynamic> jsonArray,
      T Function(Map<String, dynamic> jsonObject) parser) {
    if (IterableUtils.isNullOrEmpty(jsonArray)) {
      return List.empty();
    }
    return jsonArray.map<T>((jsonObject) => parser(jsonObject)).toList();
  }
}

class PRStringUtils {
  PRStringUtils._();

  static String truncateWithEllipsis(int cutoff, String myString) {
    if (myString == null) {
      return "";
    }
    return (myString.length <= cutoff)
        ? myString
        : '${myString.substring(0, cutoff)}...';
  }
}

class DurationUtils {
  DurationUtils._();

  static String formatDurationTillNow(DateTime instant, DateFormat format,
      {String prefix = "",
      String suffixOnDuration = "",
      bool abbreviated = false}) {
    if (instant == null) {
      return "";
    }
    Duration duration = instant.difference(DateTime.now());
    if (duration.inDays.abs() > DateTime.daysPerWeek) {
      return "$prefix ${format.format(instant)}";
    }
    String durationString =
        _formatAutoTranslatedDuration(duration, abbreviated: abbreviated);
    return "$prefix $durationString $suffixOnDuration";
  }

  static String _formatAutoTranslatedDuration(Duration duration,
      {bool abbreviated = false}) {
    return _formatDuration(duration, abbreviated: abbreviated);
  }

  static String _formatDuration(Duration duration,
      {String languageCode, bool abbreviated = false}) {
    if (duration == null) {
      return null;
    }
    DurationTersity tersity = duration.inDays.abs() <= 1
        ? DurationTersity.minute
        : DurationTersity.day;
    if (languageCode == null || languageCode.isEmpty) {
      return prettyDuration(duration,
          abbreviated: abbreviated,
          tersity: tersity,
          locale: AutoTranslatedDurationLocale());
    }
    return prettyDuration(duration,
        abbreviated: abbreviated,
        tersity: tersity,
        locale: DurationLocale.fromLanguageCode(languageCode));
  }
}

class DateTimeUtils {
  DateTimeUtils._();

  static DateTime randomDateTime({DateTime start, DateTime end}) {
    DateTime min = start ?? DateTime.fromMillisecondsSinceEpoch(0);
    DateTime max = end ?? DateTime.now();
    if (min.compareTo(max) > 0) {
      throw ArgumentError("Start: '$start' > End: '$end'");
    }
    return max.subtract(
        Duration(hours: Random().nextInt(max.difference(min).inHours)));
  }
}

class EnumUtils<E> {
  EnumUtils._();

  static E randomEnum<E>(List<E> enumValues) {
    return enumValues[Random().nextInt(enumValues.length - 1)];
  }
}

class SecurityUtils {
  SecurityUtils._();

  static Key secretKey() {
    return Key.fromUtf8(getSecretPhrase());
  }

  static Encrypted encryptAES(String plainText) {
    return Encrypter(AES(secretKey()))
        .encrypt(plainText, iv: IV.fromLength(16));
  }

  static String decryptAES(Encrypted encrypted) {
    return Encrypter(AES(secretKey()))
        .decrypt(encrypted, iv: IV.fromLength(16));
  }

  static String hash(String text) {
    return Password.hash(text, PBKDF2());
  }

  static bool verifyHash(String text, String hash) {
    return Password.verify(text, hash);
  }

  // Docs: https://medium.com/@chingsuehok/cryptojs-aes-encryption-decryption-for-flutter-dart-7ca123bd7464
  static String encryptAESCryptoJS(String plainText, String passphrase) {
    try {
      final salt = genRandomWithNonZero(8);
      var keyAndIV = deriveKeyAndIV(passphrase, salt);
      final key = Key(keyAndIV.item1);
      final iv = IV(keyAndIV.item2);
      final encrypted = Encrypter(AES(key, mode: AESMode.cbc, padding: "PKCS7"))
          .encrypt(plainText, iv: iv);
      Uint8List encryptedBytesWithSalt = Uint8List.fromList(
          createUint8ListFromString("Salted__") + salt + encrypted.bytes);
      return base64.encode(encryptedBytesWithSalt);
    } catch (error) {
      throw error;
    }
  }

  static String encryptAESCryptoJSDefaultSecret(String plainText) {
    return encryptAESCryptoJS(plainText, getSecretPhrase());
  }

  static String decryptAESCryptoJS(String encrypted, String passphrase) {
    try {
      Uint8List encryptedBytesWithSalt = base64.decode(encrypted);
      Uint8List encryptedBytes =
          encryptedBytesWithSalt.sublist(16, encryptedBytesWithSalt.length);
      var keyAndIV =
          deriveKeyAndIV(passphrase, encryptedBytesWithSalt.sublist(8, 16));
      final key = Key(keyAndIV.item1);
      final iv = IV(keyAndIV.item2);
      final decrypted = Encrypter(AES(key, mode: AESMode.cbc, padding: "PKCS7"))
          .decrypt64(base64.encode(encryptedBytes), iv: iv);
      return decrypted;
    } catch (error) {
      throw error;
    }
  }

  static String decryptAESCryptoJSDefaultSecret(String encrypted) {
    return decryptAESCryptoJS(encrypted, getSecretPhrase());
  }

  static Tuple2<Uint8List, Uint8List> deriveKeyAndIV(
      String passphrase, Uint8List salt) {
    var password = createUint8ListFromString(passphrase);
    Uint8List concatenatedHashes = Uint8List(0);
    Uint8List currentHash = Uint8List(0);
    bool enoughBytesForKey = false;
    Uint8List preHash = Uint8List(0);
    while (!enoughBytesForKey) {
      if (currentHash.length > 0)
        preHash = Uint8List.fromList(currentHash + password + salt);
      else
        preHash = Uint8List.fromList(password + salt);

      currentHash = md5.convert(preHash).bytes;
      concatenatedHashes = Uint8List.fromList(concatenatedHashes + currentHash);
      if (concatenatedHashes.length >= 48) enoughBytesForKey = true;
    }
    var keyBytes = concatenatedHashes.sublist(0, 32);
    var ivBytes = concatenatedHashes.sublist(32, 48);
    return new Tuple2(keyBytes, ivBytes);
  }

  static Uint8List createUint8ListFromString(String s) {
    var ret = new Uint8List(s.length);
    for (var i = 0; i < s.length; i++) {
      ret[i] = s.codeUnitAt(i);
    }
    return ret;
  }

  static Uint8List genRandomWithNonZero(int seedLength) {
    final random = Random.secure();
    const int randomMax = 245;
    final Uint8List uint8list = Uint8List(seedLength);
    for (int i = 0; i < seedLength; i++) {
      uint8list[i] = random.nextInt(randomMax) + 1;
    }
    return uint8list;
  }

  static String getSecretPhrase() {
    String secret = GlobalConfiguration().getString("secret");
    if (StringUtils.isNullOrEmpty(secret)) {
      throw StateError("Secret key has not been configured");
    }
    return secret;
  }
}

enum SettingNotification { SHOW_PREVIEW, SHOW_CHAT_NOTI, SHOW_GROUP_NOTI }
