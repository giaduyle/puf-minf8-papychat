import 'dart:math';

import 'package:papyrus_chat_app/core/models/event.dart';
import 'package:papyrus_chat_app/core/models/media.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:random_string/random_string.dart';

String randomAvatar({UserGender gender = UserGender.Unspecified}) {
  if (gender == UserGender.Male) {
    var avatars = _avatarUrlsGender['male'];
    return avatars[Random().nextInt(avatars.length)];
  }
  if (gender == UserGender.Female) {
    var avatars = _avatarUrlsGender['female'];
    return avatars[Random().nextInt(avatars.length)];
  }
  return _avatarUrls[Random().nextInt(_avatarUrls.length)];
}

User mockUser({bool randomId = true}) {
  var gender = EnumUtils.randomEnum(UserGender.values);
  return User(
      id: randomId ? randomString(5) : null,
      email: "${randomString(10)}@email.com",
      password: randomString(5),
      nickName: randomString(10),
      firstName: randomString(10),
      lastName: randomString(10),
      phone: randomNumeric(9),
      favourite: Random().nextBool(),
      avatarUrl: randomAvatar(gender: gender),
      gender: gender,
      lastSeenTime: _mockDateTime(),
      createdAt: _mockDateTime(),
      updatedAt: _mockDateTime(),
      status: EnumUtils.randomEnum(UserStatus.values));
}

List<User> mockUserList({int size = 3}) =>
    List<User>.generate(size, (index) => mockUser());

Message mockMessage({bool randomId = true}) => Message(
    id: randomId ? randomString(5) : null,
    senderId: randomString(5),
    content: randomString(15),
    createdAt: _mockDateTime(),
    updatedAt: _mockDateTime(),
    roomId: randomString(10));

List<Message> mockMessageList({int size = 30}) {
  List<Message> messages =
      List<Message>.generate(size, (index) => mockMessage());
  messages.sort((m1, m2) {
    return m1.createdAt.compareTo(m2.createdAt);
  });
  return List.of(messages.reversed);
}

Room mockRoom(
        {bool randomId = true,
        int memberSize = 5,
        bool mockLastMessage = true}) =>
    Room(
        id: randomId ? randomString(5) : null,
        name: randomString(10),
        isPrivate: Random().nextBool(),
        members: mockUserList(size: memberSize),
        createdAt: _mockDateTime(),
        updatedAt: _mockDateTime(),
        lastMessage: mockLastMessage ? mockMessage() : null);

List<Room> mockRoomList({size: 20, int memberSize = 5}) =>
    List<Room>.generate(size, (index) => mockRoom(memberSize: memberSize));

Event mockEvent({bool randomId = true}) => Event(
    id: randomId ? randomString(5) : null,
    senderId: randomString(10),
    receiverId: randomString(10),
    type: EventType.InviteFriend,
    status: EnumUtils.randomEnum(EventStatus.values),
    createdAt: _mockDateTime(),
    updatedAt: _mockDateTime());

Media mockMedia({bool randomId = true}) => Media(
    id: randomId ? randomString(5) : null,
    name: randomString(5),
    contentType: randomString(10),
    content: randomString(256).codeUnits,
    createdAt: _mockDateTime(),
    updatedAt: _mockDateTime());

DateTime _mockDateTime({Duration range}) {
  range = range ?? Duration(days: 15);
  return DateTimeUtils.randomDateTime(
      start: DateTime.now().subtract(Duration(days: 15)), end: DateTime.now());
}

const _avatarUrlsGender = {
  "male": [
    "assets/images/boy_avatar_1.png",
    "assets/images/boy_avatar_2.png",
  ],
  "female": [
    "assets/images/girl_avatar_1.png",
    "assets/images/girl_avatar_2.png"
  ]
};

const _avatarUrls = [
  "assets/images/boy_avatar_1.png",
  "assets/images/boy_avatar_2.png",
  "assets/images/girl_avatar_1.png",
  "assets/images/girl_avatar_2.png"
];
