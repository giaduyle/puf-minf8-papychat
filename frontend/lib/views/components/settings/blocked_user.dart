import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/views/components/user/user_summary.dart';

class BlockedUser extends StatelessWidget {
  final User _user;
  final Function(User user) onUnblocked;

  BlockedUser(this._user, {this.onUnblocked});

  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: SlidableScrollActionPane(),
      actionExtentRatio: 0.12,
      child: UserSummary(_user),
      secondaryActions: _actions(context),
    );
  }

  List<Widget> _actions(BuildContext context) {
    return [
      IconSlideAction(
        icon: Icons.undo,
        caption: "unblock".tr(),
        foregroundColor: Colors.red,
        onTap: () {
          if (onUnblocked != null) {
            onUnblocked(_user);
          }
        },
      )
    ];
  }
}
