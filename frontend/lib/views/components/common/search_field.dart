import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/views/components/common/text_field.dart';

class PRSearchField extends StatelessWidget {
  final String hintText;
  final Function onChanged;

  PRSearchField({this.hintText = 'search', this.onChanged});

  @override
  Widget build(BuildContext context) {
    return PRTextField(
      labelText: hintText.tr(),
      fillColor: kGreyColor,
      prefixIcon: Icon(Icons.search),
      isDense: true,
      floatingLabelBehavior: FloatingLabelBehavior.never,
      onChanged: onChanged,
    );
  }
}
