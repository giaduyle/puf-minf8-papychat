import 'dart:async';
import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:tuple/tuple.dart';
import 'package:universal_html/html.dart' as UniversalHtml;

class PRFileButton extends StatelessWidget {
  final double size;
  final double optionIconSize;
  final Function(Tuple2<String, Uint8List> file) onFilePicked;

  final picker = ImagePicker();

  PRFileButton({this.size = 20, this.optionIconSize = 20, this.onFilePicked});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Container(
      child: PopupMenuButton(
        offset: Offset(0, -100),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        onSelected: (option) async {
          if (onFilePicked != null) {
            onFilePicked(await _getImage(option));
          }
        },
        child: Icon(
          Icons.attach_file,
          size: size,
          color: theme.primaryColorDark,
        ),
        itemBuilder: (context) => <PopupMenuEntry<ImageSource>>[
          kIsWeb
              ? null
              : PopupMenuItem(
                  value: ImageSource.camera,
                  child: _button(context, "camera".tr(), Icons.photo_camera)),
          PopupMenuItem(
              value: ImageSource.gallery,
              child: _button(context, "photos".tr(), Icons.photo))
        ],
      ),
    );
  }

  Widget _button(BuildContext context, String text, IconData icon) {
    return Wrap(
      spacing: 10,
      children: [
        Icon(
          icon,
          color: Theme.of(context).primaryColorDark,
        ),
        Text(text)
      ],
    );
  }

  Future<Tuple2<String, Uint8List>> _getImage(source) async {
    if (kIsWeb) {
      return await _getHtmlFile();
    } else {
      final PickedFile pickedFile = await picker.getImage(source: source);
      if (pickedFile == null) {
        return null;
      }
      return Tuple2(basename(pickedFile.path), await pickedFile.readAsBytes());
    }
  }

  Future<Tuple2<String, Uint8List>> _getHtmlFile() async {
    final completer = new Completer<Tuple2<String, Uint8List>>();
    final UniversalHtml.InputElement input =
        UniversalHtml.document.createElement('input');
    input
      ..type = 'file'
      ..accept = 'image/*';
    input.onChange.listen((e) async {
      final List<UniversalHtml.File> files = input.files;
      final reader = new UniversalHtml.FileReader();
      reader.readAsArrayBuffer(files[0]);
      reader.onError.listen((error) => completer.completeError(error));
      await reader.onLoad.first;
      completer.complete(Tuple2(files[0].name, reader.result as Uint8List));
    });
    input.click();
    return completer.future;
  }
}
