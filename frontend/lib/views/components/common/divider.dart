import 'package:flutter/material.dart';

class PRDivider extends StatelessWidget {
  final double indents;
  final double thickness;
  final Color color;

  PRDivider({this.indents = 50, this.thickness = 0.5, this.color});

  @override
  Widget build(BuildContext context) {
    return Divider(
      indent: indents,
      endIndent: indents,
      thickness: thickness,
      color: this.color,
    );
  }
}
