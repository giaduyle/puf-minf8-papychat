import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/auth_controller.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/flat_button.dart';

class GoogleSignInButton extends StatelessWidget {
  final AuthController _controller = inject();

  @override
  Widget build(BuildContext context) {
    return PRFlatButton(
        text: "login_google".tr(),
        height: 50.0,
        width: kFieldAndButtonWidth,
        color: Theme.of(context).accentColor,
        textColor: Colors.black,
        iconData: FontAwesomeIcons.googlePlus,
        iconColor: Colors.red,
        onPressed: () async {
          GoogleSignIn _googleSignIn = GoogleSignIn(
            scopes: ['email', 'profile'],
          );
          try {
            GoogleSignInAccount account = await _googleSignIn.signIn();
            if (account == null) {
              return;
            }
            GoogleSignInAuthentication auth = await account.authentication;
            await _controller.googleLogin(auth.accessToken);
          } catch (error) {
            await _controller.logout();
            showError(context: context, error: error);
          }
        });
  }
}
