import 'package:flutter/material.dart';

class PRNoData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Image(
      image: AssetImage("assets/images/undraw_empty.png"),
    ));
  }
}
