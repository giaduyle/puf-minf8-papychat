import 'package:basic_utils/basic_utils.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/configs/size.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/views/components/common/flat_button.dart';
import 'package:papyrus_chat_app/views/components/common/text_button.dart';

Future<bool> showConfirmDialog(
    {@required BuildContext context,
    @required String title,
    @required String message}) {
  return showDialog<bool>(
      context: context,
      builder: (context) => _PRDialog(
            title: title,
            message: message,
            buttonType: _PRButtonType.CONFIRMATION,
          ));
}

Future<Map<String, String>> showEditDialog(
    {@required BuildContext context,
    String title,
    String message,
    @required Map<String, String> values}) {
  return showDialog<Map<String, String>>(
      context: context,
      builder: (context) => _PRDialog(
            title: title = "edit".tr(),
            message: message,
            values: values,
            buttonType: _PRButtonType.SAVE_EDITS,
          ));
}

void showError({@required BuildContext context, @required dynamic error}) {
  showDialog(
      context: context,
      builder: (context) => _PRDialog(
            title: 'error'.tr(),
            message: error?.toString(),
            buttonType: _PRButtonType.CLOSE_ONLY,
          ));
}

void showInformation(
    {@required BuildContext context,
    @required String title,
    @required String message}) {
  showDialog(
      context: context,
      builder: (context) => _PRDialog(
            title: title,
            message: message,
            buttonType: _PRButtonType.CLOSE_ONLY,
          ));
}

enum _PRButtonType { CONFIRMATION, SAVE_EDITS, CLOSE_ONLY }

class _PRDialog extends StatefulWidget {
  final String title;
  final String message;
  final Map<String, String> values;
  final _PRButtonType buttonType;

  _PRDialog(
      {@required this.title,
      this.message,
      this.values,
      this.buttonType = _PRButtonType.CLOSE_ONLY})
      : assert(buttonType == _PRButtonType.SAVE_EDITS
            ? values != null && IterableUtils.isNotNullOrEmpty(values.entries)
            : true);

  @override
  __PRDialogState createState() => __PRDialogState();
}

class __PRDialogState extends State<_PRDialog> {
  Map<String, String> _newValues;

  @override
  void initState() {
    super.initState();
    if (widget.values != null) {
      _newValues = Map<String, String>.from(widget.values);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Container(alignment: Alignment.center, child: Text(widget.title)),
      titleTextStyle:
          kTitleTextStyle.copyWith(color: Colors.black, fontSize: 25),
      elevation: 5.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      children: [
        Container(
          width: getProportionateScreenWidth(150),
          child: Column(
            children: [
              _message(context),
              _valueSection(context),
              _buttonsSection(context)
            ],
          ),
        )
      ],
    );
  }

  Widget _message(BuildContext context) {
    if (StringUtils.isNullOrEmpty(widget.message)) {
      return SizedBox.shrink();
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 30.0),
      child: Text(widget.message,
          style: kInfoTextStyle.copyWith(color: Colors.black, fontSize: 15)),
    );
  }

  Widget _valueSection(BuildContext context) {
    if (widget.values == null) {
      return SizedBox.shrink();
    }
    return Container(
      margin: EdgeInsets.only(bottom: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: widget.values.entries.map<Widget>((entry) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("${entry.key}: "),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: TextFormField(
                  initialValue: entry.value,
                  onChanged: (newValue) {
                    setState(() {
                      _newValues[entry.key] = newValue;
                      _newValues = _newValues;
                    });
                  },
                ),
              ),
            ],
          );
        }).toList(),
      ),
    );
  }

  Widget _buttonsSection(BuildContext context) {
    switch (widget.buttonType) {
      case _PRButtonType.CONFIRMATION:
        return _confirmButtons(context);
      case _PRButtonType.SAVE_EDITS:
        return _saveCancelButtons(context);
      case _PRButtonType.CLOSE_ONLY:
        return _closeButton(context);
      default:
        return SizedBox.shrink();
    }
  }

  Widget _confirmButtons(BuildContext context) {
    return Wrap(
      direction: Axis.vertical,
      runSpacing: 40,
      spacing: 20,
      runAlignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        PRFlatButton(
          text: "yes".tr(),
          width: getProportionateScreenWidth(150),
          height: getProportionateScreenHeight(35),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
        PRTextButton(
            text: "cancel".tr(),
            onPressed: () {
              Navigator.pop(context, false);
            })
      ],
    );
  }

  Widget _saveCancelButtons(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.end,
      crossAxisAlignment: WrapCrossAlignment.center,
      spacing: 20,
      children: [
        PRFlatButton(
          text: "save".tr(),
          width: getProportionateScreenWidth(75),
          height: getProportionateScreenHeight(35),
          onPressed: () {
            Navigator.pop(context, _newValues);
          },
        ),
        PRFlatButton(
          text: "cancel".tr(),
          width: getProportionateScreenWidth(75),
          height: getProportionateScreenHeight(35),
          textColor: Colors.black,
          color: kGreyColor,
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ],
    );
  }

  Widget _closeButton(BuildContext context) {
    return PRFlatButton(
      text: "close".tr(),
      width: getProportionateScreenWidth(150),
      height: getProportionateScreenHeight(35),
      textColor: Colors.black,
      color: kGreyColor,
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }
}
