import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/views/components/common/divider.dart';

class PRInformationField extends StatelessWidget {
  final String title;
  final String value;
  final IconData icon;
  final Function(String title, String value) onPressed;

  PRInformationField(this.title, this.value, {this.icon, this.onPressed});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        children: [
          InkWell(
            splashColor: theme.primaryColorLight,
            highlightColor: Colors.transparent,
            hoverColor: theme.accentColor,
            onTap: () {
              if (onPressed != null) {
                onPressed(title, value);
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Row(
                children: [
                  this.icon != null
                      ? Icon(
                          icon,
                          color: theme.primaryColor,
                        )
                      : SizedBox.shrink(),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text(
                      title,
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(value,
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.black,
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          PRDivider(
            indents: 0,
            thickness: 1,
            color: theme.primaryColor,
          )
        ],
      ),
    );
  }
}
