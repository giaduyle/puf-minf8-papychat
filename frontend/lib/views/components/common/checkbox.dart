import 'package:flutter/material.dart';

class PRCheckBox extends StatefulWidget {
  final bool value;
  final Function(bool value) onValueChanged;
  final IconData checkedIconData;
  final IconData uncheckedIconData;
  final double iconSize;

  PRCheckBox(
      {this.value = false,
      this.onValueChanged,
      this.checkedIconData,
      this.uncheckedIconData,
      this.iconSize = 25});

  @override
  _PRCheckBoxState createState() => _PRCheckBoxState();
}

class _PRCheckBoxState extends State<PRCheckBox> {
  bool _value;

  @override
  void initState() {
    super.initState();
    _value = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Container(
      width: widget.iconSize,
      child: GestureDetector(
          onTap: () {
            setState(() {
              _value = !_value;
            });
            if (widget.onValueChanged != null) {
              widget.onValueChanged(_value);
            }
          },
          child: _value
              ? Icon(widget.checkedIconData ?? Icons.radio_button_checked,
                  size: widget.iconSize, color: theme.primaryColor)
              : Icon(widget.uncheckedIconData ?? Icons.radio_button_unchecked,
                  size: widget.iconSize, color: theme.primaryColor)),
    );
  }
}
