import 'package:flutter/material.dart';

class PRTextButton extends StatefulWidget {
  final String text;
  final Function() onPressed;

  PRTextButton({@required this.text, this.onPressed});

  @override
  _PRTextButtonState createState() => _PRTextButtonState();
}

class _PRTextButtonState extends State<PRTextButton> {
  bool _pressed;

  @override
  void initState() {
    super.initState();
    _pressed = false;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return GestureDetector(
      onTapDown: (details) => switchPressedState(),
      onTapUp: (details) => switchPressedState(),
      onTapCancel: () => switchPressedState(),
      onTap: () {
        if (widget.onPressed != null) {
          widget.onPressed();
        }
      },
      child: Text(
        widget.text,
        style: TextStyle(
            color: _pressed ? theme.primaryColorLight : theme.primaryColorDark),
      ),
    );
  }

  void switchPressedState() {
    setState(() {
      _pressed = !_pressed;
    });
  }
}
