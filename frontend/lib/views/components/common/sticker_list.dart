import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/controllers/sticker_controller.dart';
import 'package:papyrus_chat_app/core/models/sticker.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/divider.dart';

class StickerList extends StatelessWidget {
  final Function(Sticker sticker) onStickerPressed;
  final StickerController controller = inject();

  StickerList({this.onStickerPressed});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PRDivider(
          indents: 0,
        ),
        SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Wrap(
            runSpacing: 20,
            children: _buildStickerList(),
          ),
        ),
      ],
    );
  }

  List<Widget> _buildStickerList() {
    List<Sticker> stickers = controller.getStickers("mimi");
    return stickers
        .map((sticker) => FlatButton(
              onPressed: () {
                if (onStickerPressed != null) {
                  onStickerPressed(sticker);
                }
              },
              child: sticker.media.toImageAsset(),
            ))
        .toList();
  }
}
