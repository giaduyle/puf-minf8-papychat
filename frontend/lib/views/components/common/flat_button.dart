import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PRFlatButton extends StatelessWidget {
  final String text;
  final double height;
  final double width;
  final Function onPressed;
  final Color color;
  final Color textColor;
  final IconData iconData;
  final Color iconColor;

  PRFlatButton(
      {@required this.text,
      this.width,
      this.height,
      this.onPressed,
      this.color,
      this.textColor,
      this.iconData,
      this.iconColor});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: this.height,
      width: this.width,
      child: FlatButton(
          color: this.color ?? Theme.of(context).primaryColor,
          onPressed: this.onPressed,
          padding: EdgeInsets.symmetric(vertical: 10.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _icon(context),
              Text(
                this.text,
                style: Theme.of(context).textTheme.button.copyWith(
                    color: this.textColor ?? Colors.white, fontSize: 15),
              )
            ],
          )),
    );
  }

  Widget _icon(BuildContext context) {
    return this.iconData != null
        ? Container(
            margin: EdgeInsets.only(right: 20),
            child: FaIcon(this.iconData,
                color: this.iconColor ??
                    this.color ??
                    Theme.of(context).primaryColor),
          )
        : Container();
  }
}
