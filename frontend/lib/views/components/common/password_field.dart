import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:papyrus_chat_app/views/components/common/text_field.dart';

const String kValidPasswordPattern = r'(?=.*?[#?!@$%^&*-])';

class PRPasswordField extends StatefulWidget {
  final TextEditingController controller;

  PRPasswordField({this.controller});

  @override
  _PRPasswordFieldState createState() => _PRPasswordFieldState();
}

class _PRPasswordFieldState extends State<PRPasswordField> {
  bool _passwordVisible;

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return PRTextField(
      labelText: "password_need".tr(),
      obscureText: !_passwordVisible,
      controller: widget.controller,
      validator: MultiValidator([
        RequiredValidator(errorText: 'required'.tr()),
        MinLengthValidator(6, errorText: 'password_invalid'.tr()),
        PatternValidator(kValidPasswordPattern,
            errorText: 'password_invalid'.tr())
      ]),
      suffixIcon: IconButton(
        icon: Icon(
          _passwordVisible ? Icons.visibility : Icons.visibility_off,
          color: Theme.of(context).primaryColorDark,
        ),
        onPressed: () {
          setState(() {
            _passwordVisible = !_passwordVisible;
          });
        },
      ),
    );
  }
}
