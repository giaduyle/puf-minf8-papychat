import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

class PRSettingRow extends StatefulWidget {
  final String title;
  final SettingNotification notificationType;

  PRSettingRow(this.title, this.notificationType);

  @override
  _PRSettingRowState createState() => _PRSettingRowState();
}

class _PRSettingRowState extends State<PRSettingRow> {
  bool _isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        child: InkWell(
          child: Row(
            children: [
              Expanded(
                child: new Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: Text(
                      widget.title,
                      style: new TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                      ),
                    )),
              ),
              Switch(
                value: _isSwitched,
                onChanged: (value) {
                  setState(() {
                    _isSwitched = value;
                    configShowNotification(widget.notificationType);
                  });
                },
                activeTrackColor: Theme.of(context).primaryColorLight,
                activeColor: Theme.of(context).primaryColor,
              )
            ],
          ),
        ));
  }

  void configShowNotification(notificationType) {
    switch (notificationType) {
      case SettingNotification.SHOW_PREVIEW:
        {
          print("show preview");
          break;
        }
      case SettingNotification.SHOW_CHAT_NOTI:
        {
          print("show chat noti");
          break;
        }
      case SettingNotification.SHOW_GROUP_NOTI:
        {
          print("show group noti");
          break;
        }
      default:
        break;
    }
  }
}
