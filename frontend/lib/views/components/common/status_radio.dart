import 'package:flutter/material.dart';

class PRStatusRadio<T> extends StatelessWidget {
  final String text;
  final T value;
  final T groupValue;
  final Color color;
  final double width;
  final double height;
  final Function(T value) onChanged;

  PRStatusRadio(
      {@required this.text,
      @required this.color,
      @required this.value,
      @required this.groupValue,
      this.width = double.infinity,
      this.height = 40,
      this.onChanged});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Container(
      width: width,
      height: height,
      child: InkWell(
        borderRadius: BorderRadius.circular(15),
        onTap: () {
          if (onChanged != null) {
            onChanged(value);
          }
        },
        splashColor: theme.primaryColorLight,
        highlightColor: Colors.transparent,
        hoverColor: theme.accentColor,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            children: [
              Icon(
                Icons.lens,
                color: color,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Text(text),
              ),
              (value == groupValue)
                  ? Expanded(
                      child: Container(
                        alignment: AlignmentDirectional.centerEnd,
                        child: Icon(
                          Icons.beenhere,
                          color: theme.primaryColor,
                        ),
                      ),
                    )
                  : SizedBox.shrink()
            ],
          ),
        ),
      ),
    );
  }
}
