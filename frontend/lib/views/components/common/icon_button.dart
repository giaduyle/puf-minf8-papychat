import 'package:flutter/material.dart';

class PRIconButton extends StatefulWidget {
  final IconData icon;
  final double size;
  final Function() onPressed;

  PRIconButton(this.icon, {this.size = 25, this.onPressed});

  @override
  _PRIconButtonState createState() => _PRIconButtonState();
}

class _PRIconButtonState extends State<PRIconButton> {
  bool _pressed;

  @override
  void initState() {
    super.initState();
    _pressed = false;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return GestureDetector(
      child: Icon(widget.icon,
          size: widget.size,
          color: _pressed ? theme.primaryColor : theme.primaryColorDark),
      onTapCancel: () => setState(() => _pressed = false),
      onTapDown: (details) => setState(() => _pressed = true),
      onTapUp: (details) => setState(() => _pressed = false),
      onTap: widget.onPressed,
    );
  }
}

class PRBackIconButton extends StatelessWidget {
  final double size;
  final Function() onTap;

  PRBackIconButton({this.size = 25, this.onTap});

  @override
  Widget build(BuildContext context) {
    return PRIconButton(
      Icons.arrow_back_ios,
      onPressed: () {
        if (onTap != null) {
          onTap();
        }
        Navigator.pop(context);
      },
    );
  }
}
