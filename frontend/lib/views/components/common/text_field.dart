import 'package:flutter/material.dart';

class PRTextField extends StatelessWidget {
  final String initialValue;
  final String labelText;
  final String hintText;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final bool obscureText;
  final Color fillColor;
  final FloatingLabelBehavior floatingLabelBehavior;
  final bool isDense;
  final bool isCollapsed;
  final bool showBorder;
  final TextEditingController controller;
  final ValueChanged<String> onChanged;
  final VoidCallback onEditingComplete;
  final FormFieldValidator<String> validator;

  PRTextField(
      {this.labelText,
      this.initialValue,
      this.hintText,
      this.obscureText = false,
      this.prefixIcon,
      this.suffixIcon,
      this.fillColor,
      this.floatingLabelBehavior = FloatingLabelBehavior.auto,
      this.isDense = false,
      this.isCollapsed = false,
      this.showBorder = true,
      this.controller,
      this.onChanged,
      this.onEditingComplete,
      this.validator});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        initialValue: initialValue,
        obscureText: obscureText,
        onChanged: onChanged,
        controller: controller,
        validator: validator,
        onEditingComplete: onEditingComplete,
        decoration: InputDecoration(
            floatingLabelBehavior: floatingLabelBehavior,
            isDense: isDense,
            isCollapsed: isCollapsed,
            prefixIcon: prefixIcon,
            hintText: hintText,
            border: !showBorder
                ? InputBorder.none
                : OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(15)),
            labelText: labelText,
            filled: true,
            fillColor: this.fillColor ?? Theme.of(context).accentColor,
            suffixIcon: suffixIcon));
  }
}
