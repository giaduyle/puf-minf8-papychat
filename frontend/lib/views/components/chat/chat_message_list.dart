import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/views/components/chat/chat_message.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ChatMessageList extends StatelessWidget {
  final Room room;
  final List<Message> messages;
  final Function() onLoading;
  final Function() onRefresh;
  final RefreshController _refreshController = RefreshController();

  ChatMessageList(this.room, this.messages, {this.onLoading, this.onRefresh});

  @override
  Widget build(BuildContext context) {
    if (IterableUtils.isNullOrEmpty(messages)) {
      return Flexible(
        child: SizedBox.expand(),
      );
    }
    return Expanded(
      child: SmartRefresher(
        key: Key("refresher_${room.id}"),
        controller: _refreshController,
        reverse: true,
        enablePullUp: true,
        enablePullDown: true,
        physics: AlwaysScrollableScrollPhysics(),
        onLoading: () {
          _refreshController.loadComplete();
          if (onLoading != null) {
            onLoading();
          }
        },
        onRefresh: () {
          _refreshController.refreshCompleted();
          if (onRefresh != null) {
            onRefresh();
          }
        },
        child: ListView.builder(
            key: Key("list_${room.id}"),
            reverse: true,
            itemCount: messages.length,
            itemBuilder: (context, index) {
              var currentMsg = messages[index];
              return Padding(
                key: Key("${currentMsg.id}_$index"),
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: ChatMessage(
                  currentMsg,
                  showDetail: _showDetail(index - 1, index, messages.length),
                ),
              );
            }),
      ),
    );
  }

  bool _showDetail(int prevIndex, int currentIndex, int length) {
    if (currentIndex == 0 || currentIndex == length) {
      return true;
    }
    return messages[prevIndex].senderId != messages[currentIndex].senderId;
  }
}
