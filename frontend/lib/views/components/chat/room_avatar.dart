import 'dart:math';

import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/views/components/user/user_avatar.dart';

class PRRoomAvatar extends StatelessWidget {
  final Room room;
  final double radius;
  final int displayAmount;

  PRRoomAvatar(this.room, {this.radius = 40, this.displayAmount = 3})
      : assert(room != null);

  @override
  Widget build(BuildContext context) {
    if (IterableUtils.isNullOrEmpty(room.members)) {
      return CircleAvatar(
        child: Icon(Icons.group),
        backgroundColor: Colors.grey,
      );
    }
    if (room.members.length <= 2 || displayAmount == 1) {
      return PRUserAvatar(
        room.members[0],
        showProfileOnPressed: true,
      );
    }
    return CircleAvatar(
      radius: radius,
      backgroundColor: Colors.transparent,
      child: Wrap(
        alignment: WrapAlignment.center,
        direction: Axis.horizontal,
        children: room.members
            .sublist(0, min(room.members.length, displayAmount))
            .map((user) => PRUserAvatar(
                  user,
                  radius:
                      radius / displayAmount * (displayAmount <= 2 ? 0.9 : 1),
                ))
            .toList(),
      ),
    );
  }
}
