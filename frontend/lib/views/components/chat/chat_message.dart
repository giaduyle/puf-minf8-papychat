import 'package:assorted_layout_widgets/assorted_layout_widgets.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/media_controller.dart';
import 'package:papyrus_chat_app/controllers/sticker_controller.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/sticker.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/user/user_avatar.dart';

class ChatMessage extends StatefulWidget {
  static final double circularRadius = 10.0;

  final Message message;
  final bool showDetail;

  ChatMessage(this.message, {this.showDetail = false})
      : assert(message != null);

  @override
  _ChatMessageState createState() => _ChatMessageState();
}

class _ChatMessageState extends State<ChatMessage> {
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment:
          _isMe() ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        ColumnSuper(
          alignment: _isMe() ? Alignment.bottomRight : Alignment.bottomLeft,
          innerDistance: -10,
          separatorOnTop: false,
          children: [_messageContent(context), _avatar(context)],
        )
      ],
    );
  }

  Widget _messageContent(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      margin: EdgeInsets.symmetric(horizontal: 5),
      constraints: BoxConstraints(
        maxWidth: MediaQuery.of(context).size.width * 0.65,
        minHeight: 30,
      ),
      decoration: BoxDecoration(
          color: _bubbleColor(context),
          borderRadius: _isMe()
              ? BorderRadius.only(
                  topRight: Radius.circular(ChatMessage.circularRadius),
                  topLeft: Radius.circular(ChatMessage.circularRadius),
                  bottomLeft: Radius.circular(ChatMessage.circularRadius),
                )
              : BorderRadius.only(
                  topRight: Radius.circular(ChatMessage.circularRadius),
                  topLeft: Radius.circular(ChatMessage.circularRadius),
                  bottomRight: Radius.circular(ChatMessage.circularRadius),
                )),
      child: Column(
        crossAxisAlignment:
            _isMe() ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          _contentSection(context),
          _timeSection(context),
        ],
      ),
    );
  }

  Widget _avatar(BuildContext context) {
    if (_isMe()) {
      return SizedBox.shrink();
    }
    return PRUserAvatar(_getSender(),
        radius: 15,
        backgroundColor:
            _isMe() ? Theme.of(context).primaryColorLight : kGreyColor);
  }

  Widget _contentSection(BuildContext context) {
    if (widget.message.isMedia) {
      if (widget.message.media == null) {
        _loadMedia();
      } else {
        return widget.message.media.toImageMemory(width: 120, height: 120);
      }
    }
    if (_loading) {
      return CircularProgressIndicator();
    }
    if (widget.message.isSticker) {
      Sticker msgStick = widget.message.sticker;
      var media = inject<StickerController>()
          .getSticker(msgStick.collectionId, msgStick.id)
          .media;
      return media.toImageAsset(width: 100, height: 100);
    }
    return Text(
      widget.message.content,
      style: GoogleFonts.openSans(color: Colors.black87),
    );
  }

  Widget _timeSection(BuildContext context) {
    if (!widget.showDetail) {
      return SizedBox.shrink();
    }
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Text(
        _formatTime(widget.message.createdAt, Localizations.localeOf(context)),
        style: GoogleFonts.roboto(color: Colors.black45),
      ),
    );
  }

  Color _bubbleColor(BuildContext context) {
    if (widget.message.isSticker || widget.message.isMedia) {
      return Colors.transparent;
    }
    return _isMe() ? Theme.of(context).primaryColorLight : kGreyColor;
  }

  String _formatTime(DateTime time, Locale locale) {
    String date = "today".tr().toLowerCase();
    if (DateTime.now().difference(time).inDays >= 1) {
      date = "${DateFormat.yMd(locale.toString()).format(time)}";
    }
    return "$date - ${DateFormat.Hm(locale.toString()).format(time)}";
  }

  User _getSender() {
    UserController controller = inject();
    if (widget.message.senderId == null) {
      return null;
    }
    if (_isMe()) {
      return controller.whoAmI();
    }
    return controller.getFriend(widget.message.senderId);
  }

  bool _isMe() {
    return inject<UserController>().isMe(User(id: widget.message.senderId));
  }

  Future<void> _loadMedia() async {
    setState(() {
      _loading = true;
    });
    try {
      var mediaId = widget.message.getMediaId;
      widget.message.media = await inject<MediaController>().get(mediaId);
    } catch (error) {
      print(error);
    }
    setState(() {
      _loading = false;
    });
  }
}
