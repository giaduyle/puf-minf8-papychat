import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/views/components/chat/room_summary.dart';
import 'package:papyrus_chat_app/views/components/common/divider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class RoomList extends StatelessWidget {
  final List<Room> _rooms;
  final Function(Room room) onRoomTapped;
  final Function(Room room) onRoomDelete;
  final Function(Room room) onRoomLeave;
  final Function() onRefresh;
  final bool enableActions;

  final RefreshController _refreshController = RefreshController();

  RoomList(this._rooms,
      {this.onRoomTapped,
      this.onRoomLeave,
      this.onRoomDelete,
      this.onRefresh,
      this.enableActions = false});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Expanded(
        child: Material(
            color: Colors.transparent,
            child: SmartRefresher(
              enablePullDown: onRefresh != null,
              controller: _refreshController,
              onRefresh: () {
                try {
                  onRefresh();
                  _refreshController.refreshCompleted();
                } catch (error) {
                  print(error);
                  _refreshController.loadFailed();
                }
              },
              child: ListView.separated(
                  itemCount: _rooms.length,
                  itemBuilder: (context, index) {
                    Room _room = _rooms[index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: InkWell(
                        onTap: () => onRoomTapped(_room),
                        splashColor: theme.primaryColorLight,
                        highlightColor: Colors.transparent,
                        hoverColor: theme.accentColor,
                        child: RoomSummary(
                          _room,
                          enableActions: enableActions,
                          onLeaveRoom: onRoomLeave,
                          onDeleteRequest: onRoomDelete,
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) => PRDivider()),
            )));
  }
}
