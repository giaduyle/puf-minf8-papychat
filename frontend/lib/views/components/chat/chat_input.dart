import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:papyrus_chat_app/configs/constants.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/core/models/media.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/views/components/common/file_button.dart';
import 'package:papyrus_chat_app/views/components/common/icon_button.dart';
import 'package:papyrus_chat_app/views/components/common/sticker_list.dart';
import 'package:papyrus_chat_app/views/components/common/text_field.dart';

class ChatInput extends StatefulWidget {
  final String text;
  final double iconSize;
  final bool showStickers;
  final Function(Message message) onMessage;

  ChatInput(
      {this.text = "Aa",
      this.iconSize = 20,
      this.showStickers = false,
      this.onMessage});

  @override
  _ChatInputState createState() => _ChatInputState();
}

class _ChatInputState extends State<ChatInput> {
  final TextEditingController editingController = TextEditingController();

  bool _showStickers;
  String _message;

  @override
  void initState() {
    super.initState();
    _showStickers = widget.showStickers;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Flexible(
              child: PRTextField(
                labelText: widget.text,
                isDense: true,
                controller: editingController,
                prefixIcon: PRFileButton(
                    size: widget.iconSize,
                    onFilePicked: (tuple) {
                      if (widget.onMessage == null) {
                        return;
                      }
                      var media =
                          Media(name: tuple.item1, content: tuple.item2);
                      widget.onMessage(
                          Message(createdAt: DateTime.now(), media: media));
                    }),
                suffixIcon: PRIconButton(
                  Icons.face,
                  size: widget.iconSize,
                  onPressed: () {
                    setState(() {
                      _showStickers = !_showStickers;
                    });
                  },
                ),
                floatingLabelBehavior: FloatingLabelBehavior.never,
                fillColor: kGreyColor.withOpacity(0.7),
                onChanged: (value) => _message = value,
              ),
            ),
            SizedBox(width: 15),
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: PRIconButton(
                FontAwesomeIcons.solidPaperPlane,
                size: widget.iconSize,
                onPressed: () {
                  if (StringUtils.isNotNullOrEmpty(_message)) {
                    if (widget.onMessage != null) {
                      widget.onMessage(Message(
                          createdAt: DateTime.now(), content: _message));
                    }
                    editingController.clear();
                  }
                },
              ),
            )
          ],
        ),
        _stickerBox(context),
      ],
    );
  }

  Widget _stickerBox(BuildContext context) {
    if (!_showStickers) {
      return SizedBox.shrink();
    }
    return StickerList(onStickerPressed: (sticker) {
      if (widget.onMessage != null) {
        widget.onMessage(Message(
            content: "$kStickerPrefix${sticker.id}",
            createdAt: DateTime.now()));
      }
      setState(() {
        _showStickers = false;
      });
    });
  }
}
