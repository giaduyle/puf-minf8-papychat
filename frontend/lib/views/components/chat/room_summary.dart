import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/chat/room_avatar.dart';

class RoomSummary extends StatelessWidget {
  final Function(Room room) onLeaveRoom;
  final Function(Room room) onDeleteRequest;
  final bool enableActions;
  final Room _room;

  RoomSummary(this._room,
      {this.onLeaveRoom, this.onDeleteRequest, this.enableActions = false});

  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: SlidableScrollActionPane(),
      actionExtentRatio: 0.12,
      child: ListTile(
        leading: PRRoomAvatar(_room),
        title: Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Text(
              _room.title,
              style: GoogleFonts.notoSans(),
            )),
        subtitle: Text(_getLastMessageContent(_room.lastMessage)),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
                _formatLastMessageTime(
                    _room.lastMessage, Localizations.localeOf(context)),
                style: GoogleFonts.openSans(fontSize: 11)),
          ],
        ),
      ),
      secondaryActions: !enableActions
          ? null
          : [
              IconSlideAction(
                icon: Icons.exit_to_app,
                foregroundColor: Theme.of(context).primaryColor,
                onTap: () {
                  if (onLeaveRoom != null) {
                    onLeaveRoom(_room);
                  }
                },
              ),
              IconSlideAction(
                icon: Icons.delete_sweep,
                foregroundColor: Colors.red,
                onTap: () {
                  if (onDeleteRequest != null) {
                    onDeleteRequest(_room);
                  }
                },
              )
            ],
    );
  }

  String _getLastMessageContent(Message message) {
    if (message == null) {
      return "";
    }
    if (message.isSticker) {
      return tr("sticker");
    }
    if (message.isMedia) {
      return tr("media");
    }
    return PRStringUtils.truncateWithEllipsis(40, message.content);
  }

  String _formatLastMessageTime(Message message, Locale locale) {
    if (message == null) {
      return "";
    }
    return DurationUtils.formatDurationTillNow(
        message.createdAt, DateFormat.MMMEd(locale.toString()));
  }
}
