import 'package:avatar_glow/avatar_glow.dart';
import 'package:basic_utils/basic_utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/mocks.dart';
import 'package:papyrus_chat_app/views/screens/profile_screen.dart';

class PRUserAvatar extends StatelessWidget {
  final User user;
  final double radius;
  final double borderRadius;
  final Color backgroundColor;
  final Color borderColor;
  final bool showGlow;
  final bool showProfileOnPressed;

  PRUserAvatar(this.user,
      {this.radius = 20,
      this.borderRadius = 22,
      this.backgroundColor = Colors.grey,
      this.borderColor = Colors.transparent,
      this.showGlow = false,
      this.showProfileOnPressed = false});

  @override
  Widget build(BuildContext context) {
    if (!showProfileOnPressed) {
      return _buildAvatar();
    }
    return InkWell(
      customBorder: CircleBorder(),
      highlightColor: Colors.transparent,
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => ProfileScreen(user)));
      },
      child: _buildAvatar(),
    );
  }

  Widget _buildAvatar() {
    return Container(
      child: showGlow ? _glowAvatar() : _avatar(),
    );
  }

  Widget _glowAvatar() {
    return AvatarGlow(
      glowColor: backgroundColor,
      curve: Curves.fastLinearToSlowEaseIn,
      endRadius: radius * 1.5,
      repeat: true,
      showTwoGlows: true,
      child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: _avatar()),
    );
  }

  Widget _avatar() {
    String avatarUrl = user?.avatarUrl;
    if (StringUtils.isNullOrEmpty(avatarUrl)) {
      avatarUrl = randomAvatar(gender: user?.gender);
    }
    return CircleAvatar(
      radius: radius * 1.1,
      backgroundColor: borderColor,
      child: ClipOval(
        child: StringUtils.isNullOrEmpty(user?.avatarUrl)
            ? Image(
                image: AssetImage(avatarUrl),
              )
            : CachedNetworkImage(
                imageUrl: avatarUrl,
                width: 512,
                height: 512,
              ),
      ),
    );
  }
}
