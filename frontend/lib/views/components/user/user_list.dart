import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/views/components/user/user_summary.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class UserList extends StatelessWidget {
  final List<User> _users;
  final Function(User user) onUserInvoked;
  final Widget Function(BuildContext context, User contact) buildTrailIcon;
  final Function() onRefresh;
  final bool enableActions;

  final RefreshController _refreshController = RefreshController();

  UserList(this._users,
      {this.buildTrailIcon,
      this.onUserInvoked,
      this.onRefresh,
      this.enableActions = false});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: SmartRefresher(
      enablePullDown: onRefresh != null,
      controller: _refreshController,
      onRefresh: () {
        try {
          onRefresh();
          _refreshController.refreshCompleted();
        } catch (error) {
          _refreshController.loadFailed();
          logError(error);
        }
      },
      child: ListView.builder(
          itemBuilder: (context, index) {
            User _user = _users[index];
            return UserSummary(
              _user,
              enableActions: enableActions,
              trailIcon: _trailIcon(context, _user),
              onInvoked: onUserInvoked,
            );
          },
          itemCount: _users.length),
    ));
  }

  Widget _trailIcon(BuildContext context, User _user) {
    if (buildTrailIcon == null) {
      return null;
    }
    return buildTrailIcon(context, _user);
  }
}
