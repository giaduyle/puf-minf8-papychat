import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/user/user_avatar.dart';
import 'package:papyrus_chat_app/views/screens/profile_screen.dart';

enum UserAction { Selected, Blocked, Deleted }

class UserSummary extends StatelessWidget {
  final User _user;
  final Widget trailIcon;
  final bool enableActions;
  final Function(User user) onInvoked;

  final UserController controller = inject();

  UserSummary(this._user,
      {this.trailIcon, this.onInvoked, this.enableActions = false});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Slidable(
      actionPane: SlidableScrollActionPane(),
      actionExtentRatio: 0.12,
      child: Material(
        child: InkWell(
          splashColor: theme.primaryColorLight,
          highlightColor: Colors.transparent,
          hoverColor: theme.accentColor,
          onTap: () => _callOnPressed(context, _user, UserAction.Selected),
          child: ListTile(
            leading: PRUserAvatar(
              _user,
              showProfileOnPressed: true,
            ),
            title: Text(_user.fullName ?? ""),
            subtitle: Text(_user.phone ?? ""),
            trailing: trailIcon,
          ),
        ),
      ),
      actions: !enableActions
          ? null
          : [
              IconSlideAction(
                icon: Icons.block,
                foregroundColor: Colors.orange,
                onTap: () => _callOnPressed(context, _user, UserAction.Blocked),
              ),
              IconSlideAction(
                icon: Icons.delete_sweep,
                foregroundColor: Colors.red,
                onTap: () => _callOnPressed(context, _user, UserAction.Deleted),
              )
            ],
    );
  }

  void _callOnPressed(context, user, action) async {
    if (action == UserAction.Blocked) {
      await controller.block(user);
      return;
    }
    if (action == UserAction.Deleted) {
      await controller.unFriend(user);
      return;
    }
    if (onInvoked == null) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => ProfileScreen(_user)));
      return;
    }
    onInvoked(_user);
  }
}
