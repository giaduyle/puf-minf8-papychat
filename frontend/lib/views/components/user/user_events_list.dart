import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/models/event.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/divider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class UserEventList extends StatelessWidget {
  final User user;

  UserEventList(this.user) : assert(user != null);

  final UserController _controller = inject();
  final RefreshController _refreshController = RefreshController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GetBuilder<UserController>(
          initState: (state) async {
            await _controller.loadEvents(user);
            if (_controller.events.isEmpty) {
              await _controller.fetchIncomingEvents(user);
            }
          },
          init: _controller,
          autoRemove: false,
          builder: (controller) {
            if (controller.events.isEmpty) {
              return SizedBox.shrink();
            }
            return SmartRefresher(
              enablePullUp: true,
              enablePullDown: false,
              onLoading: () async {
                try {
                  await _controller.fetchIncomingEvents(user);
                  _refreshController.loadComplete();
                } catch (error) {
                  print(error);
                  _refreshController.loadFailed();
                }
              },
              controller: _refreshController,
              child: ListView.separated(
                itemCount: _controller.events.length,
                separatorBuilder: (context, index) => PRDivider(),
                itemBuilder: (context, index) {
                  var event = _controller.events[index];
                  return ListTile(
                    tileColor: Theme.of(context).primaryColor,
                    leading: Icon(Icons.person_add),
                    title: Text(
                        event.sender?.nickName ?? event.sender?.fullName ?? ""),
                    subtitle: Text(_getEventTypeText(event.type)),
                    trailing: Text(DurationUtils.formatDurationTillNow(
                        event.createdAt,
                        DateFormat.yMMMMd(
                            Localizations.localeOf(context).toString()))),
                    onTap: () async {
                      var confirmed = await showConfirmDialog(
                          context: context,
                          title: tr("friend_request"),
                          message: tr("confirm_accept"));
                      if (confirmed == null) {
                        return;
                      }
                      event.status =
                          confirmed ? EventStatus.Accepted : EventStatus.Denied;
                      await _controller.updateEvent(user, event);
                    },
                  );
                },
              ),
            );
          }),
    );
  }

  String _getEventTypeText(EventType type) {
    if (EventType.InviteFriend == type) {
      return tr("invite_friend");
    }
    return "";
  }
}
