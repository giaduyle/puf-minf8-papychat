import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/configs/theme.dart';

class OnBoardIndicator extends StatelessWidget {
  final bool isActive;

  OnBoardIndicator(this.isActive);

  static List<Widget> forPages(int pages, int currentPage) {
    List<Widget> list = [];
    for (int i = 0; i < pages; i++) {
      list.add(
          i == currentPage ? OnBoardIndicator(true) : OnBoardIndicator(false));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: 8.0,
      width: isActive ? 24.0 : 16.0,
      decoration: BoxDecoration(
        color: isActive ? theme.primaryColorDark : theme.accentColor,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
}
