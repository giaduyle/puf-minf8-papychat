import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/configs/size.dart';
import 'package:easy_localization/easy_localization.dart';

class OnBoardBody extends StatelessWidget {
  final String assetPath;
  final String title;
  final String description;

  OnBoardBody(this.assetPath, this.title, this.description);

  static List<OnBoardBody> list() {
    return [
      OnBoardBody(
          'assets/images/undraw_group_chat.png',
          'onboard_title_1'.tr(),
          'onboard_subtitle_1'.tr()),
      OnBoardBody(
          'assets/images/undraw_chatting.png',
          'onboard_title_2'.tr(),
          'onboard_subtitle_2'.tr()),
      OnBoardBody(
          'assets/images/undraw_texting.png',
          'onboard_title_3'.tr(),
          'onboard_subtitle_3'.tr()),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Center(
              child: Image(
                image: AssetImage(
                  assetPath,
                ),
                height: getProportionateScreenHeight(300.0),
                width: getProportionateScreenWidth(400.0),
              ),
            ),
          ),
          Expanded(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    title,
                    style: TextStyle(
                        color: kPrimaryTextColor,
                        fontSize: 26.0,
                        height: 1.5,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 20),
                  Text(
                    description,
                    style: TextStyle(
                      color: kPrimaryTextColor,
                      fontSize: 18.0,
                      height: 1.2,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
