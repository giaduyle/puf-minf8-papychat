import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/room_controller.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/chat/room_list.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/search_field.dart';
import 'package:papyrus_chat_app/views/screens/room_screen.dart';

class MyRoomListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [_title(context), _searchRow(context), _roomList(context)],
      ),
    ));
  }

  Widget _title(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Text(
            'chats',
            style: kTitleTextStyle,
          ).tr(),
        ),
      ),
    );
  }

  Widget _searchRow(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: PRSearchField(
              hintText: 'search_conversations',
              onChanged: (value) {
                inject<RoomController>().searchMyRooms(searchText: value);
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _roomList(BuildContext context) {
    return GetBuilder<RoomController>(
      initState: (state) async {
        await inject<RoomController>().loadMyRooms();
      },
      autoRemove: false,
      builder: (controller) {
        return RoomList(
          controller.myRooms,
          enableActions: true,
          onRoomTapped: (room) => Navigator.push(context,
              MaterialPageRoute(builder: (context) => RoomScreen(room))),
          onRefresh: () async {
            await controller.fetchMyRooms();
            await controller.loadMyRooms();
          },
          onRoomDelete: (room) async {
            bool confirmed = await showConfirmDialog(
                context: context,
                title: tr("delete_room"),
                message: tr("confirm_sure"));
            if (confirmed ?? false) {
              controller.removeRoom(room);
            }
          },
          onRoomLeave: (room) async {
            bool confirmed = await showConfirmDialog(
                context: context,
                title: tr("leave_room"),
                message: tr("confirm_sure"));
            if (confirmed ?? false) {
              controller.removeMember(room, inject<UserController>().whoAmI());
            }
          },
        );
      },
    );
  }
}
