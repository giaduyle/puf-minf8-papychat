import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:papyrus_chat_app/configs/constants.dart';
import 'package:papyrus_chat_app/configs/routes.dart';
import 'package:papyrus_chat_app/configs/size.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/flat_button.dart';
import 'package:papyrus_chat_app/views/components/common/icon_button.dart';
import 'package:papyrus_chat_app/views/components/common/text_field.dart';

class RegisterDetailScreen extends StatefulWidget {
  final User user;

  RegisterDetailScreen(this.user) : assert(user != null);

  @override
  _RegisterDetailScreenState createState() => _RegisterDetailScreenState();
}

class _RegisterDetailScreenState extends State<RegisterDetailScreen> {
  final TextEditingController _firstName = new TextEditingController();
  final TextEditingController _lastName = new TextEditingController();
  final TextEditingController _phone = new TextEditingController();
  final TextEditingController _address = new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool _processing = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: PRBackIconButton(),
        titleSpacing: 0,
        title: _title(context),
      ),
      resizeToAvoidBottomInset: false,
      body: ModalProgressHUD(
        inAsyncCall: _processing,
        child: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark,
          child: SafeArea(
            child: Container(
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _art(context),
                      _fields(context),
                      _buttons(context)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _title(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Text(
            'register',
            style: kTitleTextStyle,
          ).tr(),
        ),
      ),
    );
  }

  Widget _art(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30.0),
      child: Center(
        child: Image(
          image: AssetImage(
            'assets/images/undraw_social_girl.png',
          ),
          width: kFieldAndButtonWidth,
          height: getProportionateScreenHeight(250.0),
        ),
      ),
    );
  }

  Widget _fields(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      width: kFieldAndButtonWidth,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            PRTextField(
              controller: _firstName,
              labelText: 'first_name'.tr(),
              validator: RequiredValidator(errorText: 'required'.tr()),
            ),
            SizedBox(height: 15),
            PRTextField(
              controller: _lastName,
              labelText: 'last_name'.tr(),
              validator: RequiredValidator(errorText: 'required'.tr()),
            ),
            SizedBox(height: 15),
            PRTextField(
              controller: _phone,
              labelText: 'phone_number'.tr(),
              validator: MultiValidator([
                RequiredValidator(errorText: 'required'.tr()),
                PatternValidator(kValidPhonePattern,
                    errorText: 'phone_invalid'.tr())
              ]),
            ),
            SizedBox(height: 15),
            PRTextField(
              controller: _address,
              labelText: 'address'.tr(),
              validator: RequiredValidator(errorText: 'required'.tr()),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buttons(BuildContext context) {
    const double _kButtonHeight = 50.0;
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: PRFlatButton(
                  text: "register".tr(),
                  height: _kButtonHeight,
                  width: kFieldAndButtonWidth,
                  textColor: Colors.white,
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      var user = widget.user;
                      user.firstName = _firstName.value.text;
                      user.lastName = _lastName.value.text;
                      user.phone = _phone.value.text;
                      user.address = _address.value.text;
                      setState(() {
                        _processing = true;
                      });
                      inject<UserController>().updateMe(user).then((value) {
                        setState(() {
                          _processing = false;
                        });
                        Navigator.pushReplacementNamed(context, MainRoute);
                      }).catchError((error) {
                        showError(context: context, error: error);
                        setState(() {
                          _processing = false;
                        });
                      });
                    }
                  })),
        ],
      ),
    );
  }
}
