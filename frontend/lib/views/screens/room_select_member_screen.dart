import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/checkbox.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/icon_button.dart';
import 'package:papyrus_chat_app/views/components/common/search_field.dart';
import 'package:papyrus_chat_app/views/components/common/text_button.dart';
import 'package:papyrus_chat_app/views/components/user/user_list.dart';

class RoomSelectMemberScreen extends StatelessWidget {
  final Function(List<User> users) onUsersSelected;
  final Map<String, User> _selectedUsers = Map();
  final List<User> currentRoomMembers;

  RoomSelectMemberScreen({this.currentRoomMembers, this.onUsersSelected});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: PRBackIconButton(),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          tr("choose_people"),
          style: kTitleTextStyle,
        ),
        actions: [
          Container(
            margin: EdgeInsets.all(20),
            alignment: Alignment.center,
            child: PRTextButton(
                text: tr('done'),
                onPressed: () {
                  if (_selectedUsers.isEmpty) {
                    showInformation(
                        context: context,
                        title: tr("required"),
                        message: tr("no_members_selected"));
                    return;
                  }
                  if (onUsersSelected != null) {
                    onUsersSelected(List.of(_selectedUsers.values));
                  }
                  Navigator.pop(context);
                }),
          )
        ],
      ),
      body: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              PRSearchField(),
              GetBuilder<UserController>(
                initState: (state) async {
                  await inject<UserController>().loadFriends();
                },
                autoRemove: false,
                builder: (controller) {
                  var notJoined = controller.friends;
                  var invitedIds = currentRoomMembers == null
                      ? Set()
                      : currentRoomMembers.map((e) => e.id).toSet();
                  notJoined.removeWhere((user) =>
                      currentRoomMembers != null && invitedIds.contains(user.id));
                  return UserList(
                    notJoined,
                    buildTrailIcon: (context, user) => PRCheckBox(
                      value: false,
                      onValueChanged: (value) {
                        if (value) {
                          _selectedUsers[user.id] = user;
                        } else {
                          _selectedUsers.remove(user.id);
                        }
                      },
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
