import 'package:assorted_layout_widgets/assorted_layout_widgets.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:papyrus_chat_app/configs/size.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/icon_button.dart';
import 'package:papyrus_chat_app/views/components/common/info_field.dart';
import 'package:papyrus_chat_app/views/components/user/user_avatar.dart';

class ProfileScreen extends StatefulWidget {
  final User user;

  ProfileScreen(this.user) : assert(user != null);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  UserController _controller = inject();
  bool _canEdit;

  @override
  void initState() {
    super.initState();
    _canEdit = _controller.isMe(widget.user);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          elevation: 0,
          title: Text(
            "profile".tr(),
            style: kTitleTextStyle.copyWith(color: Colors.black),
          )),
      body: Column(
        children: [
          _userDetail(context),
        ],
      ),
    );
  }

  Widget _userDetail(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return ColumnSuper(
      innerDistance: -100,
      children: [
        Container(
          height: getProportionateScreenHeight(100),
          color: theme.primaryColor,
        ),
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              PRUserAvatar(
                widget.user,
                radius: 70,
                backgroundColor: theme.primaryColor,
                borderColor: Colors.white,
                showGlow: true,
              ),
              _fullName(context),
              _details(context)
            ],
          ),
        ),
      ],
    );
  }

  Widget _fullName(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(_getGenderIcon()),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            constraints:
                BoxConstraints(maxWidth: getProportionateScreenWidth(300)),
            child: Text(
              widget.user.fullName,
              style: TextStyle(
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          !_canEdit
              ? SizedBox.shrink()
              : PRIconButton(
                  Icons.edit,
                  onPressed: () async {
                    String first = 'first_name'.tr();
                    String last = 'last_name'.tr();
                    Map<String, String> newValues = await showEditDialog(
                        context: context,
                        values: {
                          first: widget.user.firstName,
                          last: widget.user.lastName
                        });
                    if (newValues != null) {
                      setState(() {
                        widget.user.firstName = newValues[first];
                        widget.user.lastName = newValues[last];
                        _controller.updateMe(widget.user);
                      });
                    }
                  },
                )
        ],
      ),
    );
  }

  IconData _getGenderIcon() {
    if (widget.user.gender == UserGender.Male) {
      return FontAwesomeIcons.mars;
    }
    if (widget.user.gender == UserGender.Female) {
      return FontAwesomeIcons.venus;
    }
    return FontAwesomeIcons.venusMars;
  }

  Widget _details(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 80),
      child: Wrap(
        runSpacing: 30,
        children: [
          PRInformationField(
            "nickname".tr(),
            widget.user.nickName ?? "",
            icon: Icons.account_circle,
            onPressed: !_canEdit
                ? null
                : (key, value) async {
                    Map<String, String> newValues = await showEditDialog(
                        context: context, values: {key: value});
                    if (newValues != null) {
                      setState(() {
                        widget.user.nickName = newValues[key];
                        _controller.updateMe(widget.user);
                      });
                    }
                  },
          ),
          PRInformationField(
            "email".tr(),
            widget.user.email ?? "",
            icon: Icons.email,
            onPressed: !_canEdit
                ? null
                : (key, value) async {
                    Map<String, String> newValues = await showEditDialog(
                        context: context, values: {key: value});
                    if (newValues != null) {
                      setState(() {
                        widget.user.email = newValues[key];
                        _controller.updateMe(widget.user);
                      });
                    }
                  },
          ),
          PRInformationField(
            "phone".tr(),
            widget.user.phone ?? "",
            icon: Icons.call,
            onPressed: !_canEdit
                ? null
                : (key, value) async {
                    Map<String, String> newValues = await showEditDialog(
                        context: context, values: {key: value});
                    if (newValues != null) {
                      setState(() {
                        widget.user.phone = newValues[key];
                        _controller.updateMe(widget.user);
                      });
                    }
                  },
          ),
          PRInformationField(
            "address".tr(),
            widget.user.address ?? "",
            icon: Icons.home,
            onPressed: !_canEdit
                ? null
                : (key, value) async {
                    Map<String, String> newValues = await showEditDialog(
                        context: context, values: {key: value});
                    if (newValues != null) {
                      setState(() {
                        widget.user.address = newValues[key];
                        _controller.updateMe(widget.user);
                      });
                    }
                  },
          ),
        ],
      ),
    );
  }
}
