import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/auth_controller.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/divider.dart';
import 'package:papyrus_chat_app/views/components/common/setting_row.dart';
import 'package:papyrus_chat_app/views/components/settings/blocked_user.dart';

class SettingScreen extends StatefulWidget {
  SettingScreen();

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

Map<String, String> languages = {
  "en": "english",
  "fr": "french",
  "vi": "vietnamese"
};

class _SettingScreenState extends State<SettingScreen> {
  UserController _userController = inject();
  AuthController _authController = inject();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          color: Colors.transparent,
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Column(
              children: <Widget>[
                _headerText(context),
                _deleteAccount(context),
                _divider(context),
                _notifications(context),
                _divider(context),
                _language(context),
                _divider(context),
                _blockedUsers(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _headerText(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 30),
          child: Text(
            tr('settings'),
            style: kTitleTextStyle,
          ),
        ),
      ),
    );
  }

  Widget _deleteAccount(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 15, bottom: 20),
      child: InkWell(
        child: Row(children: [
          Icon(
            Icons.delete,
            color: Colors.red,
          ),
          new Padding(
              padding: const EdgeInsets.only(left: 30.0),
              child: Text(
                tr("delete_account"),
                style: new TextStyle(
                  color: Colors.black,
                  fontSize: 15.0,
                ),
              )),
        ]),
        onTap: _showDeleteAccountDialog,
      ),
    );
  }

  void _showDeleteAccountDialog() {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: new Text(tr("delete_account")),
              content: new Text(tr("do_you_want_to_delete_this_account_?")),
              actions: <Widget>[
                FlatButton(
                  child: Text(tr("yes")),
                  onPressed: () async {
                    await _userController.deleteMe(_userController.whoAmI());
                    await _authController.logout();
                  },
                ),
                FlatButton(
                  child: Text(tr("cancel")),
                  onPressed: () {
                    print("Cancel");
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }

  Widget _notifications(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Container(
        child: Theme(
      data: theme.copyWith(
          unselectedWidgetColor: theme.primaryColorDark,
          accentColor: theme.primaryColor,
          dividerColor: Colors.transparent),
      child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: ExpansionTile(
            leading: Icon(Icons.notifications, color: Colors.green),
            title: Text(tr('notifications')),
            children: <Widget>[
              PRSettingRow(
                  tr("show_preview"), SettingNotification.SHOW_PREVIEW),
              PRSettingRow(tr("show_chat_notification"),
                  SettingNotification.SHOW_CHAT_NOTI),
              PRSettingRow(tr("show_group_notification"),
                  SettingNotification.SHOW_GROUP_NOTI),
            ],
          )),
    ));
  }

  Widget _language(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(20),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(Icons.language, color: Theme.of(context).primaryColor),
            Expanded(
                child: new Padding(
              padding: const EdgeInsets.only(left: 30.0),
              child: DropdownButton<String>(
                isExpanded: true,
                isDense: true,
                items: languages.entries.map((language) {
                  return DropdownMenuItem(
                    value: language.key,
                    child: Text(tr(language.value)),
                  );
                }).toList(),
                underline: SizedBox(),
                value: context.locale.toString(),
                onChanged: (String value) {
                  setState(() {
                    context.locale = Locale(value);
                  });
                },
              ),
            )),
          ],
        ));
  }

  Widget _blockedUsers(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Expanded(
      child: Column(
        children: [
          Expanded(
            child: Theme(
              data: theme.copyWith(
                  unselectedWidgetColor: theme.primaryColorDark,
                  accentColor: theme.primaryColor,
                  dividerColor: Colors.transparent),
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                child: GetBuilder(
                  init: _userController,
                  autoRemove: false,
                  builder: (controller) => ExpansionTile(
                    leading: Icon(Icons.block, color: Colors.red),
                    title: Text(tr("blocked")),
                    expandedAlignment: Alignment.center,
                    children: _userController.blocks
                        .map((user) => Column(
                              children: [
                                _divider(context),
                                BlockedUser(
                                  user,
                                  onUnblocked: (user) async {
                                    await controller.unblock(user);
                                  },
                                )
                              ],
                            ))
                        .toList(),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _divider(BuildContext context) {
    return PRDivider(
      indents: 20,
      color: Theme.of(context).primaryColor,
    );
  }
}
