import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/room_controller.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/divider.dart';
import 'package:papyrus_chat_app/views/components/common/flat_button.dart';
import 'package:papyrus_chat_app/views/components/common/icon_button.dart';
import 'package:papyrus_chat_app/views/components/common/info_field.dart';
import 'package:papyrus_chat_app/views/components/common/status_radio.dart';
import 'package:papyrus_chat_app/views/components/user/user_summary.dart';
import 'package:papyrus_chat_app/views/screens/room_select_member_screen.dart';

class RoomSettingScreen extends StatefulWidget {
  final Room room;

  RoomSettingScreen(this.room);

  @override
  _RoomSettingScreenState createState() => _RoomSettingScreenState();
}

class _RoomSettingScreenState extends State<RoomSettingScreen> {
  final RoomController _roomController = inject();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: PRBackIconButton(),
          title: Text(
            "room_settings".tr(),
            style: kTitleTextStyle,
          ),
        ),
        body: Container(
          margin: EdgeInsets.only(top: 30),
          color: Colors.transparent,
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Column(
              children: [
                _roomTitle(context),
                _privacySetting(context),
                _divider(context),
                _members(context)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _privacySetting(BuildContext context) {
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      runSpacing: 20,
      children: [
        PRStatusRadio(
          text: "public".tr(),
          color: Colors.green,
          value: false,
          groupValue: widget.room.isPrivate,
          onChanged: (value) {
            setState(() {
              widget.room.isPrivate = value;
              _roomController.updateRoom(widget.room);
            });
          },
        ),
        PRStatusRadio(
          text: "private".tr(),
          color: Colors.red,
          value: true,
          groupValue: widget.room.isPrivate,
          onChanged: (value) {
            setState(() {
              widget.room.isPrivate = value;
              _roomController.updateRoom(widget.room);
            });
          },
        ),
      ],
    );
  }

  Widget _roomTitle(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15.0),
      child: PRInformationField(
        "room_title".tr(),
        widget.room.title,
        onPressed: (key, value) async {
          Map<String, String> newValues =
              await showEditDialog(context: context, values: {key: value});
          if (newValues != null) {
            setState(() {
              widget.room.name = newValues[key];
              _roomController.updateRoom(widget.room);
            });
          }
        },
      ),
    );
  }

  Widget _members(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Expanded(
      child: Column(
        children: [
          PRFlatButton(
            text: "invite_members".tr(),
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => RoomSelectMemberScreen(
                          currentRoomMembers: widget.room.members,
                          onUsersSelected: (users) async {
                            await _roomController.addMembers(
                                widget.room, users);
                          },
                        ))),
          ),
          Expanded(
            child: Theme(
              data: theme.copyWith(
                  unselectedWidgetColor: theme.primaryColorDark,
                  accentColor: theme.primaryColor,
                  dividerColor: Colors.transparent),
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                child: ExpansionTile(
                  initiallyExpanded: true,
                  key: PageStorageKey<Room>(widget.room),
                  leading: Icon(Icons.group),
                  title: Text("members".tr()),
                  expandedAlignment: Alignment.center,
                  children: widget.room.members
                      .map((user) => Column(
                            children: [_divider(context), UserSummary(user)],
                          ))
                      .toList(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _divider(BuildContext context) {
    return PRDivider(
      indents: 20,
      color: Theme.of(context).primaryColorLight,
    );
  }
}
