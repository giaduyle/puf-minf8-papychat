import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:papyrus_chat_app/configs/routes.dart';
import 'package:papyrus_chat_app/configs/size.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/auth_controller.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/flat_button.dart';
import 'package:papyrus_chat_app/views/components/common/google_signin_button.dart';
import 'package:papyrus_chat_app/views/components/common/password_field.dart';
import 'package:papyrus_chat_app/views/components/common/text_field.dart';
import 'package:papyrus_chat_app/views/screens/register_detail_screen.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _email = new TextEditingController();
  final TextEditingController _password = new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool _processing = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: ModalProgressHUD(
        inAsyncCall: _processing,
        child: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark,
          child: SafeArea(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _title(context),
                    _art(context),
                    _fields(context),
                    _buttons(context),
                    _footer(context)
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _title(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Text(
            'register',
            style: kTitleTextStyle,
          ).tr(),
        ),
      ),
    );
  }

  Widget _art(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30.0),
      child: Center(
        child: Image(
          image: AssetImage(
            'assets/images/undraw_social_girl.png',
          ),
          width: kFieldAndButtonWidth,
          height: getProportionateScreenHeight(250.0),
        ),
      ),
    );
  }

  Widget _fields(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      width: kFieldAndButtonWidth,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            PRTextField(
              validator: EmailValidator(errorText: 'email_invalid'.tr()),
              labelText: "email_need".tr(),
              controller: _email,
            ),
            SizedBox(height: 15),
            PRPasswordField(
              controller: _password,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buttons(BuildContext context) {
    const double _kButtonHeight = 50.0;
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: PRFlatButton(
                  text: "register".tr(),
                  height: _kButtonHeight,
                  width: kFieldAndButtonWidth,
                  textColor: Colors.white,
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      await _createUser(
                          User(email: _email.text, password: _password.text));
                    }
                  })),
          Text(
            'or'.tr().toUpperCase(),
            style: TextStyle(
                color: Colors.black, fontSize: 10.0, letterSpacing: 1),
          ).tr(),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15.0),
            child: GoogleSignInButton(),
          ),
        ],
      ),
    );
  }

  Widget _footer(BuildContext context) {
    return Container(
      width: kFieldAndButtonWidth,
      child: Column(
        children: [
          Divider(
            thickness: 1,
            color: Colors.black,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("existing_account".tr(), style: TextStyle(fontSize: 15)),
              Container(
                  margin: EdgeInsets.only(left: 10),
                  child: InkWell(
                    child: Text("login".tr(),
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold)),
                    onTap: () =>
                        {Navigator.pushReplacementNamed(context, LoginRoute)},
                  ))
            ],
          )
        ],
      ),
    );
  }

  Future<void> _createUser(User user) async {
    setState(() {
      _processing = true;
    });
    User newUser = User(email: _email.text, password: _password.text);
    await inject<AuthController>().register(newUser).then((
      createdUser,
    ) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => RegisterDetailScreen(createdUser)));
      setState(() {
        _processing = false;
      });
    }, onError: (error) {
      setState(() {
        _processing = false;
      });
      showError(context: context, error: error);
    });
  }
}
