import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/auth_controller.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/mocks.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/divider.dart';
import 'package:papyrus_chat_app/views/components/user/user_avatar.dart';
import 'package:papyrus_chat_app/views/components/user/user_events_list.dart';
import 'package:papyrus_chat_app/views/screens/profile_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

List statusList = [
  Item(
      UserStatus.Offline,
      'offline'.tr(),
      Icon(
        Icons.lens,
        color: Colors.blueGrey,
      )),
  Item(
      UserStatus.Online,
      'online'.tr(),
      Icon(
        Icons.lens,
        color: Colors.lightGreen,
      )),
  Item(
      UserStatus.Away,
      'away'.tr(),
      Icon(
        Icons.lens,
        color: Colors.orangeAccent,
      )),
  Item(UserStatus.Busy, 'busy'.tr(), Icon(Icons.lens, color: Colors.redAccent)),
];

class Item {
  const Item(this.status, this.name, this.icon);

  final UserStatus status;
  final String name;
  final Icon icon;
}

class _HomeScreenState extends State<HomeScreen> {
  UserController _userController;
  User _me;
  Item _selectedStatus;

  @override
  void initState() {
    super.initState();
    _userController = inject();
    _me = _userController.whoAmI();
    _selectedStatus = statusList[_me.status?.index ?? 0];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Wrap(
              runSpacing: 5,
              children: <Widget>[
                _headerText(context),
                _userInfo(context),
                _divider(context),
                _statusDropdownButton(context),
                _divider(context),
                _profileButton(context),
                _divider(context),
                _signOutButton(context),
                _divider(context),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Flexible(child: UserEventList(_me))
          ],
        ),
      ),
    );
  }

  Widget _headerText(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Text(
            'home'.tr(),
            style: kTitleTextStyle,
          ),
        ),
      ),
    );
  }

  Widget _avatarSection(BuildContext context) {
    return PRUserAvatar(_me,
        radius: 42,
        showGlow: true,
        backgroundColor: _selectedStatus.icon.color);
  }

  Widget _userInfoSection() {
    return new Expanded(
        child: new Container(
      padding: new EdgeInsets.only(left: 10.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          new Text(
            _me.nickName ?? "",
            style: new TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w600,
              fontSize: 20.0,
            ),
          ),
          new Text(
            _me.email ?? "",
            style: new TextStyle(
              color: Colors.black,
              fontSize: 15.0,
            ),
          ),
        ],
      ),
    ));
  }

  Widget _userInfo(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 50, top: 40, left: 40, right: 20),
      child: Center(
        child: Row(children: [
          _avatarSection(context),
          _userInfoSection(),
        ]),
      ),
    );
  }

  Widget _statusDropdownButton(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        child: DropdownButton(
          isExpanded: true,
          isDense: true,
          items: statusList.map((status) {
            return DropdownMenuItem(
              value: status,
              child: Row(
                children: [
                  status.icon,
                  new Container(
                    padding: new EdgeInsets.only(left: 20.0),
                    child: Text(status.name),
                  ),
                ],
              ),
            );
          }).toList(),
          value: _selectedStatus,
          underline: SizedBox(),
          onChanged: (value) async {
            _me.status = UserStatus.values[value.status.index];
            await _userController.updateMe(_me);
            setState(() {
              _selectedStatus = value;
            });
          },
        ));
  }

  Widget _profileButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20),
      child: InkWell(
        child: Row(children: [
          Icon(
            Icons.account_circle,
            color: Theme.of(context).primaryColor,
          ),
          new Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                "profile".tr(),
                style: new TextStyle(
                  color: Colors.black,
                  fontSize: 15.0,
                ),
              )),
        ]),
        onTap: () => {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ProfileScreen(_me))),
        },
      ),
    );
  }

  Widget _signOutButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20),
      child: InkWell(
        child: Row(children: [
          Icon(
            Icons.power_settings_new,
            color: Theme.of(context).primaryColor,
          ),
          new Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                "sign_out".tr(),
                style: new TextStyle(
                  color: Colors.black,
                  fontSize: 15.0,
                ),
              )),
        ]),
        onTap: () async {
          bool confirmed = await showConfirmDialog(
              context: context,
              title: "sign_out".tr(),
              message: "sign_out_confirmation".tr());
          if (confirmed ?? false) {
            await inject<AuthController>().logout();
          }
        },
      ),
    );
  }

  Widget _divider(BuildContext context) {
    return PRDivider(
      indents: 20,
      color: Theme.of(context).primaryColor,
    );
  }
}
