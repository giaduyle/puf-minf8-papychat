import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/icon_button.dart';
import 'package:papyrus_chat_app/views/components/common/search_field.dart';
import 'package:papyrus_chat_app/views/components/user/user_list.dart';

class FriendAddScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: PRBackIconButton(),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          "add_contact",
          style: kTitleTextStyle,
        ).tr(),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Container(
          color: Colors.white,
          child: GetBuilder<UserController>(
            initState: (state) {
              inject<UserController>().searchServerStream.value = "";
            },
            autoRemove: false,
            builder: (controller) {
              return Column(
                children: [
                  PRSearchField(
                    onChanged: (value) {
                      controller.searchServerStream.value = value;
                    },
                  ),
                  UserList(controller.strangers,
                      onRefresh: () {
                        controller.searchStrangers(
                            controller.searchServerStream.value);
                      },
                      onUserInvoked: (user) async {
                        if (!controller.isStranger(user)) {
                          return;
                        }
                        bool confirmed = await showConfirmDialog(
                            context: context,
                            title: tr("friend_request"),
                            message: tr("question_send_friend_request",
                                args: [user.fullName]));
                        if (confirmed) {
                          await controller.addFriend(user);
                        }
                      },
                      buildTrailIcon: (context, user) =>
                          !controller.isStranger(user)
                              ? null
                              : PRIconButton(
                                  Icons.person_add,
                                ))
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
