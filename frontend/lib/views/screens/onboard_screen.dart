import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:papyrus_chat_app/configs/routes.dart';
import 'package:papyrus_chat_app/configs/size.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/views/components/common/flat_button.dart';
import 'package:papyrus_chat_app/views/components/onboard/onboard_body.dart';
import 'package:papyrus_chat_app/views/components/onboard/onboard_indicator.dart';

class OnBoardScreen extends StatefulWidget {
  @override
  _OnBoardScreenState createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> {
  final List<OnBoardBody> onBoardContents = OnBoardBody.list();
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: SafeArea(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 40.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  _welcomeText(context),
                  _onBoardList(),
                  SizedBox(height: 50),
                  _indicators(),
                  SizedBox(height: 40),
                  _bottomSheet(context)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  bool _isEndPage() => _currentPage == onBoardContents.length - 1;

  Widget _welcomeText(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          'welcome_title',
          style: kTitleTextStyle,
        ).tr(),
      ),
    );
  }

  Widget _onBoardList() {
    return Expanded(
      child: Container(
        child: PageView(
          physics: ClampingScrollPhysics(),
          controller: _pageController,
          onPageChanged: (int page) {
            setState(() {
              _currentPage = page;
            });
          },
          children: OnBoardBody.list(),
        ),
      ),
    );
  }

  Widget _indicators() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: OnBoardIndicator.forPages(onBoardContents.length, _currentPage),
    );
  }

  Widget _bottomSheet(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Align(
        alignment: FractionalOffset.bottomCenter,
        child: PRFlatButton(
          text: _isEndPage() ? 'get_started'.tr() : "continue".tr(),
          height: getProportionateScreenHeight(40.0),
          width: kFieldAndButtonWidth,
          onPressed: () => _isEndPage()
              ? Navigator.pushReplacementNamed(context, LoginRoute)
              : _pageController.animateToPage(_currentPage + 1,
                  duration: kAnimationDuration, curve: Curves.easeIn),
        ),
      ),
    );
  }
}
