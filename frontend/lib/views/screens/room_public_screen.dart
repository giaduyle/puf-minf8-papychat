import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/room_controller.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/chat/room_list.dart';
import 'package:papyrus_chat_app/views/components/common/icon_button.dart';
import 'package:papyrus_chat_app/views/components/common/search_field.dart';
import 'package:papyrus_chat_app/views/screens/room_screen.dart';
import 'package:papyrus_chat_app/views/screens/room_select_member_screen.dart';

class PublicRoomListScreen extends StatelessWidget {
  final RoomController _roomController = inject();
  final UserController _userController = inject();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [_title(context), _searchRow(context), _roomList(context)],
      ),
    ));
  }

  Widget _title(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Text(
            'public_chats',
            style: kTitleTextStyle,
          ).tr(),
        ),
      ),
    );
  }

  Widget _searchRow(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: PRSearchField(
              hintText: 'search_conversations',
              onChanged: (value) {
                inject<RoomController>().searchServerStream.value = value;
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 10),
            child: PRIconButton(Icons.add_comment, size: 25, onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return RoomSelectMemberScreen(onUsersSelected: (users) async {
                  Get.to(RoomScreen(await _roomController.addRoom(
                      Room(isPrivate: false), users)));
                });
              }));
            }),
          )
        ],
      ),
    );
  }

  Widget _roomList(BuildContext context) {
    return GetBuilder<RoomController>(
      init: _roomController,
      autoRemove: false,
      initState: (state) async {
        _roomController.searchServerStream.value = "";
        await _roomController.searchPublicRooms();
      },
      builder: (controller) {
        return RoomList(
          controller.publicRooms,
          onRoomTapped: (room) async {
            await _roomController.addMembers(room, [_userController.whoAmI()]);
            await _roomController.fetchMyRooms();
            Get.to(RoomScreen(room));
          },
          onRefresh: () {
            controller.searchPublicRooms(
                searchText: controller.searchServerStream.value);
          },
        );
      },
    );
  }
}
