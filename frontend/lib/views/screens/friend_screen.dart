import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/room_controller.dart';
import 'package:papyrus_chat_app/controllers/user_controller.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/checkbox.dart';
import 'package:papyrus_chat_app/views/components/common/icon_button.dart';
import 'package:papyrus_chat_app/views/components/common/search_field.dart';
import 'package:papyrus_chat_app/views/components/user/user_list.dart';
import 'package:papyrus_chat_app/views/screens/friend_add_screen.dart';
import 'package:papyrus_chat_app/views/screens/room_screen.dart';

class FriendScreen extends StatelessWidget {
  final UserController _userController = inject();
  final RoomController _roomController = inject();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _title(context),
            _searchRow(context),
            _friendList(context),
          ],
        ),
      ),
    );
  }

  Widget _title(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Text(
            'contacts',
            style: kTitleTextStyle,
          ).tr(),
        ),
      ),
    );
  }

  Widget _searchRow(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: PRSearchField(
                hintText: 'search_friends',
                onChanged: (value) {
                  inject<UserController>().searchFriends(value);
                }),
          ),
          Container(
              margin: EdgeInsets.only(left: 10),
              child: PRIconButton(Icons.group_add,
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FriendAddScreen()))))
        ],
      ),
    );
  }

  Widget _friendList(BuildContext context) {
    return GetBuilder<UserController>(
      initState: (state) async {
        await _userController.loadFriends();
        if (_userController.friends.isEmpty) {
          await _userController.fetchFriends();
        }
      },
      autoRemove: false,
      builder: (controller) {
        return UserList(
          controller.friends,
          onRefresh: () async {
            await controller.fetchFriends();
          },
          enableActions: true,
          onUserInvoked: (user) async {
            Room room = await _roomController.findRoomForUser(user);
            if (room == null) {
              room = await _roomController.addRoom(Room(), [user]);
            }
            Get.to(RoomScreen(room));
          },
          buildTrailIcon: (context, user) => PRCheckBox(
            value: user.favourite,
            onValueChanged: (value) {
              if (value) {
                controller.addFavourite(user);
              } else {
                controller.removeFavourite(user);
              }
            },
            checkedIconData: Icons.star,
            uncheckedIconData: Icons.star_border,
          ),
        );
      },
    );
  }
}
