import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:papyrus_chat_app/configs/routes.dart';
import 'package:papyrus_chat_app/configs/size.dart';
import 'package:papyrus_chat_app/configs/theme.dart';
import 'package:papyrus_chat_app/controllers/auth_controller.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/flat_button.dart';
import 'package:papyrus_chat_app/views/components/common/google_signin_button.dart';
import 'package:papyrus_chat_app/views/components/common/password_field.dart';
import 'package:papyrus_chat_app/views/components/common/text_field.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _email = TextEditingController();
  final _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool _logging = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: ModalProgressHUD(
        inAsyncCall: _logging,
        child: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark,
          child: SafeArea(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _title(context),
                    _art(context),
                    _fields(context),
                    _buttons(context),
                    _footer(context)
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _title(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Text(
            'login',
            style: kTitleTextStyle,
          ).tr(),
        ),
      ),
    );
  }

  Widget _art(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30.0),
      child: Center(
        child: Image(
          image: AssetImage(
            'assets/images/undraw_ideas.png',
          ),
          width: kFieldAndButtonWidth,
          height: getProportionateScreenHeight(250.0),
        ),
      ),
    );
  }

  Widget _fields(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      width: kFieldAndButtonWidth,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            PRTextField(
              labelText: "email_need".tr(),
              controller: _email,
              validator: MultiValidator([
                RequiredValidator(errorText: "required".tr()),
                EmailValidator(errorText: "email_invalid".tr()),
              ]),
            ),
            SizedBox(height: 15),
            PRPasswordField(
              controller: _password,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buttons(BuildContext context) {
    const double _kButtonHeight = 50.0;
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: PRFlatButton(
                text: "login".tr(),
                height: _kButtonHeight,
                width: kFieldAndButtonWidth,
                textColor: Colors.white,
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    setState(() {
                      _logging = true;
                    });
                    inject<AuthController>()
                        .login(User(
                            email: _email.value.text,
                            password: _password.value.text))
                        .catchError((error) {
                      showError(context: context, error: error);
                      setState(() {
                        _logging = false;
                      });
                    });
                  }
                },
              )),
          Text(
            'or'.tr().toUpperCase(),
            style: TextStyle(
                color: Colors.black, fontSize: 10.0, letterSpacing: 1),
          ).tr(),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15.0),
            child: GoogleSignInButton(),
          ),
        ],
      ),
    );
  }

  Widget _footer(BuildContext context) {
    return Container(
      width: kFieldAndButtonWidth,
      child: Column(
        children: [
          Divider(
            thickness: 1,
            color: Colors.black,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("no_account".tr(), style: TextStyle(fontSize: 15)),
              Container(
                  margin: EdgeInsets.only(left: 10),
                  child: InkWell(
                    child: Text("register".tr(),
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold)),
                    onTap: () => {
                      Navigator.pushReplacementNamed(context, RegisterRoute)
                    },
                  ))
            ],
          )
        ],
      ),
    );
  }
}
