import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:papyrus_chat_app/configs/size.dart';
import 'package:papyrus_chat_app/controllers/auth_controller.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/screens/friend_screen.dart';
import 'package:papyrus_chat_app/views/screens/home_screen.dart';
import 'package:papyrus_chat_app/views/screens/onboard_screen.dart';
import 'package:papyrus_chat_app/views/screens/room_mine_screen.dart';
import 'package:papyrus_chat_app/views/screens/room_public_screen.dart';
import 'package:papyrus_chat_app/views/screens/setting_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final PageController _pageController = PageController(keepPage: false);

  int _currentPage;

  @override
  void initState() {
    super.initState();
    _currentPage = 0;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.of(context);
    if (!inject<AuthController>().isAuthenticated()) {
      return OnBoardScreen();
    }
    return Scaffold(
      extendBody: true,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark, child: _pages(context)),
      bottomNavigationBar: _bottomNavigationBar(context),
    );
  }

  Widget _pages(BuildContext context) {
    return Material(
      child: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        children: [
          MyRoomListScreen(),
          FriendScreen(),
          PublicRoomListScreen(),
          HomeScreen(),
          SettingScreen()
        ],
        onPageChanged: (index) {
          setState(() {
            _currentPage = index;
          });
        },
      ),
    );
  }

  Widget _bottomNavigationBar(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(20),
        topRight: Radius.circular(20),
      ),
      child: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.chat_bubble), title: SizedBox.shrink()),
            BottomNavigationBarItem(
                icon: Icon(Icons.contacts), title: SizedBox.shrink()),
            BottomNavigationBarItem(
                icon: Icon(FontAwesomeIcons.users), title: SizedBox.shrink()),
            BottomNavigationBarItem(
                icon: Icon(Icons.dashboard), title: SizedBox.shrink()),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings), title: SizedBox.shrink()),
          ],
          backgroundColor: theme.primaryColor,
          selectedItemColor: Colors.black,
          unselectedItemColor: theme.accentColor,
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          currentIndex: _currentPage,
          onTap: (index) => _pageController.jumpToPage(index)),
    );
  }
}
