import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:papyrus_chat_app/controllers/room_controller.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:papyrus_chat_app/views/components/chat/chat_input.dart';
import 'package:papyrus_chat_app/views/components/chat/chat_message_list.dart';
import 'package:papyrus_chat_app/views/components/chat/room_avatar.dart';
import 'package:papyrus_chat_app/views/components/common/dialog.dart';
import 'package:papyrus_chat_app/views/components/common/icon_button.dart';
import 'package:papyrus_chat_app/views/screens/room_setting_screen.dart';

class RoomScreen extends StatelessWidget {
  final Room room;
  final RoomController _controller = inject();

  RoomScreen(this.room);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: PRBackIconButton(
          onTap: () {
            _controller.outCurrentRoom();
          },
        ),
        titleSpacing: 0,
        title: _title(context),
        actions: [_actions(context)],
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: GetBuilder<RoomController>(
          init: _controller,
          autoRemove: false,
          initState: (state) async {
            try {
              await _controller.joinRoom(room);
              var hasMessages = await _controller.loadMessages();
              if (!hasMessages) {
                await _controller.fetchPastMessages();
              }
            } catch (error) {
              Navigator.pop(context);
              showError(context: context, error: error);
            }
          },
          builder: (controller) {
            return Column(
              children: [
                StreamBuilder(
                    stream: controller.messageStream.stream,
                    builder: (context, snapshot) {
                      return ChatMessageList(
                        room,
                        snapshot.data,
                        onLoading: () async {
                          await _controller.fetchPastMessages();
                        },
                        onRefresh: () async {
                          await _controller.fetchNewerMessages();
                        },
                      );
                    }),
                Container(
                    margin: EdgeInsets.only(top: 10, bottom: 30),
                    child: ChatInput(
                      text: tr("input_message"),
                      iconSize: 23,
                      onMessage: (message) {
                        controller.sendMessage(message);
                      },
                    ))
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _title(BuildContext context) {
    return Row(
      children: [
        PRRoomAvatar(room),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                room.title,
                style: GoogleFonts.notoSans(fontSize: 18),
              ),
              Container(
                margin: EdgeInsets.only(top: 5),
                child: Text(
                  _formatLastSeen(
                      room.members[0], Localizations.localeOf(context)),
                  style: GoogleFonts.openSans(fontSize: 11, color: Colors.grey),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _actions(BuildContext context) {
    return Container(
      width: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          PRIconButton(Icons.more_vert,
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RoomSettingScreen(room))))
        ],
      ),
    );
  }

  String _formatLastSeen(User user, Locale locale) {
    return DurationUtils.formatDurationTillNow(
        user.lastSeenTime, DateFormat.yMMMMd(locale.toString()),
        prefix: tr("last_seen"), suffixOnDuration: tr("ago"));
  }
}
