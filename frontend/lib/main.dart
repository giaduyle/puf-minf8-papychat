import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:papyrus_chat_app/configs/configurations.dart';
import 'package:papyrus_chat_app/configs/routes.dart';
import 'package:papyrus_chat_app/configs/theme.dart';

void main() async {
  await initConfigurations();
  runApp(EasyLocalization(
    child: PapyChatApp(),
    supportedLocales: [
      const Locale("en"),
      const Locale("fr"),
      const Locale("vi"),
    ],
    path: 'assets/translations',
    fallbackLocale: const Locale('en'),
    useOnlyLangCode: true,
  ));
}

class PapyChatApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'PapyChat',
      debugShowCheckedModeBanner: false,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      theme: kThemeData,
      initialRoute: MainRoute,
      routes: PRRoutes,
    );
  }
}
