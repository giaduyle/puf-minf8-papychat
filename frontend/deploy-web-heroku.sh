#!/bin/bash

if [[ $(heroku whoami) ]]; then
    echo "Logged in Heroku"
else
    heroku login
fi

git submodule sync
git submodule update --init --remote

# Build flutter app
flutter build web --release
rm -rf ./deploy-heroku/*
cp -rf ./build/web/* ./deploy-heroku/

cd ./deploy-heroku
heroku git:remote -a papy-chat-website
mv index.html home.html
echo '<?php' > index.php
echo '  header("Access-Control-Allow-Origin:*");' >> index.php
echo '  header("Access-Control-Allow-Credentials:true");' >> index.php
echo '  header("Access-Control-Allow-Methods:GET,PUT,POST,DELETE,OPTIONS");' >> index.php
echo '  header("Access-Control-Max-Age:1000");'  >> index.php
echo '  header("Access-Control-Allow-Headers:Origin,Content-Type,X-Auth-Token,Authorization");' >> index.php
echo '  include_once("home.html");' >> index.php
echo '?>'  >> index.php
echo '{}' > composer.json

# Force redeploy same source code (unrem when needed)
# heroku plugins:install heroku-repo
# heroku repo:reset -a papy-chat-website
# heroku repo:gc -a papy-chat-website
# heroku repo:purge_cache -a papy-chat-website

git add .
git commit -m "Deploying website to Heroku new version"
git push -f heroku master
git push -f origin master
