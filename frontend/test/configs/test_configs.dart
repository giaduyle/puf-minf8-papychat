import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:mockito/mockito.dart';
import 'package:papyrus_chat_app/configs/configurations.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';

Future<void> initTestSettings() async {
  GlobalConfiguration().loadFromMap({
    "web-socket": "ws://localhost",
    "host": "http://localhost",
    "port": 8080,
    "api": "/api/v1",
    "secret": "papychat-client-secret-123456789"
  });
  Hive.init("./build/hive_test");
  await initHttpClient();
  await registerAdapters();
  await registerRepositories();
  await registerApis();
  await registerControllers();
}

class MockHttpFacade extends Mock implements HttpFacade {
  static void putGlobally() => Get.put<HttpFacade>(MockHttpFacade());
}
