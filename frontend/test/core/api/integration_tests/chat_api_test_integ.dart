import 'dart:async';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/core/api/auth_api.dart';
import 'package:papyrus_chat_app/core/api/chat_api.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';
import 'package:papyrus_chat_app/core/api/room_api.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/token_repository.dart';
import 'package:papyrus_chat_app/utils/mocks.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../../configs/test_configs.dart';

void main() {
  ChatApi _api;

  setUpAll(() async {
    await initTestSettings();
    _api = inject();
    inject<TokenRepository>().save(await inject<AuthApi>()
        .login(User(email: "duy.le@iei.vn", password: "P@ssw0rd")));
  });

  group('Chat api', () {
    IOWebSocketChannel channel;

    tearDown(() {
      channel?.sink?.close();
    });

    test('Chat via room', () async {
      RoomApi roomApi = inject<RoomApi>();
      var toBeCreatedRoom =
          mockRoom(randomId: false, mockLastMessage: false, memberSize: 0);
      toBeCreatedRoom.isPrivate = false;
      var newRoom = await roomApi.create(toBeCreatedRoom);

      RoomToken token = await _api.getRoomToken(newRoom.id);
      expect(token, isNotNull);

      WebSocketChannel channel = _api.joinRoom(token);
      expect(channel, isNotNull);

      StreamView(channel.stream).listen((value) {
        logDebug(value);
      }, onError: (error) => logError(error));

      String messageContent = "Test Chat Integration";
      _api.sendMessage(channel, messageContent);

      logDebug(newRoom.id);
      logDebug(inject<TokenRepository>().getAuthToken().token);

      await channel?.sink?.close();

      logDebug("Closed web socket");
      sleep(const Duration(seconds: 5));

      PageData<Message> newMessages = await roomApi.getMessages(newRoom.id);
      expect(newMessages.data, isNotEmpty);
      expect(newMessages.data[0].content, messageContent);

      var lastMessage = (await inject<RoomApi>().get(newRoom.id)).lastMessage;
      expect(lastMessage.content, messageContent);
    });
  });
}
