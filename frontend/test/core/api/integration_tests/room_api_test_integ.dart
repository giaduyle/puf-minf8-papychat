import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/api/auth_api.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';
import 'package:papyrus_chat_app/core/api/room_api.dart';
import 'package:papyrus_chat_app/core/api/user_api.dart';
import 'package:papyrus_chat_app/core/models/exceptions.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/token_repository.dart';
import 'package:papyrus_chat_app/utils/mocks.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import '../../../configs/test_configs.dart';
import '../../models/room_test.dart';

void main() {
  RoomApi _api;

  setUpAll(() async {
    await initTestSettings();
    _api = inject();
    inject<TokenRepository>().save(await inject<AuthApi>()
        .login(User(email: "duy.le@iei.vn", password: "P@ssw0rd")));
  });

  group('Group apis', () {
    test('CRUD room', () async {
      var me = await inject<UserApi>().getMe();
      expect(me, isNotNull);

      PageData<Room> rooms =
          await _api.getMany(page: Page(pageSize: 20, pageNum: 0));
      expect(rooms.page.returnCount, greaterThanOrEqualTo(1));
      expect(rooms.page.returnPageCount, greaterThanOrEqualTo(1));
      expect(rooms.data, isNotEmpty);

      PageData<Room> searched = await _api.search("DASA");
      expect(searched.page.returnCount, 1);
      expect(searched.page.returnPageCount, 1);
      expect(searched.data.length, 1);

      Room created = await _api.create(
          mockRoom(randomId: false, memberSize: 2, mockLastMessage: false));
      expect(created, isNotNull);
      expect(created.id, isNotNull);
      expect(created.members, isNull);

      Room queryCreated = await _api.get(created.id);
      expectSameRoom(created, queryCreated,
          expectLastMessage: false, expectMembers: false);

      await _api.delete(created.id);
      expect(() async => await _api.get(created.id),
          throwsA(isInstanceOf<HttpClientError>()));
    });

    test('Members', () async {
      var me = await inject<UserApi>().getMe();
      expect(me, isNotNull);

      Room iotRoom = (await _api.search('IoT')).data[0];
      expect(iotRoom, isNotNull);
      expect(iotRoom.lastMessage, isNotNull);
      expect(iotRoom.members, isNull);

      List<User> iotMembers = (await _api.getMembers(iotRoom.id)).data;
      expect(iotMembers, isNotEmpty);

      Room createdRoom = await _api.create(
          mockRoom(randomId: false, memberSize: 2, mockLastMessage: false));
      var newUser = await inject<UserApi>().create(mockUser(randomId: false));
      List<User> addedMembers =
          await _api.addMembers(createdRoom.id, [newUser.id]);
      expect(addedMembers.length, 1);
      expect(addedMembers.map((e) => e.id).contains(newUser.id), isTrue);

      PageData<User> queryAddedMembers = await _api.getMembers(createdRoom.id);
      expect(
          queryAddedMembers.data.map((e) => e.id).contains(newUser.id), isTrue);

      await _api.removeMember(createdRoom.id, newUser.id);
      PageData<User> deleteMember = await _api.getMembers(createdRoom.id);
      expect(deleteMember.data, isEmpty);

      List<User> replacedMembers = await _api.replaceMembers(
          createdRoom.id, iotMembers.map((e) => e.id).toList());
      expect(replacedMembers.length, iotMembers.length);

      PageData<User> queryReplacedMembers =
          await _api.getMembers(createdRoom.id);
      expect(queryReplacedMembers.data.length, iotMembers.length);
    });

    test('Messages', () async {
      var me = await inject<UserApi>().getMe();
      expect(me, isNotNull);

      Room iotRoom = (await _api.search('IoT')).data[0];
      expect(iotRoom, isNotNull);
      expect(iotRoom.lastMessage, isNotNull);
      expect(iotRoom.members, isNull);

      PageData<Message> messages = await _api.getMessages(iotRoom.id);
      expect(messages.data, isNotEmpty);
      messages.data.forEach((message) {
        expect(message.roomId, iotRoom.id);
      });
    });
  });
}
