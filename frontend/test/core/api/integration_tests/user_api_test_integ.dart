import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/api/auth_api.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';
import 'package:papyrus_chat_app/core/api/user_api.dart';
import 'package:papyrus_chat_app/core/models/event.dart';
import 'package:papyrus_chat_app/core/models/exceptions.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/token_repository.dart';
import 'package:papyrus_chat_app/utils/mocks.dart';
import 'package:papyrus_chat_app/utils/utils.dart';
import 'package:random_string/random_string.dart';

import '../../../configs/test_configs.dart';
import '../../models/user_test.dart';

void main() {
  UserApi _api;

  setUpAll(() async {
    await initTestSettings();
    _api = inject();
    inject<TokenRepository>().save(await inject<AuthApi>()
        .login(User(email: "duy.le@iei.vn", password: "P@ssw0rd")));
  });

  group('User api', () {
    test('CRUD', () async {
      var me = await _api.getMe();
      expect(me, isNotNull);

      var meById = await _api.get(me.id);
      expectSameUser(meById, me, compareDates: false);

      me.nickName = "new nickname";
      User updated = await _api.replace(me);
      expect(updated.nickName, "new nickname");

      PageData<User> all = await _api.getMany();
      expect(all.data.length, greaterThanOrEqualTo(1));
      expect(all.page.returnCount, greaterThanOrEqualTo(1));
      expect(all.page.returnPageCount, greaterThanOrEqualTo(1));

      PageData<User> searched = await _api.search("Son");
      expect(searched.data.length, 2);
      expect(searched.page.returnCount, 2);
      expect(searched.page.returnPageCount, 1);

      var newUser = mockUser(randomId: false);
      newUser.email = "test_${randomAlphaNumeric(10)}@email.com";
      newUser.password = "123456";
      var created = await _api.create(newUser);
      expect(created.id, isNotNull);
      expectSameUser(await _api.get(created.id), created, compareDates: false);

      created.password = "123456";
      inject<TokenRepository>().save(await inject<AuthApi>().login(created));
      await _api.delete(created.id);
      expect(() async => await _api.get(created.id),
          throwsA(isInstanceOf<HttpClientError>()));
    });

    test('Friends', () async {
      var me = await _api.getMe();
      expect(me, isNotNull);

      Iterable<User> myFriends = await _api.getFriends(me.id);
      expect(myFriends.length, greaterThanOrEqualTo(1));

      var newUser = await _api.create(mockUser(randomId: false));
      await _api.addFriend(newUser.id, myFriends.map((e) => e.id).toList());

      await _api.removeFriend(newUser.id, me.id);
      var newUserRemovedMe = await _api.getFriends(newUser.id);
      expect(newUserRemovedMe.contains(me.id), isFalse);
      expect(newUserRemovedMe.length, 0);
    });

    test('Favourites', () async {
      var me = await _api.getMe();
      expect(me, isNotNull);

      Iterable<User> myFavourites = await _api.getFavourites(me.id);
      expect(myFavourites.length, greaterThanOrEqualTo(1));

      var newUser = await _api.create(mockUser(randomId: false));
      var newUserFriends = await _api.addFavourite(
          newUser.id, myFavourites.map((e) => e.id).toList());
      expect(newUserFriends.length, myFavourites.length);

      await _api.replaceFavourites(newUser.id, [me.id]);
      var newUserOnlyMe = await _api.getFavourites(newUser.id);
      expect(newUserOnlyMe.length, 1);
      expect(newUserOnlyMe[0].id == me.id, isTrue);

      await _api.removeFavourite(newUser.id, me.id);
      var newUserRemovedMe = await _api.getFavourites(newUser.id);
      expect(newUserRemovedMe.length, 0);
    });

    test('Blocks', () async {
      var userA = await _api.create(mockUser(randomId: false));
      var userB = await _api.create(mockUser(randomId: false));
      var userC = await _api.create(mockUser(randomId: false));

      Set<String> blocks = (await _api.block(userA.id, [userB.id, userC.id]))
          .map((e) => e.id)
          .toSet();
      expect(blocks.length, 2);
      expect(blocks.contains(userB.id), isTrue);
      expect(blocks.contains(userC.id), isTrue);

      Set<String> getBlocks =
          (await _api.getBlocks(userA.id)).map((e) => e.id).toSet();
      expect(getBlocks.length, 2);
      expect(getBlocks.contains(userB.id), isTrue);
      expect(getBlocks.contains(userC.id), isTrue);

      var userD = await _api.create(mockUser(randomId: false));
      Set<String> replacedBlocks =
          (await _api.replaceBlocks(userA.id, [userD.id]))
              .map((e) => e.id)
              .toSet();
      expect(replacedBlocks.length, 1);
      expect(replacedBlocks.contains(userB.id), isFalse);
      expect(replacedBlocks.contains(userC.id), isFalse);
      expect(replacedBlocks.contains(userD.id), isTrue);

      Set<String> queryReplacedBlocks =
          (await _api.getBlocks(userA.id)).map((e) => e.id).toSet();
      expect(queryReplacedBlocks.length, 1);
      expect(queryReplacedBlocks.contains(userB.id), isFalse);
      expect(queryReplacedBlocks.contains(userC.id), isFalse);
      expect(queryReplacedBlocks.contains(userD.id), isTrue);

      await _api.unblock(userA.id, userD.id);
      Set<String> queryUnblocks =
          (await _api.getBlocks(userA.id)).map((e) => e.id).toSet();
      expect(queryUnblocks.length, 0);
      expect(queryUnblocks.contains(userB.id), isFalse);
      expect(queryUnblocks.contains(userC.id), isFalse);
      expect(queryUnblocks.contains(userD.id), isFalse);
    });

    test('Rooms', () async {
      var me = await _api.getMe();
      expect(me, isNotNull);

      PageData<Room> rooms = await _api.getRooms(me.id);
      expect(rooms.data, isNotEmpty);
    });

    test('Events', () async {
      inject<TokenRepository>().save(await inject<AuthApi>()
          .login(User(email: "y.nguyen@iei.vn", password: "P@ssw0rd")));
      var me = await inject<UserApi>().getMe();

      var eventsQueried = (await _api.getEvents(me.id)).data;
      expect(eventsQueried, isNotEmpty);

      var event = eventsQueried[0];
      var randomStatus = EnumUtils.randomEnum(EventStatus.values);
      event.status = randomStatus;
      Event updatedEvent = await _api.updateEvent(me.id, event);
      expect(updatedEvent, isNotNull);
      expect(updatedEvent.status, randomStatus);

      var updatedEventStatus = await _api.getEvent(me.id, event.id);
      expect(updatedEventStatus, isNotNull);
      expect(updatedEventStatus.status, randomStatus);
    });
  });
}
