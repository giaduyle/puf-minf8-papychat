import 'dart:convert';
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/api/auth_api.dart';
import 'package:papyrus_chat_app/core/api/media_api.dart';
import 'package:papyrus_chat_app/core/models/media.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/token_repository.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import '../../../configs/test_configs.dart';

void main() {
  MediaApi _api;

  setUpAll(() async {
    await initTestSettings();
    _api = inject();
    inject<TokenRepository>().save(await inject<AuthApi>()
        .login(User(email: "duy.le@iei.vn", password: "P@ssw0rd")));
  });

  group('Media apis', () {
    test('upload', () async {
      var media = Media(
          contentType: ContentType.text.mimeType,
          content: utf8.encoder.convert("This is a test content"),
          name: "test.txt");

      var uploaded = (await _api.upload(media));
      expect(uploaded, isNotNull);
      expect(uploaded.content, isNull);

      var meta = await _api.get(uploaded.id);
      expect(meta.id, uploaded.id);
      expect(meta.name, media.name);
      expect(meta.contentType, media.contentType);

      var downloaded = await _api.download(uploaded.id);
      expect(downloaded, isNotNull);
      expect(downloaded.content, isNotNull);
      expect(ListEquality().equals(downloaded.content, media.content), isTrue);
    });
  });
}
