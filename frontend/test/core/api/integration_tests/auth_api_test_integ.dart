import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/core/api/auth_api.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import '../../../configs/test_configs.dart';

void main() {
  setUp(() async {
    await initTestSettings();
  });

  group('Auth', () {
    test("login/refresh", () async {
      Token loggedIn = await inject<AuthApi>()
          .login(User(email: "test01@iei.vn", password: "P@ssw0rd"));
      logInfo(loggedIn.toString());
      expect(loggedIn, isNotNull);
      expect(loggedIn.token, isNotEmpty);

      Token refreshed = await inject<AuthApi>().refresh(loggedIn);
      logInfo(refreshed.toString());
      expect(refreshed, isNotNull);
      expect(refreshed.token, isNotEmpty);
      expect(refreshed.token == loggedIn.token, isFalse);
    });
  });
}
