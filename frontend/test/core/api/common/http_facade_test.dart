import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:papyrus_chat_app/core/api/common/http_facade.dart';
import 'package:papyrus_chat_app/core/models/exceptions.dart';

void main() {
  group("Formatting", () {
    test("Page", () {
      Page page = Page();
      expect(page.hasNext, isFalse);

      Page onlyPageNum = Page(pageNum: 0);
      expect(onlyPageNum.hasNext, isTrue);

      Page manyPages = Page(pageNum: 9, pageSize: 200, returnPageCount: 10);
      expect(manyPages.hasNext, isTrue);
      expect(manyPages.previous.pageNum, lessThan(manyPages.pageNum));
      expect(manyPages.next.pageNum, greaterThan(manyPages.pageNum));
      expect(manyPages.next.hasNext, isFalse);
    });

    test("Format only page", () {
      expect(HttpFacade.formatParams(page: Page(pageNum: 2, pageSize: 15)),
          "?pageNum=2&pageSize=15");

      DateTime now = DateTime.now();
      expect(
          HttpFacade.formatParams(
              page: Page(pageNum: 2, pageSize: 15, beforeTime: now)),
          "?pageNum=2&pageSize=15&beforeTime=${now.toIso8601String()}");
      expect(
          HttpFacade.formatParams(
              page: Page(pageNum: 2, pageSize: 15, afterTime: now)),
          "?pageNum=2&pageSize=15&afterTime=${now.toIso8601String()}");
      expect(
          HttpFacade.formatParams(
              page: Page(
                  pageNum: 2, pageSize: 15, beforeTime: now, afterTime: now)),
          "?pageNum=2&pageSize=15"
          "&beforeTime=${now.toIso8601String()}"
          "&afterTime=${now.toIso8601String()}");
    });

    test("Format only queries", () {
      expect(
          HttpFacade.formatParams(
              queries: {"name": "ABC", "email": "ABC@email.com"}),
          "?name=ABC&email=ABC@email.com");
    });

    test("Format page and queries", () {
      expect(
          HttpFacade.formatParams(
              page: Page(pageNum: 2, pageSize: 15),
              queries: {"name": "ABC", "email": "ABC@email.com"}),
          "?pageNum=2&pageSize=15&name=ABC&email=ABC@email.com");
    });
  });

  group("Error", () {
    test("Extract client error", () {
      expect(HttpFacade.error(Response("error", HttpStatus.badRequest)),
          isInstanceOf<HttpClientError>());
    });

    test("Extract server error", () {
      expect(
          HttpFacade.error(Response("error", HttpStatus.internalServerError)),
          isInstanceOf<HttpServerError>());
    });
  });
}
