import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/user_repository.dart';
import 'package:papyrus_chat_app/utils/mocks.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import '../../configs/test_configs.dart';
import '../models/user_test.dart';

void main() {
  final _now = DateTime.now();
  UserRepository _repository;

  setUpAll(() async {
    await initTestSettings();
    _repository = inject();
  });

  tearDownAll(() async {
    await _repository.clear();
    await _repository.delete();
  });

  group('Test User repo', () {
    test('Test Me', () async {
      User meBefore = User(
          id: "id1",
          email: "@email.com",
          password: "password",
          firstName: "firstName",
          lastName: "lastName",
          nickName: "nickName",
          address: "address",
          phone: "phone",
          status: UserStatus.Online,
          avatarUrl: "avatarUrl",
          birthday: _now,
          gender: UserGender.Male,
          lastSeenTime: _now,
          favourite: true,
          createdAt: _now,
          updatedAt: _now);

      _repository.putMe(meBefore);

      User meAfter = _repository.getMe();

      expectSameUser(meAfter, meBefore);
    });

    User _friendA = mockUser();
    _friendA.id = "idA";
    _friendA.firstName = "A";
    _friendA.lastName = "lastName";

    User _friendB = mockUser();
    _friendB.id = "idB";
    _friendB.firstName = "B";
    _friendB.lastName = "lastName";

    User _friendC = mockUser();
    _friendC.id = "idC";
    _friendC.firstName = "C";
    _friendC.lastName = "C";

    test('Test Friends', () async {
      _repository.putFriends([_friendA, _friendB]);

      expect(_repository.getFriends().length, 2);
      expect(_repository.getFriend(_friendA.id), _friendA);
      expect(_repository.getFriend(_friendB.id), _friendB);

      _repository.putFriends([_friendC]);
      expect(_repository.getFriends().length, 3);
      expect(_repository.getFriend(_friendC.id), _friendC);

      List<User> searchedA = List.from(_repository.searchFriends("A"));
      expect(searchedA.length, 1);
      expectSameUser(searchedA[0], _friendA);

      List<User> sameLastName =
          List.from(_repository.searchFriends("lastName"));
      expect(sameLastName.length, 2);
      expectSameUser(sameLastName[0], _friendA);
      expectSameUser(sameLastName[1], _friendB);

      await _repository.compact();
      expect(_repository.getFriends().length, 3);

      await _repository.clearFriends();
      expect(_repository.getFriends().length, 0);
    });

    test('Test Blocks', () async {
      _repository.putBlocks([_friendA, _friendB]);

      expect(_repository.getBlocks().length, 2);
      expect(_repository.getBlock(_friendA.id), _friendA);
      expect(_repository.getBlock(_friendB.id), _friendB);

      _repository.putBlocks([_friendC]);
      expect(_repository.getBlocks().length, 3);
      expect(_repository.getBlock(_friendC.id), _friendC);

      await _repository.compact();
      expect(_repository.getBlocks().length, 3);

      await _repository.clearBlocks();
      expect(_repository.getBlocks().length, 0);
    });
  });
}
