import 'package:collection/collection.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/core/repositories/common/repository_mixin.dart';

import '../../configs/test_configs.dart';

void main() {
  setUpAll(() async {
    await initTestSettings();
  });

  test('Reversed key comparator', () async {
    Box<String> strings =
        await Hive.openBox("strings", keyComparator: reversedKeyComparator);

    await strings.put("Key-1", "1");
    await strings.put("Key-2", "2");
    await strings.put("Key-3", "3");

    var values = strings.values;
    logInfo(values.toString());
    Function eq = ListEquality<String>().equals;
    expect(eq(List.of(values), <String>["3", "2", "1"]), isTrue);

    await strings.clear();
    await strings.deleteFromDisk();
  });
}
