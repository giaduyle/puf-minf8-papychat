import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/repositories/event_repository.dart';
import 'package:papyrus_chat_app/utils/mocks.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import '../../configs/test_configs.dart';

void main() {
  EventRepository _repository;

  setUpAll(() async {
    await initTestSettings();
    _repository = inject();
  });

  tearDownAll(() async {
    await _repository.clear();
    await _repository.delete();
  });

  group('Events', () {
    var eventA = mockEvent();
    eventA.id = "event-A";

    var eventB = mockEvent();
    eventB.id = "event-B";

    test('Events', () async {
      var roomA = mockRoom(memberSize: 2);
      roomA.id = "room-A";

      var roomB = mockRoom(memberSize: 2);
      roomB.id = "room-B";

      var userA = mockUser();
      userA.id = "user-A";

      var userB = mockUser();
      userB.id = "user-B";

      await _repository.putRoomEvents(roomA, [eventA, eventB]);
      await _repository.putRoomEvents(roomB, [eventA, eventB]);
      var roomEvents = _repository.getRoomEvents(roomA);
      expect(roomEvents.length, 2);
      expect(roomEvents[0].id, "event-A");
      expect(roomEvents[1].id, "event-B");

      await _repository.putUserEvents(userA, [eventA, eventB]);
      await _repository.putUserEvents(userB, [eventA, eventB]);
      var userEvents = _repository.getUserEvents(userA);
      expect(userEvents.length, 2);
      expect(userEvents[0].id, "event-A");
      expect(userEvents[1].id, "event-B");

      await _repository.clearRoomEvents(roomA);
      expect(_repository.getRoomEvents(roomA), isEmpty);
      expect(_repository.getRoomEvents(roomB).length, 2);
      expect(_repository.getUserEvents(userA), isNotNull);

      await _repository.clearUserEvents(userA);
      expect(_repository.getUserEvents(userA), isEmpty);
      expect(_repository.getUserEvents(userB).length, 2);
    });
  });
}
