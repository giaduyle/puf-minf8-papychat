import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:papyrus_chat_app/core/repositories/token_repository.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import '../../configs/test_configs.dart';

void main() {
  TokenRepository _repository;

  setUpAll(() async {
    await initTestSettings();
    _repository = inject();
  });

  tearDownAll(() async {
    _repository.delete();
    expect(inject<TokenRepository>(), isNull);
  });

  test('Test save/retrieve', () async {
    Token input = Token(id: "123", token: "tk-123456789");

    _repository.save(input);

    Token retrieved = _repository.get("123");
    expect(retrieved.id, input.id);
    expect(retrieved.token, input.token);

    AuthToken authToken = AuthToken("auth-token-123");
    _repository.save(authToken);
    expect(_repository.getAuthToken().token, authToken.token);

    RoomToken roomToken = RoomToken("room-1", "room-token-123");
    _repository.save(roomToken);
    expect(_repository.getRoomToken("room-1").token, roomToken.token);

    Token notFound = _repository.get("not-found");
    expect(notFound, isNull);
  });
}
