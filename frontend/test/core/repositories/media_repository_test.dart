import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/repositories/media_repository.dart';
import 'package:papyrus_chat_app/utils/mocks.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import '../../configs/test_configs.dart';

void main() {
  MediaRepository _repository;

  setUpAll(() async {
    await initTestSettings();
    _repository = inject();
  });

  tearDownAll(() async {
    await _repository.clear();
    await _repository.delete();
  });

  group('Media repository', () {
    test('Media', () async {
      var mediaA = mockMedia();
      mediaA.id = 'media-A';

      var mediaB = mockMedia();
      mediaB.id = 'media-B';

      var mediaC = mockMedia();
      mediaC.id = 'media-C';

      await _repository.putMedias([mediaA, mediaB]);
      await _repository.putMedia(mediaC);
      expect(await _repository.getMedia("media-A"), isNotNull);
      expect(await _repository.getMedia("media-B"), isNotNull);
      expect(await _repository.getMedia("media-C"), isNotNull);

      await _repository.deleteMedias([mediaA, mediaB]);
      await _repository.deleteMedia(mediaC);
      expect(await _repository.getMedia("media-A"), isNull);
      expect(await _repository.getMedia("media-B"), isNull);
      expect(await _repository.getMedia("media-C"), isNull);
    });
  });
}
