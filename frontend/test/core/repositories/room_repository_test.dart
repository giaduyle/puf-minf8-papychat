import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';
import 'package:papyrus_chat_app/core/models/user.dart';
import 'package:papyrus_chat_app/core/repositories/room_repository.dart';
import 'package:papyrus_chat_app/utils/mocks.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import '../../configs/test_configs.dart';
import '../models/room_test.dart';

void main() {
  RoomRepository _repository;

  setUpAll(() async {
    await initTestSettings();
    _repository = inject();
  });

  tearDownAll(() async {
    await _repository.clear();
    await _repository.delete();
  });

  group('Room Repository', () {
    var roomA = mockRoom(memberSize: 2);
    roomA.id = "room-A";
    roomA.name = "name-1";

    var roomB = mockRoom(memberSize: 2);
    roomB.id = "room-B";
    roomB.name = "name-1";

    var roomC = mockRoom(memberSize: 2);
    roomC.id = "room-C";
    roomC.name = "name-2";

    test('Rooms', () async {
      _repository.putRooms([roomA, roomB, roomC]);
      expect(_repository.getRooms().length, 3);
      expectSameRoom(_repository.getRoom("room-A"), roomA);
      expectSameRoom(_repository.getRoom("room-B"), roomB);
      expectSameRoom(_repository.getRoom("room-C"), roomC);

      List<Room> searchedName1 = List.of(_repository.searchRooms("name-1"));
      expect(searchedName1.length, 2);
      expect(searchedName1[0].id, "room-B");
      expect(searchedName1[1].id, "room-A");

      List<Room> searchedName2 = List.of(_repository.searchRooms("name-2"));
      expect(searchedName2.length, 1);
      expect(searchedName2[0].id, "room-C");

      await _repository.deleteRoom(roomA);
      expect(_repository.getRooms().length, 2);
      expect(_repository.getRoom("room-A"), isNull);
    });

    test('Messages', () async {
      var lastMessage = mockMessage();
      lastMessage.createdAt = DateTime.now().subtract(Duration(days: 365));
      roomA.lastMessage = lastMessage;

      _repository.putRooms([roomA, roomB]);
      expect(roomA.lastMessage, isNotNull);

      DateTime now = DateTime.now();
      var message1 = mockMessage();
      message1.id = "message-1";
      message1.createdAt = now;

      var message2 = mockMessage();
      message2.id = "message-2";
      message2.createdAt = now.add(Duration(hours: 1));

      var message3 = mockMessage();
      message3.id = "message-3";
      message3.createdAt = now.add(Duration(hours: 3));

      await _repository.putMessages(roomA, [message3, message1, message2]);

      List<Message> roomAMessages =
          List.of(await _repository.getMessages(roomA));
      expect(roomAMessages.length, 3);
      expect(roomAMessages[0].id, "message-3");
      expect(roomAMessages[1].id, "message-2");
      expect(roomAMessages[2].id, "message-1");

      expect(await _repository.getMessages(roomB), isEmpty);

      Message latestMessage = await _repository.findLatestMessage(roomA);
      expect(latestMessage.id, message3.id);

      await _repository.deleteMessages(roomA);
      expect(await _repository.getMessages(roomA), isEmpty);
    });

    test('Members', () async {
      _repository.putRooms([roomA, roomB]);

      var member1 = mockUser();
      member1.id = "member-1";

      var member2 = mockUser();
      member2.id = "member-2";

      await _repository.putMembers(roomA, [member1, member2]);
      List<User> roomAMembers = List.of(await _repository.getMembers(roomA));
      expect(roomAMembers.length, 2);
      expect(roomAMembers[0].id, "member-1");
      expect(roomAMembers[1].id, "member-2");

      await _repository.removeAllMembers(roomA);
      expect(await _repository.getMembers(roomA), isEmpty);
    });
  });
}
