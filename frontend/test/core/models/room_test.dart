import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/models/message.dart';
import 'package:papyrus_chat_app/core/models/room.dart';

import 'message_test.dart';

void expectSameRoom(Room source, Room target,
    {expectLastMessage = true, expectMembers = true}) {
  expect(source.id, target.id);
  expect(source.name, target.name);
  expect(source.isPrivate, target.isPrivate);
  expect(source.createdAt, target.createdAt);
  expect(source.updatedAt, target.updatedAt);
  if (expectLastMessage) {
    expectSameMessage(source.lastMessage, target.lastMessage);
  }
  if (expectMembers) {
    expect(source.members?.length, target.members?.length);
  }
}

void main() {
  final _now = DateTime.now();
  final _expectedJson = {
    "id": "room-123",
    "name": "Room1",
    "isPrivate": true,
    "createdAt": _now.toIso8601String(),
    "updatedAt": _now.toIso8601String(),
    "lastMessage": {
      "id": "msg-123",
      "senderId": "user-123",
      "content": "test message",
      "roomId": "room-123",
      "createdAt": _now.toIso8601String(),
      "updatedAt": _now.toIso8601String(),
    }
  };
  final _expectedRoom = Room(
      id: "room-123",
      name: "Room1",
      isPrivate: true,
      createdAt: _now,
      updatedAt: _now,
      lastMessage: Message(
        id: "msg-123",
        senderId: "user-123",
        content: "test message",
        roomId: "room-123",
        createdAt: _now,
        updatedAt: _now,
      ));

  group('Room json', () {
    test('To json', () {
      expect(_expectedRoom.toJson(), _expectedJson);
    });

    test('From json', () {
      expectSameRoom(Room.fromJson(_expectedJson), _expectedRoom);
    });
  });
}
