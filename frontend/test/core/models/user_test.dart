import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/models/user.dart';

void expectSameUser(User source, User target, {bool compareDates = true}) {
  expect(source.id, target.id);
  expect(source.email, target.email);
  expect(source.password, target.password);
  expect(source.firstName, target.firstName);
  expect(source.lastName, target.lastName);
  expect(source.nickName, target.nickName);
  expect(source.address, target.address);
  expect(source.phone, target.phone);
  expect(source.status, target.status);
  expect(source.avatarUrl, target.avatarUrl);
  expect(source.birthday, target.birthday);
  expect(source.gender, target.gender);
  expect(source.favourite, target.favourite);
  if (compareDates) {
    expect(source.lastSeenTime, target.lastSeenTime);
    expect(source.createdAt, target.createdAt);
    expect(source.updatedAt, target.updatedAt);
  }
}

void main() {
  final _now = DateTime.now();
  final _expectedJson = {
    "id": "id1",
    "email": "@email.com",
    "password": "password",
    "firstName": "firstName",
    "lastName": "lastName",
    "nickName": "nickName",
    "address": "address",
    "phone": "phone",
    "status": 1,
    "avatarUrl": "avatarUrl",
    "birthday": _now.toIso8601String(),
    "gender": 2,
    "lastSeenTime": _now.toIso8601String(),
    "createdAt": _now.toIso8601String(),
    "updatedAt": _now.toIso8601String()
  };
  final _expectedUser = User(
      id: "id1",
      email: "@email.com",
      password: "password",
      firstName: "firstName",
      lastName: "lastName",
      nickName: "nickName",
      address: "address",
      phone: "phone",
      status: UserStatus.Online,
      avatarUrl: "avatarUrl",
      birthday: _now,
      gender: UserGender.Male,
      lastSeenTime: _now,
      favourite: true,
      createdAt: _now,
      updatedAt: _now);

  group('User json', () {
    test('To json', () {
      expect(_expectedUser.toJson(), _expectedJson);
    });

    test('From json ', () {
      _expectedUser.favourite = false;
      expectSameUser(User.fromJson(_expectedJson), _expectedUser);
    });
  });
}
