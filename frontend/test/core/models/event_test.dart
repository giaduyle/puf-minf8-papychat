import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/models/event.dart';

void expectSameEvent(Event source, Event target) {
  expect(source.senderId, target.senderId);
  expect(source.receiverId, target.receiverId);
  expect(source.type, target.type);
  expect(source.status, target.status);
  expect(source.createdAt, target.createdAt);
  expect(source.updatedAt, target.updatedAt);
}

void main() {
  final _now = DateTime.now();
  final _expectedJson = {
    "id": "event-123",
    "senderId": "user-123",
    "receiverId": "user-456",
    "type": 0,
    "status": 1,
    "createdAt": _now.toIso8601String(),
    "updatedAt": _now.toIso8601String()
  };
  final _expectedEvent = Event(
      id: "event-123",
      senderId: "user-123",
      receiverId: "user-456",
      type: EventType.InviteFriend,
      status: EventStatus.Accepted,
      createdAt: _now,
      updatedAt: _now);

  group('Event json', () {
    test('To json', () {
      expect(_expectedEvent.toJson(), _expectedJson);
    });

    test('From json', () {
      expectSameEvent(Event.fromJson(_expectedJson), _expectedEvent);
    });
  });
}
