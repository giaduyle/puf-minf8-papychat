import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/models/message.dart';

void expectSameMessage(Message source, Message target) {
  expect(source.id, target.id);
  expect(source.senderId, target.senderId);
  expect(source.content, target.content);
  expect(source.roomId, target.roomId);
  expect(source.createdAt, target.createdAt);
  expect(source.updatedAt, target.updatedAt);
}

void main() {
  final now = DateTime.now();
  final _expectedJson = {
    "id": "msg-123",
    "senderId": "user-123",
    "content": "msg content",
    "roomId": "room-123",
    "createdAt": now.toIso8601String(),
    "updatedAt": now.toIso8601String()
  };

  final _expectedMessage = Message(
      id: "msg-123",
      senderId: "user-123",
      content: "msg content",
      roomId: "room-123",
      createdAt: now,
      updatedAt: now);

  group('Message json', () {
    test('To json', () {
      expect(_expectedMessage.toJson(), _expectedJson);
    });
    test('From json', () {
      expectSameMessage(Message.fromJson(_expectedJson), _expectedMessage);
    });
  });
}
