import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/models/media.dart';

void expectSameMedia(Media source, Media target) {
  expect(source.id, target.id);
  expect(source.name, target.name);
  expect(source.contentType, target.contentType);
  expect(source.createdAt, target.createdAt);
  expect(source.updatedAt, target.updatedAt);
}

void main() {
  final _now = DateTime.now();
  final _binaries = List.generate(20, (index) => index);
  final _expectedJson = {
    "id": "media-123",
    "name": "Test image media",
    "contentType": "image",
    "createdAt": _now.toIso8601String(),
    "updatedAt": _now.toIso8601String()
  };
  final _expectedMedia = Media(
      id: "media-123",
      name: "Test image media",
      content: _binaries,
      contentType: "image",
      createdAt: _now,
      updatedAt: _now);

  group('Media json', () {
    test('To json', () {
      expect(_expectedMedia.toJson(), _expectedJson);
    });

    test('From json', () {
      expectSameMedia(Media.fromJson(_expectedJson), _expectedMedia);
    });
  });
}
