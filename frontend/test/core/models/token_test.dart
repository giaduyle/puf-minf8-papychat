import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/core/models/token.dart';
import 'package:random_string/random_string.dart';

void main() {
  final String tokenString = randomString(10);
  final _expectedJson = {'token': tokenString};
  final _expectedToken = Token(id: 'test-token', token: tokenString);

  group('Token json', () {
    test('To json', () {
      expect(_expectedToken.toJson(), _expectedJson);
    });

    test('From json', () {
      expect(Token.fromJson(_expectedJson).token, _expectedToken.token);
    });
  });
}
