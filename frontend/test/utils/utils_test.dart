import 'package:flutter_test/flutter_test.dart';
import 'package:papyrus_chat_app/configs/logging.dart';
import 'package:papyrus_chat_app/utils/utils.dart';

import '../configs/test_configs.dart';

void main() {
  setUpAll(() async {
    await initTestSettings();
  });

  group('Encryption', () {
    test('encrypt/decrypt AESCryptoJS', () {
      String plainText = "Text to be encrypted";
      String secretKey = SecurityUtils.getSecretPhrase();

      logDebug(secretKey);
      String encrypted = SecurityUtils.encryptAESCryptoJS(plainText, secretKey);

      logDebug(encrypted);
      String decrypted = SecurityUtils.decryptAESCryptoJS(encrypted, secretKey);

      expect(decrypted, "Text to be encrypted");
    });
  });
}
