# PapyrusChat application

A chat application inspired by ancient papyrus scrolls used to
communicate in Egypt.

## How to setup the project
#### Install Flutter SDK from here:
[https://flutter.dev/docs/get-started/install]()

#### Run Flutter doctor and fix all problems:
`flutter doctor`

### Generating missing codes
`flutter pub run build_runner build --delete-conflicting-outputs`

## How to run the application

### For development
`flutter run --dart-define=ENV=dev -d {device_id}`

### For UI Mocks
`flutter run --dart-define=ENV=mock -d {device_id}`

### For production
`flutter run --dart-define=ENV=prod -d {device_id}`

### List current Flutter supported devices
`flutter devices`

#### For Chrome
`flutter run -d chrome`
* First start an Android simulator
* Find the Android device identifier from the list devices command
* Run this command `flutter run -d {android_id}`

#### For iOS
* First start an iOS simulator
* Find the iOS device identifier from the list devices command
* Run this command `flutter run -d {ios_id}`

### For cleaning the current workspace (build)
`flutter clean`